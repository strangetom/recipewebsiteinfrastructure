import base64
import hashlib
from functools import lru_cache
from typing import Any


@lru_cache
def sha256(string: str) -> str:
    """Calculate sha256 hash from string

    Parameters
    ----------
    string : str
        String to calculate sha256 hash of

    Returns
    -------
    str
        Base64 encoded sha256 hash of inpnut
    """
    encoded_content = string.encode("utf")
    sha256 = hashlib.sha256(encoded_content).digest()
    return base64.b64encode(sha256).decode()


@lru_cache
def readable_duration(seconds: int) -> str:
    """Turn a number of seconds into a human readable string

    Parameters
    ----------
    seconds : int
        Number of seconds

    Returns
    -------
    str
        Human readable string e.g. 1 hr 20 mins
    """
    hours = int(seconds / 60 / 60)
    minutes = int(seconds / 60 - hours * 60)
    if hours > 0 and minutes == 0:
        return f"{hours} hr"
    elif hours > 0:
        return f"{hours} hr {minutes} mins"
    else:
        return f"{minutes} mins"


def select_annotatable_ingredients(
    ingredients: list[dict[str, Any]],
) -> list[dict[str, str]]:
    """Select all ingredients that can be annoted in instructions and return
    as a flat list.
    Annotatable ingredients are those that have a quantity or a unit, or both.
    Return as a sorted list, with the longest ingredient first

    Parameters
    ----------
    ingredients : list[Any]
        List of recipe ingredients.

    Returns
    -------
    list[Dict[str, Any]]
        Flattened, sorted list of annotatable ingredients
    """
    # Filter out any ingredients with no quantity and no unit
    ingredients = [i for i in ingredients if i["quantity"] != "" or i["unit"] != ""]

    # Order list with longest ingredient first
    ingredients = sorted(ingredients, key=ingredient_length, reverse=True)

    return ingredients


def ingredient_length(ingredient: dict[str, str]) -> int:
    """Return length of ingredient used for tooltips

    Parameters
    ----------
    ingredient : dict[str, str]
        Dict of ingredient information

    Returns
    -------
    int
        Length of ingredient synonym or name

    """
    return len(ingredient["synonym"] or ingredient["name"])
