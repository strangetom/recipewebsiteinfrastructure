# RecipeWebsiteInfrastructure

This repository contains the infrastructure for the StrangerFoods.org website.

## Structure

```bash
.
├── build_site.py
├── buider
│   ├── filters 
├── static
│   ├── css
│   ├── fonts
│   ├── icons
│   ├── images
│   ├── img
│   ├── js
├── templates
└── website

```

```build_site.py``` generates the site files in the ```website``` directory from the contents of the sub-directories in ```resources```.

### External tools

The ```build_site.py``` script relies on esbuild and rsync.

## Website Features

Below is a list of the features of StrangerFoods.org.

### General

* Fuzzy search recipes by name
* Show recipes by category, cuisine, diet or tag
* Offline access
* Show recent recipes
* Atom and JSON feeds
* Dark theme
* Support javascript disabled

### Recipes

* Scale recipes by 0.5x, 1x, 2x, 3x
* Switch ingredient units between imperial and metric, switch temperatures between celcius and farenheit.
* Print style sheets
* Tooltips showing ingredient quantity and units in recipe instructions
* Wakelock option to keep screen on
* Timers in instructions, with the option to notify on expiry
* Nutrition information
* Embedded Recipe schema.org microdata
* Web share API on supported platforms
* Ingredient substitutions