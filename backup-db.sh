#!/bin/bash -x

# Ensure script stops when commands fail.
set -e

# Set destination
DEST="/home/tom/Syncthing/Important documents/recipes.db.gz"

# Backup & compress our database to the temp directory.
sqlite3 /home/tom/Recipes/Recipes/recipes.db '.backup /tmp/recipes.db'
gzip -f /tmp/recipes.db

# Copy to sync'd folder
# A backup of the previous version is saved in the Recipe Database History folder with the current date appended to the end of the name
rsync --backup-dir="./Recipe Database History/" --suffix=`date +'.%F'` /tmp/recipes.db.gz "$DEST"

# Delete any backups older than 14 days
find "/home/tom/Syncthing/Important documents/Recipe Database History/" -type f -mtime +14 -delete