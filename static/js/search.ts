"use strict";

interface SearchResult {
  category: string;
  cuisine: string[];
  diet: string[];
  slug: string;
  name: string;
  source: string;
  tags: string[];
  time: string;
  yield: number | string;
  positive: boolean;
  ingredients: number[];
}

interface RecipeData {
  recipes: SearchResult[];
}

declare var recipe_data: RecipeData;

// Create flexsearch object
var index = new FlexSearch.Index({ tokenize: "forward" });
recipe_data.recipes.forEach(({ name }, i) => {
  index.add(i, name);
});

document.addEventListener("DOMContentLoaded", () => {
  installServiceWorker();
  customElements.define("recipe-card", RecipeCard, { extends: "a" });

  let categoryBtn: HTMLButtonElement = document.querySelector("#category-btn");
  categoryBtn.disabled = false;
  let categoryModal: HTMLDialogElement =
    document.querySelector("#dialog-category");
  categoryBtn.addEventListener("click", () => {
    categoryModal.showModal();
  });
  let categoryCheckboxes: NodeListOf<HTMLInputElement> =
    categoryModal.querySelectorAll(".checkbox > input");
  categoryCheckboxes.forEach((c) => {
    c.addEventListener("change", displayCategoryFilter);
  });

  let cuisineBtn: HTMLButtonElement = document.querySelector("#cuisine-btn");
  cuisineBtn.disabled = false;
  let cuisineModal: HTMLDialogElement =
    document.querySelector("#dialog-cuisine");
  cuisineBtn.addEventListener("click", () => {
    cuisineModal.showModal();
  });
  let cuisineCheckboxes: NodeListOf<HTMLInputElement> =
    cuisineModal.querySelectorAll(".checkbox");
  cuisineCheckboxes.forEach((c) => {
    c.addEventListener("change", displayCuisineFilter);
  });

  let ingredientBtn: HTMLButtonElement =
    document.querySelector("#ingredients-btn");
  ingredientBtn.disabled = false;
  let ingredientModal: HTMLDialogElement = document.querySelector(
    "#dialog-ingredients",
  );
  let ingredientSearchBox: HTMLInputElement = ingredientModal.querySelector(
    "#search-ingredients",
  );
  ingredientBtn.addEventListener("click", () => {
    ingredientModal.showModal();
    ingredientSearchBox.focus({ preventScroll: true });
  });
  ingredientSearchBox.addEventListener("input", addIngredientToFilter);

  let tagBtn: HTMLButtonElement = document.querySelector("#tag-btn");
  tagBtn.disabled = false;
  let tagModal: HTMLDialogElement = document.querySelector("#dialog-tag");
  tagBtn.addEventListener("click", () => {
    tagModal.showModal();
  });
  let tagCheckboxes: NodeListOf<HTMLInputElement> =
    tagModal.querySelectorAll(".checkbox");
  tagCheckboxes.forEach((c) => {
    c.addEventListener("change", displayTagFilter);
  });

  let nameBtn: HTMLButtonElement = document.querySelector("#name-btn");
  nameBtn.disabled = false;
  let nameModal: HTMLDialogElement = document.querySelector("#dialog-name");
  let nameSearchBox: HTMLInputElement = nameModal.querySelector("#search-name");
  nameBtn.addEventListener("click", () => {
    nameModal.showModal();
    nameSearchBox.focus({ preventScroll: true });
  });
  nameSearchBox.addEventListener("input", displayNameFilter);

  let sourceBtn: HTMLButtonElement = document.querySelector("#source-btn");
  sourceBtn.disabled = false;
  let sourceModal: HTMLDialogElement = document.querySelector("#dialog-source");
  let sourceSearchBox: HTMLInputElement =
    sourceModal.querySelector("#search-source");
  sourceBtn.addEventListener("click", () => {
    sourceModal.showModal();
    sourceSearchBox.focus({ preventScroll: true });
  });
  sourceSearchBox.addEventListener("input", displaySourceFilter);

  // Close any dialog by clicking on backdrop
  let dialogs = document.querySelectorAll("dialog");
  dialogs.forEach((d) => {
    d.addEventListener("click", (event) => {
      if ((event.target as HTMLElement).nodeName === "DIALOG") {
        storeSearch("", []);
        d.close("cancel");
      }
    });
  });

  // Close any dialog by clicking close button on dialog
  let dialogCloseBtns = document.querySelectorAll(
    "dialog .dialog-header button",
  );
  dialogCloseBtns.forEach((btn) => {
    btn.addEventListener("click", closeDialog);
  });

  // Set text on cache button
  updateCacheButton();
  let cacheBtn = document.querySelector("#cache-site-btn");
  cacheBtn.addEventListener("click", () => {
    cacheSite(recipe_data.recipes);
  });

  // Enable theme button and add event listener
  let themeBtn: HTMLButtonElement = document.querySelector("#theme-btn");
  themeBtn.disabled = false;
  themeBtn.addEventListener("click", toggleTheme);
  // Set default theme
  // Use prefers-color-scheme if no prference set
  let lightTheme = localStorage.getItem("light-theme");
  if (lightTheme == null) {
    // No preference set
    applyTheme("Auto");
  } else if (lightTheme == "true") {
    applyTheme("Light");
  } else {
    applyTheme("Dark");
  }

  restoreSearch();
});

class RecipeCard extends HTMLAnchorElement {
  constructor(recipe: SearchResult) {
    super();

    const time_icon =
      '<svg xmlns="http://www.w3.org/2000/svg" class="label-icon" viewBox="0 0 100 100"><circle cx="50" cy="50" r="45" fill="none" stroke="var(--fg)" stroke-width="10"/><path stroke="var(--fg)" fill="none" stroke-width="10" d="M50 15v35l25 25"/></svg>';
    const yield_icon =
      '<svg xmlns="http://www.w3.org/2000/svg" class="label-icon" viewBox="0 0 60 90"><g fill="var(--fg)"><path d="M26.814 21.33c0-10.2-6.003-21.3-13.407-21.3C6.002.03 0 11.13 0 21.33c0 6.429 2.387 10.96 6.006 13.433 2.262 1.546 3.647 4.075 3.599 6.814l-.76 43.582c-.042 2.419 1.717 4.6 4.126 4.824a4.568 4.568 0 005.003-4.626l-.764-43.78c-.048-2.739 1.338-5.268 3.599-6.814 3.618-2.474 6.005-7.004 6.005-13.433zM60.983 29.703L60.219.973a1 1 0 00-1-.973h-2.222a1 1 0 00-1 1v24.127a1 1 0 01-1 1h-1.915a1 1 0 01-1-1V1a1 1 0 00-1-1h-2.297a1 1 0 00-1 1v24.127a1 1 0 01-1 1H44.87a1 1 0 01-1-1V1a1 1 0 00-1-1h-2.222a1 1 0 00-1 .973l-.764 28.73a3.999 3.999 0 002.961 3.969l2.14.575a2.998 2.998 0 012.221 2.95l-.84 48.159a4.566 4.566 0 109.132 0l-.84-48.159a3.001 3.001 0 012.221-2.95l2.14-.575a3.998 3.998 0 002.964-3.969z"/></g></svg>';
    const heart_icon =
      '<svg xmlns="http://www.w3.org/2000/svg" class="label-icon" width="16" height="16" fill="currentColor" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"/></svg>';

    const name = document.createElement("h2");
    name.style.setProperty("--bg-img", `url('/images/${recipe.slug}.webp')`);
    name.innerText = recipe.name;
    name.appendChild(this.newLabel(time_icon + "\n" + recipe.time));
    name.appendChild(this.newLabel(yield_icon + "\nServes " + recipe.yield));

    if (recipe?.positive) {
      let el: HTMLSpanElement = document.createElement("span");
      el.classList.add("recipe-heart");
      el.innerHTML = heart_icon;
      name.appendChild(el);
    }

    this.href = `/recipes/${recipe.slug}`;
    this.appendChild(name);
  }

  /**
   * Convenience function to create a label element
   * @param {string} text      Label text
   */
  newLabel(text: string) {
    const el: HTMLSpanElement = document.createElement("span");
    el.innerHTML = text;
    el.classList.add("recipe-label");
    return el;
  }
}

/**
 * Convenience function to install service worker
 */
function installServiceWorker() {
  if ("serviceWorker" in navigator) {
    console.log("CLIENT: service worker registration in progress.");
    navigator.serviceWorker.register("/service-worker.min.js").then(
      function () {
        console.log("CLIENT: service worker registration complete.");
      },
      function () {
        console.log("CLIENT: service worker registration failure.");
      },
    );
  } else {
    console.log("CLIENT: service worker is not supported.");
  }
}

function closeDialog(e: Event) {
  let btn: HTMLButtonElement = <HTMLButtonElement>e.target;
  let dialog = btn.closest("dialog");
  storeSearch("", []);
  dialog.close();
}

/**
 * Convenience function to create RecipeCard elements to search results
 * @param {SearchResult[]} results Array of SearchResult objects to display
 */
function displayResults(results: SearchResult[], resultsElement: HTMLElement) {
  let newResultsList = resultsElement.cloneNode(false);

  // Create HTML elements for results
  for (let recipe of results) {
    let el = new RecipeCard(recipe);
    el.classList.add("search-result");
    newResultsList.appendChild(el);
  }

  // Replace the old results from the DOM.
  resultsElement.parentNode.replaceChild(newResultsList, resultsElement);
}

/**
 * Convenience function to remove all children from element
 * @param {HTMLElement} resultsElement Element to remove children from
 */
function clearResults(resultsElement: HTMLElement) {
  let emptyList = resultsElement.cloneNode(false);
  resultsElement.parentNode.replaceChild(emptyList, resultsElement);
}

function displayCategoryFilter() {
  let checkedBoxes: NodeListOf<HTMLInputElement> = document.querySelectorAll(
    "#dialog-category input:checked",
  );
  let checkedValues = [...checkedBoxes].map((el) => el.value);
  storeSearch("#dialog-category", checkedValues);

  if (checkedValues.length > 0) {
    let filteredRecipes: SearchResult[] = recipe_data.recipes.filter(
      (recipe) => {
        return checkedValues.includes(recipe.category);
      },
    );
    displayResults(filteredRecipes, document.querySelector("#result-category"));
  } else {
    clearResults(document.querySelector("#result-category"));
  }
}

function displayCuisineFilter() {
  let checkedBoxes: NodeListOf<HTMLInputElement> = document.querySelectorAll(
    "#dialog-cuisine input:checked",
  );
  let checkedValues = [...checkedBoxes].map((el) => el.value);
  storeSearch("#dialog-cuisine", checkedValues);

  if (checkedValues.length > 0) {
    let filteredRecipes: SearchResult[] = recipe_data.recipes.filter(
      (recipe) => {
        let intersection = checkedValues.filter(
          (v) => recipe.cuisine?.includes(v),
        );
        return intersection.length > 0;
      },
    );
    displayResults(filteredRecipes, document.querySelector("#result-cuisine"));
  } else {
    clearResults(document.querySelector("#result-cuisine"));
  }
}

function displayTagFilter() {
  let checkedBoxes: NodeListOf<HTMLInputElement> = document.querySelectorAll(
    "#dialog-tag input:checked",
  );
  let checkedValues = [...checkedBoxes].map((el) => el.value);
  storeSearch("#dialog-tag", checkedValues);

  if (checkedValues.length > 0) {
    let filteredRecipes: SearchResult[] = recipe_data.recipes.filter(
      (recipe) => {
        let intersection = checkedValues.filter(
          (v) => recipe.tags?.includes(v),
        );
        return intersection.length > 0;
      },
    );
    displayResults(filteredRecipes, document.querySelector("#result-tag"));
  } else {
    clearResults(document.querySelector("#result-tag"));
  }
}

function displayNameFilter() {
  let pattern: string = (
    document.querySelector("#search-name") as HTMLInputElement
  ).value;
  storeSearch("#dialog-name", [pattern]);

  if (pattern.length > 0) {
    // If pattern, display maximum of 20 matches
    let idx: number[] = index.search(pattern, 20);
    let filteredRecipes: SearchResult[] = idx.map(
      (i) => recipe_data.recipes[i],
    );
    displayResults(filteredRecipes, document.querySelector("#result-name"));
  } else {
    clearResults(document.querySelector("#result-name"));
  }
}

function displaySourceFilter() {
  let pattern: string = (
    document.querySelector("#search-source") as HTMLInputElement
  ).value;
  storeSearch("#dialog-source", [pattern]);

  if (pattern.length > 0) {
    var filteredRecipes: SearchResult[] = recipe_data.recipes.filter((recipe) =>
      recipe.source.includes(pattern),
    );
    displayResults(filteredRecipes, document.querySelector("#result-source"));
  } else {
    clearResults(document.querySelector("#result-source"));
  }
}

function addIngredientToFilter(e: InputEvent) {
  let searchBox = document.querySelector(
    "#search-ingredients",
  ) as HTMLInputElement;
  //storeSearch("#dialog-ingredients", [pattern]);

  // Only add ingredient to filter if triggered by selection of datalist item
  // In Firefox, the event input type is 'insertReplacementText', in chrome it's null.
  if (e.inputType == "insertReplacementText" || e.inputType == null) {
    let selectedContainer = document.querySelector("#selected-ingredients");
    let ingredient = newIngredient(searchBox.value);
    ingredient.addEventListener("change", (e) => {
      e.preventDefault();
      (e.target as HTMLElement).parentElement.remove();
      displayIngredientFilter();
    });
    selectedContainer.appendChild(ingredient);
    searchBox.value = "";
    displayIngredientFilter();
  }
}

function newIngredient(name: string) {
  let fdc_id = (
    document.querySelector(
      `#recipe-ingredients > option[value='${name}']`,
    ) as HTMLOptionElement
  ).dataset.fdcId;

  let div = document.createElement("div");
  div.classList.add("checkbox");

  let input = document.createElement("input");
  input.setAttribute("type", "checkbox");
  input.setAttribute("value", name);
  input.setAttribute("id", `ingredient-${name}`);
  input.setAttribute("name", `ingredient-${name}`);
  input.setAttribute("data-fdc-id", fdc_id);

  let label = document.createElement("label");
  label.setAttribute("for", `ingredient-${name}`);
  label.innerText = name + " ✕";

  div.appendChild(input);
  div.appendChild(label);
  return div;
}

function displayIngredientFilter() {
  let selections: NodeListOf<HTMLInputElement> = document.querySelectorAll(
    "#selected-ingredients input",
  );
  let fdcIds: number[] = [...selections].map((el) => Number(el.dataset.fdcId));
  //storeSearch("#dialog-source", [pattern]);

  if (fdcIds.length > 0) {
    var filteredRecipes: SearchResult[] = recipe_data.recipes.filter(
      (recipe) => {
        let intersection = fdcIds.filter((id) =>
          recipe.ingredients.includes(id),
        );
        return intersection.length == fdcIds.length;
      },
    );
    displayResults(
      filteredRecipes,
      document.querySelector("#result-ingredients"),
    );
  } else {
    clearResults(document.querySelector("#result-ingredients"));
  }
}

// Offline cache status
enum CacheStatus {
  None,
  Update,
  Current,
}

/**
 * Check the status of the offline cache of the site
 * Return CachceStatus enum value
 */
function checkOfflineCacheStatus() {
  let lastUpdateHash = localStorage.getItem("last-update-hash");
  if (lastUpdateHash == null) {
    // Never done a full site cache
    return CacheStatus.None;
  }

  let lastUpdate: HTMLMetaElement = <HTMLMetaElement>(
    document.querySelector("meta[name='update-hash']")
  );
  if (lastUpdate.content != lastUpdateHash) {
    // Last site update was more recent that last cache, so update is available
    return CacheStatus.Update;
  }
  // Cache is current
  return CacheStatus.Current;
}

/**
 * Cache entire site for offline viewing
 * @param {SearchResult[]} recipes Array of SearchResult objects
 */
function cacheSite(recipes: SearchResult[]) {
  let urls: string[] = recipes
    .map((item) => {
      return [`/recipes/${item.slug}`, `/images/${item.slug}.webp`];
    })
    .flat();
  // Add some extra static urls
  urls.push("/js/search_data.min.js");
  urls.push("/all-recipes");
  urls.push("/random");

  // Add all urls to cache.
  // If there are multiple caches, use the last one beacuse caches
  // are named sequentially so the last one is the latest.
  caches
    .keys()
    .then((cacheKeys) => {
      return cacheKeys[cacheKeys.length - 1];
    })
    .then((key) => {
      caches.open(key).then((cache) => {
        cache.addAll(urls);
      });
    })
    .then(() => {
      // Store last site update date
      let lastUpdate: HTMLMetaElement = <HTMLMetaElement>(
        document.querySelector("meta[name='update-hash']")
      );
      localStorage.setItem("last-update-hash", lastUpdate.content);
      // Update cache button
      updateCacheButton();
    });
}

/**
 * Update cache button text according to offline cache status
 */
async function updateCacheButton() {
  // Check if sufficient quote is available for cache. If not, abort
  let cacheEstimate = await navigator.storage.estimate();
  let quota = cacheEstimate.quota;
  let cacheSize: number = Number(
    (document.querySelector("meta[name='cache-size']") as HTMLMetaElement)
      .content,
  );
  if (cacheSize > quota) {
    return;
  }
  // Make cache section visible - if this runs, javascript must be enabled.
  let cacheBtn: HTMLButtonElement = document.querySelector("#cache-site-btn");
  let cacheSizeMB = Math.ceil(cacheSize / 1e6).toString();
  let cacheStatus = checkOfflineCacheStatus();

  if (cacheStatus == CacheStatus.None) {
    cacheBtn.innerHTML = `Cache site<br><span class="light-text">${cacheSizeMB} MB</span>`;
    cacheBtn.disabled = false;
  } else if (cacheStatus == CacheStatus.Update) {
    cacheBtn.innerHTML = `Update cache<br><span class="light-text">${cacheSizeMB} MB</span>`;
    cacheBtn.disabled = false;
  } else {
    cacheBtn.innerHTML = `Available<br>offline`;
    cacheBtn.disabled = true;
  }
}

/**
 * Toggle theme between auto, dark, light in order
 */
function toggleTheme() {
  let themeBtnText: HTMLSpanElement = document.querySelector(
    "#theme-btn .light-text",
  );
  if (themeBtnText.innerText == "Auto") {
    applyTheme("Dark");
  } else if (themeBtnText.innerText == "Dark") {
    applyTheme("Light");
  } else if (themeBtnText.innerText == "Light") {
    applyTheme("Auto");
  }
}

/**
 * Set theme between to auto, dark or light
 */
function applyTheme(theme: string) {
  let html = document.querySelector("html");
  let themeBtnText: HTMLSpanElement = document.querySelector(
    "#theme-btn .light-text",
  );
  if (theme == "Dark") {
    // Set to dark theme
    localStorage.setItem("light-theme", "false");
    html.classList.remove("light-theme");
    themeBtnText.innerText = "Dark";
  } else if (theme == "Light") {
    // Set to light theme
    localStorage.setItem("light-theme", "true");
    html.classList.add("light-theme");
    themeBtnText.innerText = "Light";
  } else if (theme == "Auto") {
    applyAutoTheme();
  }
}

/**
 * Set theme based on prefers-color-scheme property
 */
function applyAutoTheme() {
  let html = document.querySelector("html");
  let themeBtnText: HTMLSpanElement = document.querySelector(
    "#theme-btn .light-text",
  );
  themeBtnText.innerText = "Auto";
  if (window.matchMedia("(prefers-color-scheme: light)").matches) {
    // Set to light theme and remove local storage property
    localStorage.removeItem("light-theme");
    html.classList.add("light-theme");
  } else {
    // Set to dark theme and remove local storage property
    localStorage.removeItem("light-theme");
    html.classList.remove("light-theme");
  }
}

/**
 * Store search details in session storage so it can be restored
 * @param {string}   modalID ID of modal of last search
 * @param {string[]} values  Values of last search
 */
function storeSearch(modalID: string, values: string[]) {
  sessionStorage.setItem("search-modalID", modalID);
  sessionStorage.setItem("search-values", JSON.stringify(values));
}

/**
 * Restore previous search using data stored in session storage
 */
function restoreSearch() {
  let modalID = sessionStorage.getItem("search-modalID");
  if (modalID == null || modalID == "") {
    return;
  }

  let values = JSON.parse(sessionStorage.getItem("search-values"));
  if (values == null || values.length == 0) {
    return;
  }

  let modal: HTMLDialogElement = document.querySelector(modalID);
  modal.showModal();

  if (modalID == "#dialog-name" || modalID == "#dialog-source") {
    let input = modal.querySelector("input");
    input.value = values[0];
    input.dispatchEvent(new Event("input"));
  } else {
    for (const v of values) {
      let checkbox: HTMLInputElement = modal.querySelector(
        `input[value="${v}"`,
      );
      checkbox.checked = true;
    }
    switch (modalID) {
      case "#dialog-category":
        displayCategoryFilter();
        break;
      case "#dialog-cuisine":
        displayCuisineFilter();
        break;
      case "#dialog-tag":
        displayTagFilter();
        break;
    }
  }
}
