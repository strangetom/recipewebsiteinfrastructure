"use strict";

/* A version number is useful when updating the worker logic,
   allowing you to remove outdated cache entries during the update.
*/
const VERSION = "v37::";

/* These resources will be downloaded and cached by the service worker
   during the installation process. If any resource fails to be downloaded,
   then the service worker won't be installed either.
*/
const OFFLINE_FUNDAMENTALS = [
  "/",
  "/offline.html",
  "/404.html",
  "/css/colours.min.css",
  "/css/index.min.css",
  "/css/offline.min.css",
  "/css/recipe.min.css",
  "/css/all.min.css",
  "/css/atom.xsl",
  "/js/flexsearch.light.js",
  "/js/scale_timers.min.js",
  "/js/search.min.js",
  "/img/logo.svg",
  "/img/page-bg-light.svg",
  "/img/page-bg-dark.svg",
  "/img/search.svg",
  "/img/timer-reset.svg",
  "/img/timer-start.svg",
  "/img/timer-close.svg",
  "/img/cart.svg",
  "/img/cart-fill.svg",
  "/img/notification.svg",
  "/img/notification-disabled.svg",
  "/fonts/latin-modern-mono-prop-light-10-regular-latin.woff2",
  "/fonts/latin-modern-mono-prop-light-10-regular-latin-ext.woff2",
];

/* The install event fires when the service worker is first installed.
   You can use this event to prepare the service worker to be able to serve
   files while visitors are offline.
*/
self.addEventListener("install", function (event) {
  /* Using event.waitUntil(p) blocks the installation process on the provided
     promise. If the promise is rejected, the service worker won't be installed.
  */
  event.waitUntil(
    /* The caches built-in is a promise-based API that helps you cache responses,
       as well as finding and deleting them.
    */
    caches
      /* You can open a cache by name, and this method returns a promise. We use
         a versioned cache name here so that we can remove old cache entries in
         one fell swoop later, when phasing out an older service worker.
      */
      .open(VERSION + "cached")
      .then(function (cache) {
        /* After the cache is opened, we can fill it with the offline fundamentals.
           The method below will add all resources in `offlineFundamentals` to the
           cache, after making requests for them.
        */
        return cache.addAll(OFFLINE_FUNDAMENTALS);
      })
  );
});

/* The fetch event fires whenever a page controlled by this service worker requests
   a resource. This isn't limited to `fetch` or even XMLHttpRequest. Instead, it
   comprehends even the request for the HTML page on first load, as well as JS and
   CSS resources, fonts, any images, etc.
*/
self.addEventListener("fetch", function (event) {
  /* We should only cache http GET requests, and deal with the rest of method
     in the client-side, by handling failed POST,PUT,PATCH,etc. requests.
  */
  if (event.request.method !== 'GET' || !event.request.url.startsWith('http')) {
    /* If we don't block the event as shown below, then the request will go to
       the network as usual.
    */
    return;
  }

  if (event.request.mode == "navigate") {
    /*
    Respond to fetch navigate events with a time limited network call. 
    If the network fetch takes longer than 4 seconds, fallback to serving the resource from the cache
    */
    event.respondWith(
      fromNetwork(event.request, 4000).catch(() => {
        return fromCache(event.request);
      })
    );
  } else {
    /*
    Respond to all other fetch events from cache, falling back to network is there's a cache miss
    */
    event.respondWith(
      caches.match(event.request).then((cached) => {
        let networked = fromNetwork(event.request, 4000).catch( () => {
          return fromCache(event.request);
        });
        return cached || networked;
      })
    );
  }
});

/* Time limited network request. If the network fails or the response is not
   served before timeout, the promise is rejected.
*/
function fromNetwork(request, timeout) {
  return new Promise(function (fulfill, reject) {
    // Reject in case of timeout.
    let timeoutId = setTimeout(reject, timeout);
    // Fulfill and store in cache in case of success.
    fetch(request).then(function (response) {
      clearTimeout(timeoutId);
      /* We copy the response before fulfilling the promise.
         This is the response that will be stored on the ServiceWorker cache.
      */
      let cacheCopy = response.clone();
      caches
        // We open a cache to store the response for this request.
        .open(VERSION + "cached")
        .then(function add(cache) {
          /* We store the response for this request. It'll later become
             available to caches.match(event.request) calls, when looking
             for cached responses.
          */
          cache.put(request, cacheCopy);
        });
      fulfill(response);
      // Reject also if network fetch rejects.
    }, reject);
  });
}

/* Open the cache where the assets were stored and search for the requested
   resource. If the resource isn't found, then return offline page
*/
function fromCache(request) {
  return caches.open(VERSION + "cached").then(function (cache) {
    return cache.match(request).then(function (matching) {
      // If no matching html, return offline page. For all other mimetypes, return undefined if no match.
      if (request.headers.get('Accept').includes('text/html')) {
        return matching || caches.match("offline.html");
      }else{
        return matching;
      }
    });
  });
}

/* The activate event fires after a service worker has been successfully installed.
   It is most useful when phasing out an older version of a service worker, as at
   this point you know that the new worker was installed correctly. In this example,
   we delete old caches that don't match the version in the worker we just finished
   installing.
*/
self.addEventListener("activate", function (event) {
  /* Just like with the install event, event.waitUntil blocks activate on a promise.
     Activation will fail unless the promise is fulfilled.
  */
  event.waitUntil(
    caches
      /* This method returns a promise which will resolve to an array of available
         cache keys.
      */
      .keys()
      .then(function (keys) {
        // We return a promise that settles when all outdated caches are deleted.
        return Promise.all(
          keys
            .filter(function (key) {
              // Filter by keys that don't start with the latest version prefix.
              return !key.startsWith(VERSION);
            })
            .map(function (key) {
              /* Return a promise that's fulfilled
                 when each outdated cache is deleted.
              */
              return caches.delete(key);
            })
        );
      })
  );
});

/* Timers can create notifications if the user chooses. 
By default clicking a notification does nothing, so add a callback to do something
*/
self.addEventListener("notificationclick", function (event) {
  const notification = event.notification;
  // Close notification on click
  notification.close();
  // and open url that created the notification
  event.waitUntil(
    // Find all the current tabs that are open
    clients.matchAll({ type: "window" }).then((clientsArray) => {
      // Find out if any current tab is the url we want and focus it if it exists
      for (let c of clientsArray) {
        if (c.url === notification.data.url) {
          return c.focus();
        }
      }
      // Otherwise open a new tab
      return clients.openWindow(notification.data.url);
    })
  );
});
