import re
import unicodedata
from functools import lru_cache
from typing import Any

import lxml.etree as ET

from builder.exceptions import (
    InvalidBoldNumberError,
    InvalidTemperatureError,
    InvalidTimerError,
)

# <, > {, }, [, ] are the tokens to split on because they represent special things
TOKENISER = re.compile(r"([<>{}\[\]])")

# Regex pattern to match parts of the timer markup
# ([\d\-\.\u00BC-\u00BE\u2150-\u215E]+) matches a number, unicode fraction
# or range between angular brackets e.g. 2 or 4-5 or ¾
# Note: \u00BC-\u00BE is ¼,½,¾
# Note: \u2150-\u215E is ⅐, ⅑, ⅒, ⅓, ⅔, ⅕, ⅖, ⅗, ⅘, ⅙, ⅚, ⅛, ⅜, ⅝, ⅞
# (.*?) matches the minimum number of any characters followed by a space.
# This is the timer unit, e.g. seconds, minutes
# (\@.*?)$ matches an @ symbol followed by anynumber of characters up to the
# end of the string. This is the timer label.
TIMER_PATTERN = re.compile(r"(([\d\-\.\u00BC-\u00BE\u2150-\u215E]+)\s(.*?)(\@.*?)?)$")

# Regex pattern to match parts of the temperature markup
# (\d+) matches a number
# ([CF]) matches the letter C or F, the temperature unit
TEMPERATURE_PATTERN = re.compile(r"((\d+)°([CF]))$")


def _clock_svg() -> ET.Element:
    """Return svg element for clock icon

    Returns
    -------
    ET.Element
        SVG element
    """

    svg = ET.Element(
        "svg",
        attrib={
            "width": "16",
            "height": "16",
            "xmlns": "http://www.w3.org/2000/svg",
        },
    )
    path_1 = ET.Element(
        "path",
        attrib={
            "d": "M8.5 5.6a.5.5 0 10-1 0v2.9h-3a.5.5 0 000 1H8a.5.5 0 00.5-.5V5.6z",
            "fill": "currentColor",
        },
    )
    path_2 = ET.Element(
        "path",
        attrib={
            "d": "M6.5 1A.5.5 0 017 .5h2a.5.5 0 010 1v.57c1.36.196 2.594.78 3.584 1.64a.715.715 0 01.012-.013l.354-.354-.354-.353a.5.5 0 01.707-.708l1.414 1.415a.5.5 0 11-.707.707l-.353-.354-.354.354a.512.512 0 01-.013.012A7 7 0 117 2.071V1.5a.5.5 0 01-.5-.5zM8 3a6 6 0 10.001 12A6 6 0 008 3z",
            "fill": "currentColor",
        },
    )

    svg.append(path_1)
    svg.append(path_2)

    div = ET.Element("div", attrib={"class": "toast-btn-icon"})
    div.append(svg)
    return div


CLOCK_SVG = _clock_svg()


class BoldNumberNode:
    """Bold number node for recipe instructions

    Attributes
    ----------
    text : str
        Text for bold number
    """

    def __init__(self, text: str):
        self.text = text

    def __repr__(self):
        return f"BoldNumberNode(text='{self.text}')"

    def markup(self):
        """Generate HTM markup for bold number

        Returns
        -------
        str
            HTML markup
        """
        return f"<b>{self.text}</b>"


class TimerNode:
    """Timer node for recipe instructions

    Attributes
    ----------
    label : str
        Optional label for timer
    text : str
        Text for timer
    time : str
        Time for time in unit
    time_seconds : int
        Timer time in seconds
    unit : str
        Unit for timer
    """

    def __init__(self, text: str):
        self.text = text
        self._parse_text(text)

    def __repr__(self):
        return f"TimerNode(time='{self.time}', unit='{self.unit}', label='{self.label}', seconds='{self.time_seconds}')"

    def _parse_text(self, text: str):
        """Parse timer text and extract time, unit and label

        Parameters
        ----------
        text : str
            Text to parse
        """
        match = TIMER_PATTERN.match(text)
        if match is None:
            raise InvalidTimerError(f"Timer is wrong format: {self.text}")

        groups = match.groups()

        self.time = groups[1]
        self.unit = groups[2]
        self.time_seconds = _convert_time_to_seconds(self.time, self.unit)
        self.label = (
            groups[3] if groups[3] is None else groups[3].replace("@", "")
        )  # Strip off '@' if there is a label

    def markup(self) -> str:
        """Generate HTML markup for timer

        Returns
        -------
        str
            String of HTML markup
        """
        return _create_button_html(
            self.time_seconds, self.label or "", self.time, self.unit
        )


@lru_cache(maxsize=None)
def _create_button_html(time_seconds: str, label: str, time: str, unit: str) -> str:
    """Create html markup for time button
    <button class="toast-btn" data-time="..." data-label="..." title="Start timer">
        <svg>...</svg>
    </button>

    Parameters
    ----------
    time_seconds : str
        Time for timer in seconds
    label : str
        Label for timer
    time : str
        Time value to display on button
    unit : str
        Time units to display on button

    Returns
    -------
    str
        HTML markup for button
    """
    button = ET.Element(
        "button",
        attrib={
            "class": "toast-btn",
            "data-time": str(time_seconds),
            "data-label": label or "",
            "title": "Start timer",
        },
    )

    button.append(CLOCK_SVG)

    time_value = ET.Element("b")
    time_value.text = time
    time_value.tail = " " + unit

    text = ET.Element("span")
    text.append(time_value)
    button.append(text)

    return ET.tostring(button, encoding="unicode", method="html")


@lru_cache(maxsize=None)
def _convert_time_to_seconds(time: str, units: str) -> int:
    """Convert time to number of seconds. If the time is a range, take the lower bound

    Parameters
    ----------
    time : str
        Time as a string. May be an integer, a range or a fraction
    units : str
        Units, not plural

    Returns
    -------
    int
        Time in seconds
    """
    if "-" in time:
        # If a time range, take lower bound
        time = time.split("-")[0]

    # Convert unicode fractions to decimal
    if len(time) == 1 and unicodedata.name(time).startswith("VULGAR FRACTION"):
        # Single unicode fraction
        time = str(unicodedata.numeric(time))
    elif len(time) > 1 and unicodedata.name(time[-1]).startswith("VULGAR FRACTION"):
        # Unicode fraction preceeded by integer
        time = str(float(time[:-1]) + unicodedata.numeric(time[-1]))

    if units in ["second", "seconds"]:
        return int(time)
    elif units in ["minute", "min", "minutes", "mins"]:
        return int(float(time) * 60)
    elif units in ["hour", "hours"]:
        return int(float(time) * 3600)

    return 0


class TemperatureNode:
    """Temperature node for recipe instructions

    Attributes
    ----------
    temperature : int
        Temperature value
    text : str
        Text for temperature
    unit : str
        Temperature unit
    """

    def __init__(self, text: str):
        self.text = text
        self._parse_text(text)

    def __repr__(self):
        return f"TemperatureNode(value='{self.temperature}, unit='{self.unit}')"

    def _parse_text(self, text: str):
        """Parse temperature value and unit

        Parameters
        ----------
        text : str
            Text to parse
        """
        match = TEMPERATURE_PATTERN.match(text)
        if match is None:
            raise InvalidTemperatureError(f"Temperature is wrong format: {self.text}")

        groups = match.groups()

        self.temperature = groups[1]
        self.unit = groups[2]

    def markup(self) -> str:
        """Generate HTML markup for timer

        Returns
        -------
        str
            String of HTML markup
        """
        return _create_temperature_html(self.temperature, self.unit)


@lru_cache(maxsize=None)
def _create_temperature_html(value: int, unit: str) -> str:
    """Create html markup for temperature element
    <span class="temperature" data-value="..." data-unit="...">
        <b>...<b>&deg;C
    </span>
    """
    span = ET.Element(
        "span",
        attrib={"class": "temperature", "data-value": str(value), "data-unit": unit},
    )
    bold = ET.Element("b")
    bold.text = value
    bold.tail = f"&deg;{unit}"
    span.append(bold)

    # Be default the & get's escaped, so restore the original version after
    # converting to string.
    return ET.tostring(span, encoding="unicode", method="html").replace("&amp;", "&")


class TextNode:
    """Text node in recipe instructions

    Attributes
    ----------
    text : str
        Text of text noe
    """

    def __init__(self, text: str):
        self.text = text

    def __repr__(self):
        return f"TextNode(text='{self.text}')"

    def add_ingredient_tooltips(self, ingredients: list[dict[str, str]]) -> None:
        """Insert HTML elements into text to add tooltops with ingredients quantity
        when hovering over ingedients in text

        Parameters
        ----------
        ingredients : list[dict[str, str]]
            Ingredients list for recipe
        """

        for ingred in ingredients:
            # if ingredient has a synonym, use that for matching in text
            # This works beacuse "" is False-y
            ingredient = ingred["synonym"] or ingred["name"]

            if ingredient in self.text:
                # if ingredient found in text, replace with tooltip HTML
                quantity = ingred["quantity"]
                formatted = ingred["formatted_quantity"]
                unit = ingred["unit"]
                """
                (?<![>\"]) is a negative lookbehind that matches > and "
                (?![<\"]) is a negative lookahead that matches < and "
                """
                regex = build_regex(ingredient)
                self.text = regex.sub(
                    f'<span data-default="{quantity}" data-defaultunit="{unit}" data-quantity="{formatted}" data-unit="{unit}" class="tooltip scalable">{ingredient}</span>',
                    self.text,
                )

    def markup(self):
        """Return markup for text node

        Returns
        -------
        str
            Text node text
        """
        return self.text


@lru_cache(maxsize=None)
def build_regex(ingredient: str) -> re.Pattern:
    """
    Compile regex pattern for ingredient annotation.
    This function is cached so regexes can be reused.
    (?<![>\"]) is a negative lookbehind that matches > and "
    (?![<\"]) is a negative lookahead that matches < and "

    Parameters
    ----------
    ingredient : str
        Ingredient string to annotate

    Returns
    -------
    re.Pattern
        Compiled regular expression
    """
    return re.compile(rf"(?<![>\"])\b{ingredient}\b(?![<\"])")


def annotate_instruction(text: str, ingredients: list[Any]) -> str:
    """Annotate instructions text with HTML markup for bold number,
    timers and ingredient tooltips.

    Parameters
    ----------
    text : str
        Text of recipe instruction step
    ingredients : list[Any]
        List of ingredients for recipe

    Returns
    -------
    str
        Instruction text with HTML markup added
    """
    tokenised_instruction = TOKENISER.split(text)

    timer_start_indices, timer_end_indices = [], []
    if "{" in tokenised_instruction:
        timer_start_indices = [
            i for i, token in enumerate(tokenised_instruction) if token == "{"
        ]
        timer_end_indices = [
            i for i, token in enumerate(tokenised_instruction) if token == "}"
        ]

    bold_numbers_start_indices, bold_numbers_end_indices = [], []
    if "<" in tokenised_instruction:
        bold_numbers_start_indices = [
            i for i, token in enumerate(tokenised_instruction) if token == "<"
        ]
        bold_numbers_end_indices = [
            i for i, token in enumerate(tokenised_instruction) if token == ">"
        ]

    temp_start_indices, temp_end_indices = [], []
    if "[" in tokenised_instruction:
        temp_start_indices = [
            i for i, token in enumerate(tokenised_instruction) if token == "["
        ]
        temp_end_indices = [
            i for i, token in enumerate(tokenised_instruction) if token == "]"
        ]

    nodes = []
    i = 0
    while i < len(tokenised_instruction):
        if i in timer_start_indices:
            try:
                timer_index = timer_start_indices.index(i)
                timer_end = timer_end_indices[timer_index]
            except IndexError:
                timer = tokenised_instruction[i : i + 2]
                timer_str = "".join(timer).strip()[:20] + "..."
                raise InvalidTimerError(f"Mismatched timer brackets: '{timer_str}'")

            timer = tokenised_instruction[i + 1 : timer_end]
            timer_str = "".join(timer).strip()
            nodes.append(TimerNode(timer_str))

            i = timer_end + 1

        elif i in bold_numbers_start_indices:
            try:
                bn_index = bold_numbers_start_indices.index(i)
                bn_end = bold_numbers_end_indices[bn_index]
            except IndexError:
                bn = tokenised_instruction[i : i + 2]
                bn_str = "".join(bn).strip()[:10] + "..."
                raise InvalidBoldNumberError(
                    f"Mismatched bold number brackets: '{bn_str}'"
                )

            bn = tokenised_instruction[i + 1 : bn_end]
            bn_str = "".join(bn).strip()
            nodes.append(BoldNumberNode(bn_str))

            i = bn_end + 1

        elif i in temp_start_indices:
            try:
                temp_index = temp_start_indices.index(i)
                temp_end = temp_end_indices[temp_index]
            except IndexError:
                temp = tokenised_instruction[i + 1 : i + 2]
                temp_str = "".join(temp).strip()[:10] + "..."
                raise InvalidTemperatureError(
                    f"Mismatched temperature brackets: '{temp_str}'"
                )

            temp = tokenised_instruction[i + 1 : temp_end]
            temp_str = "".join(temp).strip()
            nodes.append(TemperatureNode(temp_str))

            i = temp_end + 1

        else:
            text_node = TextNode(tokenised_instruction[i])
            text_node.add_ingredient_tooltips(ingredients)
            nodes.append(text_node)
            i = i + 1

    return "".join(node.markup() for node in nodes)


def select_annotatable_ingredients(
    ingredients: list[dict[str, Any]],
) -> list[dict[str, str]]:
    """Select all ingredients that can be annoted in instructions and return
    as a flat list.
    Annotatable ingredients are those that have a quantity or a unit, or both.
    Return as a sorted list, with the longest ingredient first

    Parameters
    ----------
    ingredients : list[Any]
        List of recipe ingredients.

    Returns
    -------
    list[Dict[str, Any]]
        Flattened, sorted list of annotatable ingredients
    """
    # Flatten ingredients list
    ingredients = flatten_ingredients(ingredients)

    # Filter out any ingredients with no quantity and no unit
    ingredients = [i for i in ingredients if i["quantity"] != "" or i["unit"] != ""]

    # Order list with longest ingredient first
    ingredients = sorted(ingredients, key=ingredient_length, reverse=True)

    return ingredients


def flatten_ingredients(ingredients: list[dict[str, Any]]) -> list[dict[str, str]]:
    """Flatten ingedients list from a mixed list of dicts of single ingredients
    and groups of ingredients, to a list of dicts of single ingredients only

    Parameters
    ----------
    ingredients : list[dict[str, Any]]
        Mixed list of dicts of single ingredients and dicts groups of ingredients

    Returns
    -------
    list[dict[str, str]]
        Flat tist containing all dict element and subelements

    """
    flat = []
    for item in ingredients:
        if "group" in item.keys():
            flat.extend(item["items"])
        else:
            flat.append(item)
    return flat


def ingredient_length(ingredient: dict[str, str]) -> int:
    """Return length of ingredient used for tooltips

    Parameters
    ----------
    ingredient : dict[str, str]
        Dict of ingredient information

    Returns
    -------
    int
        Length of ingredient synonym or name

    """
    return len(ingredient["synonym"] or ingredient["name"])
