var recipe_data = {
    "recipes": [
        {
            "name": "Mangalore curry with kosambari",
            "image": "/img/placeholder-seafood.svg",
            "html": "/recipes/mangalore-curry-with-kosambari",
            "category": "Seafood",
            "yield": "4",
            "source": "The Spicery",
            "time": "1 hr",
            "cuisine": "Indian"
        },
        {
            "name": "Creamy roasted red pepper rigatoni pasta",
            "image": "/img/placeholder-veggie.svg",
            "html": "/recipes/creamy-roasted-red-pepper-rigatoni-pasta",
            "category": "Veggie",
            "yield": "6",
            "source": "themodernproper.com",
            "time": "20 mins"
        },
        {
            "name": "Ethopian doro wat",
            "image": "/images/ethopian-doro-wat.webp",
            "html": "/recipes/ethopian-doro-wat",
            "category": "Poultry",
            "yield": "4",
            "source": "The Spicery",
            "time": "50 mins",
            "cuisine": "Ethiopian"
        },
        {
            "name": "Zanzibari curry with date chutney",
            "image": "/images/zanzibari-curry-with-date-chutney.webp",
            "html": "/recipes/zanzibari-curry-with-date-chutney",
            "category": "Poultry",
            "yield": "4",
            "source": "The Spicery",
            "time": "1 hr 30 mins",
            "cuisine": "Zanzibari"
        },
        {
            "name": "Smoky rice with black beans, feta, corn & salsa",
            "image": "/images/smoky-rice-with-black-beans-feta-corn-and-salsa.webp",
            "html": "/recipes/smoky-rice-with-black-beans-feta-corn-and-salsa",
            "category": "Veggie",
            "yield": "4",
            "source": "Foolproof Veggie One-Pot: 60 Vibrant and Easy-Going Vegetarian Dishes",
            "time": "55 mins",
            "positive": true
        },
        {
            "name": "Basil-parmesan chicken and rice with tomatoes",
            "image": "/images/basil-parmesan-chicken-and-rice-with-tomatoes.webp",
            "html": "/recipes/basil-parmesan-chicken-and-rice-with-tomatoes",
            "category": "Poultry",
            "yield": "4",
            "source": "www.triedandtruerecipe.com",
            "time": "55 mins",
            "positive": true
        },
        {
            "name": "Chorizo, potato, sweetcorn, tomato & onion traybake",
            "image": "/images/chorizo-potato-sweetcorn-tomato-and-onion-traybake.webp",
            "html": "/recipes/chorizo-potato-sweetcorn-tomato-and-onion-traybake",
            "category": "Pork",
            "yield": "4",
            "source": "Persiana Everyday",
            "time": "1 hr 15 mins"
        },
        {
            "name": "Lavash bread",
            "image": "/images/lavash-bread.webp",
            "html": "/recipes/lavash-bread",
            "category": "Veggie",
            "yield": "12",
            "source": "www.themediterraneandish.com",
            "time": "1 hr 10 mins",
            "cuisine": "Middle Eastern",
            "tags": [
                "bread"
            ]
        },
        {
            "name": "Corn-ish pie",
            "image": "/images/corn-ish-pie.webp",
            "html": "/recipes/corn-ish-pie",
            "category": "Veggie",
            "yield": "4",
            "source": "David",
            "time": "45 mins"
        },
        {
            "name": "Pork tenderloin with cabbage",
            "image": "/images/pork-tenderloin-with-cabbage.webp",
            "html": "/recipes/pork-tenderloin-with-cabbage",
            "category": "Pork",
            "yield": "4",
            "source": "www.triedandtruerecipe.com",
            "time": "1 hr"
        },
        {
            "name": "Creamy hummus pasta bake",
            "image": "/images/creamy-hummus-pasta-bake.webp",
            "html": "/recipes/creamy-hummus-pasta-bake",
            "category": "Veggie",
            "yield": "4",
            "source": "www.pickuplimes.com",
            "time": "45 mins",
            "diet": [
                "Vegan"
            ]
        },
        {
            "name": "Sausage, mustard & apple hash",
            "image": "/images/sausage-mustard-apple-hash.webp",
            "html": "/recipes/sausage-mustard-apple-hash",
            "category": "Pork",
            "yield": "2",
            "source": "www.bbcgoodfood.com",
            "time": "35 mins"
        },
        {
            "name": "The best vegan chow mein",
            "image": "/images/the-best-vegan-chow-mein.webp",
            "html": "/recipes/the-best-vegan-chow-mein",
            "category": "Veggie",
            "yield": "3",
            "source": "www.pickuplimes.com",
            "time": "35 mins"
        },
        {
            "name": "Salmon tacos (mediterranean-style)",
            "image": "/images/salmon-tacos-mediterranean-style.webp",
            "html": "/recipes/salmon-tacos-mediterranean-style",
            "category": "Seafood",
            "yield": "4",
            "source": "www.themediterraneandish.com",
            "time": "33 mins",
            "cuisine": "Mediterranean"
        },
        {
            "name": "Parmesan chicken butties with red cabbage slaw",
            "image": "/images/parmesan-chicken-butties-with-red-cabbage-slaw.webp",
            "html": "/recipes/parmesan-chicken-butties-with-red-cabbage-slaw",
            "category": "Poultry",
            "yield": "2",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "25 mins"
        },
        {
            "name": "Sticky honey mustard posh dogs with chips",
            "image": "/images/sticky-honey-mustard-posh-dogs-with-chips.webp",
            "html": "/recipes/sticky-honey-mustard-posh-dogs-with-chips",
            "category": "RedMeat",
            "yield": "2",
            "source": "www.gousto.co.uk",
            "time": "35 mins",
            "cuisine": "American"
        },
        {
            "name": "Arroz chaufa",
            "image": "/images/arroz-chaufa.webp",
            "html": "/recipes/arroz-chaufa",
            "category": "Poultry",
            "yield": "5",
            "source": "tastesbetterfromscratch.com",
            "time": "15 mins",
            "cuisine": "Peruvian"
        },
        {
            "name": "Crispy cajun cauli tacos and slaw",
            "image": "/images/crispy-cajun-cauli-tacos-and-slaw.webp",
            "html": "/recipes/crispy-cajun-cauli-tacos-and-slaw",
            "category": "Veggie",
            "yield": "2",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "30 mins",
            "diet": [
                "DairyFree"
            ],
            "positive": true
        },
        {
            "name": "Pork chops with mushroom-scallion risotto",
            "image": "/images/pork-chops-with-mushroom-scallion-risotto.webp",
            "html": "/recipes/pork-chops-with-mushroom-scallion-risotto",
            "category": "Pork",
            "yield": "4",
            "source": "www.triedandtruerecipe.com",
            "time": "1 hr 10 mins"
        },
        {
            "name": "Sage butter, feta & black pepper pasta",
            "image": "/images/sage-butter-feta-and-black-pepper-pasta.webp",
            "html": "/recipes/sage-butter-feta-and-black-pepper-pasta",
            "category": "Veggie",
            "yield": "2",
            "source": "Persiana Everyday",
            "time": "20 mins"
        },
        {
            "name": "Sheet-pan chicken and potatoes with feta, lemon and dill",
            "image": "/images/sheet-pan-chicken-and-potatoes-with-feta-lemon-and-dill.webp",
            "html": "/recipes/sheet-pan-chicken-and-potatoes-with-feta-lemon-and-dill",
            "category": "Poultry",
            "yield": "4",
            "source": "cooking.nytimes.com",
            "time": "45 mins"
        },
        {
            "name": "Brown sugar shortbread",
            "image": "/images/brown-sugar-shortbread.webp",
            "html": "/recipes/brown-sugar-shortbread",
            "category": "Dessert",
            "yield": "2",
            "source": "sallysbakingaddiction.com",
            "time": "4 hr 45 mins"
        },
        {
            "name": "Meatball & mushroom stroganoff",
            "image": "/images/meatball-and-mushroom-stroganoff.webp",
            "html": "/recipes/meatball-and-mushroom-stroganoff",
            "category": "RedMeat",
            "yield": "4",
            "source": "Persiana Everyday",
            "time": "1 hr 5 mins",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Grilled corn salad",
            "image": "/images/grilled-corn-salad.webp",
            "html": "/recipes/grilled-corn-salad",
            "category": "Veggie",
            "yield": "6",
            "source": "rainbowplantlife.com",
            "time": "25 mins"
        },
        {
            "name": "Enchiladas con carne",
            "image": "/images/enchiladas-con-carne.webp",
            "html": "/recipes/enchiladas-con-carne",
            "category": "RedMeat",
            "yield": "6",
            "source": "cooking.nytimes.com",
            "time": "2 hr 30 mins",
            "cuisine": "Mexican",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Spicy couscous recipe with shrimp and chorizo",
            "image": "/images/spicy-couscous-recipe-with-shrimp-and-chorizo.webp",
            "html": "/recipes/spicy-couscous-recipe-with-shrimp-and-chorizo",
            "category": "Seafood",
            "yield": "4-6",
            "source": "www.themediterraneandish.com",
            "time": "25 mins",
            "cuisine": "Spanish"
        },
        {
            "name": "Ouzi \u2014 phyllo pastries with meat and rice filling",
            "image": "/images/ouzi-phyllo-pastries-with-meat-and-rice-filling.webp",
            "html": "/recipes/ouzi-phyllo-pastries-with-meat-and-rice-filling",
            "category": "RedMeat",
            "yield": "10",
            "source": "Sumac",
            "time": "1 hr 10 mins",
            "cuisine": "Syrian",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Quick pesto couscous salad",
            "image": "/images/quick-pesto-couscous-salad.webp",
            "html": "/recipes/quick-pesto-couscous-salad",
            "category": "Veggie",
            "yield": "3",
            "source": "www.pickuplimes.com",
            "time": "10 mins",
            "diet": [
                "Vegan"
            ]
        },
        {
            "name": "Hot italian stromboli",
            "image": "/images/hot-italian-stromboli.webp",
            "html": "/recipes/hot-italian-stromboli",
            "category": "Pork",
            "yield": "2",
            "source": "www.halfbakedharvest.com",
            "time": "40 mins",
            "positive": true
        },
        {
            "name": "Pull-apart meatball sliders",
            "image": "/images/pull-apart-meatball-sliders.webp",
            "html": "/recipes/pull-apart-meatball-sliders",
            "category": "RedMeat",
            "yield": "6",
            "source": "www.bbcgoodfood.com",
            "time": "45 mins",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "20 minute ginger pineapple chicken stir fry",
            "image": "/images/20-minute-ginger-pineapple-chicken-stir-fry.webp",
            "html": "/recipes/20-minute-ginger-pineapple-chicken-stir-fry",
            "category": "Poultry",
            "yield": "6",
            "source": "www.halfbakedharvest.com",
            "time": "20 mins"
        },
        {
            "name": "Stuffed energy balls",
            "image": "/images/stuffed-energy-balls.webp",
            "html": "/recipes/stuffed-energy-balls",
            "category": "Dessert",
            "yield": "8",
            "source": "www.pickuplimes.com",
            "time": "15 mins",
            "diet": [
                "Vegan",
                "GlutenFree"
            ]
        },
        {
            "name": "All-in-one fish, chickpea & spinach stew",
            "image": "/images/all-in-one-fish-chickpea-and-spinach-stew.webp",
            "html": "/recipes/all-in-one-fish-chickpea-and-spinach-stew",
            "category": "Seafood",
            "yield": "4",
            "source": "Foolproof One-Pot: 60 Simple and Satisfying Recipes",
            "time": "1 hr 20 mins"
        },
        {
            "name": "Herby olive italian orzo salad",
            "image": "/images/herby-olive-italian-orzo-salad.webp",
            "html": "/recipes/herby-olive-italian-orzo-salad",
            "category": "Veggie",
            "yield": "6",
            "source": "www.ambitiouskitchen.com",
            "time": "30 mins",
            "cuisine": "Italian"
        },
        {
            "name": "Harissa & lemon roasted chicken thighs",
            "image": "/images/harissa-and-lemon-roasted-chicken-thighs.webp",
            "html": "/recipes/harissa-and-lemon-roasted-chicken-thighs",
            "category": "Poultry",
            "yield": "4-6",
            "source": "Persiana Everyday",
            "time": "50 mins"
        },
        {
            "name": "Aromatic prawn & sweetcorn rice",
            "image": "/images/aromatic-prawn-and-sweetcorn-rice.webp",
            "html": "/recipes/aromatic-prawn-and-sweetcorn-rice",
            "category": "Seafood",
            "yield": "2",
            "source": "Persiana Everyday",
            "time": "30 mins"
        },
        {
            "name": "Speedy lamb shawarma",
            "image": "/images/speedy-lamb-shawarma.webp",
            "html": "/recipes/speedy-lamb-shawarma",
            "category": "RedMeat",
            "yield": "2-4",
            "source": "Persiana Everyday",
            "time": "25 mins",
            "cuisine": "Middle Eastern",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Malaysian red curry noodles",
            "image": "/images/malaysian-red-curry-noodles.webp",
            "html": "/recipes/malaysian-red-curry-noodles",
            "category": "Veggie",
            "yield": "4",
            "source": "www.triedandtruerecipe.com",
            "time": "30 mins",
            "cuisine": "Malaysian"
        },
        {
            "name": "Braised indian chickpea stew",
            "image": "/images/braised-indian-chickpea-stew.webp",
            "html": "/recipes/braised-indian-chickpea-stew",
            "category": "Veggie",
            "yield": "6",
            "source": "rainbowplantlife.com",
            "time": "1 hr 50 mins",
            "diet": [
                "Vegan"
            ],
            "cuisine": "Indian"
        },
        {
            "name": "Salmon & spinach kouliabac",
            "image": "/images/salmon-and-spinach-kouliabac.webp",
            "html": "/recipes/salmon-and-spinach-kouliabac",
            "category": "Seafood",
            "yield": "4",
            "source": "Foolproof Freezer: 60 Fuss-Free Meals that Make the Most of Your Freezer",
            "time": "1 hr 10 mins",
            "cuisine": "Russian"
        },
        {
            "name": "Chicken tikka naanwhich",
            "image": "/images/chicken-tikka-naanwhich.webp",
            "html": "/recipes/chicken-tikka-naanwhich",
            "category": "Poultry",
            "yield": "4",
            "source": "Pinch of Nom Enjoy",
            "time": "20 mins"
        },
        {
            "name": "Honey-mustard bacon steaks with sweetcorn crush",
            "image": "/images/honey-mustard-bacon-steaks-with-sweetcorn-crush.webp",
            "html": "/recipes/honey-mustard-bacon-steaks-with-sweetcorn-crush",
            "category": "Pork",
            "yield": "4",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "25 mins"
        },
        {
            "name": "Sausage, lentil & fennel stew with creme fraiche & dijon mustard",
            "image": "/images/sausage-lentil-and-fennel-stew-with-creme-fraiche-and-dijon-mustard.webp",
            "html": "/recipes/sausage-lentil-and-fennel-stew-with-creme-fraiche-and-dijon-mustard",
            "category": "Pork",
            "yield": "4",
            "source": "Foolproof One-Pot: 60 Simple and Satisfying Recipes",
            "time": "1 hr 20 mins"
        },
        {
            "name": "Harissa baked cod with giant couscous, black chickpeas, carrots, coriander & lemon",
            "image": "/images/harissa-baked-cod-with-giant-couscous-black-chickpeas-carrots-coriander-and-lemon.webp",
            "html": "/recipes/harissa-baked-cod-with-giant-couscous-black-chickpeas-carrots-coriander-and-lemon",
            "category": "Seafood",
            "yield": "4",
            "source": "Foolproof One-Pot: 60 Simple and Satisfying Recipes",
            "time": "1 hr",
            "positive": true
        },
        {
            "name": "Creamy tomato orzo",
            "image": "/images/creamy-tomato-orzo.webp",
            "html": "/recipes/creamy-tomato-orzo",
            "category": "Veggie",
            "yield": "4",
            "source": "www.pickuplimes.com",
            "time": "20 mins",
            "diet": [
                "Vegan"
            ]
        },
        {
            "name": "Blueberry muffin crumb cake",
            "image": "/images/blueberry-muffin-crumb-cake.webp",
            "html": "/recipes/blueberry-muffin-crumb-cake",
            "category": "Dessert",
            "yield": "8",
            "source": "thedomesticrebel.com",
            "time": "1 hr 25 mins"
        },
        {
            "name": "Boston baked beans",
            "image": "/images/boston-baked-beans.webp",
            "html": "/recipes/boston-baked-beans",
            "category": "Pork",
            "yield": "4",
            "source": "Foolproof One-Pot: 60 Simple and Satisfying Recipes",
            "time": "5 hr 10 mins",
            "cuisine": "American"
        },
        {
            "name": "Mango and coriander salmon",
            "image": "/images/mango-and-coriander-salmon.webp",
            "html": "/recipes/mango-and-coriander-salmon",
            "category": "Seafood",
            "yield": "4",
            "source": "30 Minute Mowgli",
            "time": "30 mins",
            "positive": true
        },
        {
            "name": "Lazy potato curry",
            "image": "/images/lazy-potato-curry.webp",
            "html": "/recipes/lazy-potato-curry",
            "category": "Veggie",
            "yield": "2",
            "source": "Meat Free Mowgli",
            "time": "40 mins",
            "cuisine": "Indian"
        },
        {
            "name": "Turkish flatbread",
            "image": "/images/turkish-flatbread.webp",
            "html": "/recipes/turkish-flatbread",
            "category": "Veggie",
            "yield": "6",
            "source": "www.youtube.com",
            "time": "45 mins",
            "cuisine": "Turkish",
            "tags": [
                "bread"
            ]
        },
        {
            "name": "Oozy tuna and apple pasta bake",
            "image": "/images/oozy-tuna-and-apple-pasta-bake.webp",
            "html": "/recipes/oozy-tuna-and-apple-pasta-bake",
            "category": "Seafood",
            "yield": "4",
            "source": "30 Minute Mowgli",
            "time": "40 mins"
        },
        {
            "name": "Chacarero chileno (chilean steak and bean sandwiches)",
            "image": "/images/chacarero-chileno-chilean-steak-and-bean-sandwiches.webp",
            "html": "/recipes/chacarero-chileno-chilean-steak-and-bean-sandwiches",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.seriouseats.com",
            "time": "45 mins",
            "cuisine": "Chilean",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Angry bird",
            "image": "/images/angry-bird.webp",
            "html": "/recipes/angry-bird",
            "category": "Poultry",
            "yield": "4",
            "source": "Mowgli Street Food",
            "time": "45 mins"
        },
        {
            "name": "Godfather pasta",
            "image": "/images/godfather-pasta.webp",
            "html": "/recipes/godfather-pasta",
            "category": "Pork",
            "yield": "4",
            "source": "Mowgli Street Food",
            "time": "45 mins"
        },
        {
            "name": "Four-spice salmon",
            "image": "/images/four-spice-salmon.webp",
            "html": "/recipes/four-spice-salmon",
            "category": "Seafood",
            "yield": "4",
            "source": "cooking.nytimes.com",
            "time": "20 mins"
        },
        {
            "name": "Honey-mustard chicken tenders",
            "image": "/images/honey-mustard-chicken-tenders.webp",
            "html": "/recipes/honey-mustard-chicken-tenders",
            "category": "Poultry",
            "yield": "4",
            "source": "cooking.nytimes.com",
            "time": "45 mins",
            "positive": true
        },
        {
            "name": "Lebanese rice with vermicelli",
            "image": "/images/lebanese-rice-with-vermicelli.webp",
            "html": "/recipes/lebanese-rice-with-vermicelli",
            "category": "Veggie",
            "yield": "6",
            "source": "www.themediterraneandish.com",
            "time": "35 mins",
            "cuisine": "Lebanese"
        },
        {
            "name": "Baked cod with lemon and garlic",
            "image": "/images/baked-cod-with-lemon-and-garlic.webp",
            "html": "/recipes/baked-cod-with-lemon-and-garlic",
            "category": "Seafood",
            "yield": "5",
            "source": "www.themediterraneandish.com",
            "time": "22 mins",
            "cuisine": "Mediterranean"
        },
        {
            "name": "Steak and chips pie",
            "image": "/images/steak-and-chips-pie.webp",
            "html": "/recipes/steak-and-chips-pie",
            "category": "RedMeat",
            "yield": "4",
            "source": "Pinch of Nom Comfort Food",
            "time": "4 hr 5 mins",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Easy honey mustard salmon",
            "image": "/images/easy-honey-mustard-salmon.webp",
            "html": "/recipes/easy-honey-mustard-salmon",
            "category": "Seafood",
            "yield": "6",
            "source": "www.themediterraneandish.com",
            "time": "30 mins"
        },
        {
            "name": "Vegan red lentil curry",
            "image": "/images/vegan-red-lentil-curry.webp",
            "html": "/recipes/vegan-red-lentil-curry",
            "category": "Veggie",
            "yield": "4",
            "source": "rainbowplantlife.com",
            "time": "45 mins",
            "diet": [
                "Vegan"
            ],
            "cuisine": "Indian"
        },
        {
            "name": "Barbari bread",
            "image": "/images/barbari-bread.webp",
            "html": "/recipes/barbari-bread",
            "category": "Veggie",
            "yield": "10",
            "source": "www.themediterraneandish.com",
            "time": "45 mins",
            "cuisine": "Middle Eastern",
            "tags": [
                "bread"
            ]
        },
        {
            "name": "Ginger-lime chicken",
            "image": "/images/ginger-lime-chicken.webp",
            "html": "/recipes/ginger-lime-chicken",
            "category": "Poultry",
            "yield": "4",
            "source": "cooking.nytimes.com",
            "time": "15 mins"
        },
        {
            "name": "Simple pancetta pasta with peas and parmesan",
            "image": "/images/simple-pancetta-pasta-with-peas-and-parmesan.webp",
            "html": "/recipes/simple-pancetta-pasta-with-peas-and-parmesan",
            "category": "Pork",
            "yield": "4",
            "source": "www.themediterraneandish.com",
            "time": "30 mins",
            "cuisine": "Italian"
        },
        {
            "name": "Coconut curry salmon",
            "image": "/images/coconut-curry-salmon.webp",
            "html": "/recipes/coconut-curry-salmon",
            "category": "Seafood",
            "yield": "4",
            "source": "www.delish.com",
            "time": "30 mins"
        },
        {
            "name": "No churn ice cream",
            "image": "/images/no-churn-ice-cream.webp",
            "html": "/recipes/no-churn-ice-cream",
            "category": "Dessert",
            "yield": "6",
            "source": "www.youtube.com",
            "time": "10 mins"
        },
        {
            "name": "Key lime loaf cake",
            "image": "/images/key-lime-loaf-cake.webp",
            "html": "/recipes/key-lime-loaf-cake",
            "category": "Dessert",
            "yield": "12",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "1 hr 20 mins",
            "diet": [
                "GlutenFree"
            ],
            "cuisine": "American",
            "tags": [
                "cake"
            ]
        },
        {
            "name": "BBQ cauliflower and chickpea tray bake",
            "image": "/images/bbq-cauliflower-and-chickpea-tray-bake.webp",
            "html": "/recipes/bbq-cauliflower-and-chickpea-tray-bake",
            "category": "Veggie",
            "yield": "4",
            "source": "www.pickuplimes.com",
            "time": "50 mins",
            "diet": [
                "Vegan"
            ]
        },
        {
            "name": "Harissa lamb and rice salad",
            "image": "/images/harissa-lamb-and-rice-salad.webp",
            "html": "/recipes/harissa-lamb-and-rice-salad",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.simplybeefandlamb.co.uk",
            "time": "20 mins",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Vegan pasta salad",
            "image": "/images/vegan-pasta-salad.webp",
            "html": "/recipes/vegan-pasta-salad",
            "category": "Veggie",
            "yield": "5",
            "source": "rainbowplantlife.com",
            "time": "40 mins",
            "diet": [
                "Vegan"
            ],
            "positive": true
        },
        {
            "name": "Chicago-style deep dish pizza",
            "image": "/images/chicago-style-deep-dish-pizza.webp",
            "html": "/recipes/chicago-style-deep-dish-pizza",
            "category": "Pork",
            "yield": "12",
            "source": "www.kingarthurbaking.com",
            "time": "2 hr 35 mins",
            "cuisine": "American"
        },
        {
            "name": "Asian spiced burgers with mango salsa",
            "image": "/images/asian-spiced-burgers-with-mango-salsa.webp",
            "html": "/recipes/asian-spiced-burgers-with-mango-salsa",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.simplybeefandlamb.co.uk",
            "time": "31 mins",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Sheet pan sausage, peppers and onions",
            "image": "/images/sheet-pan-sausage-peppers-and-onions.webp",
            "html": "/recipes/sheet-pan-sausage-peppers-and-onions",
            "category": "Poultry",
            "yield": "6",
            "source": "www.themediterraneandish.com",
            "time": "18 mins",
            "cuisine": "Italian"
        },
        {
            "name": "Smoky pork and pineapple tostadas",
            "image": "/images/smoky-pork-and-pineapple-tostadas.webp",
            "html": "/recipes/smoky-pork-and-pineapple-tostadas",
            "category": "Pork",
            "yield": "4",
            "source": "www.lovepork.co.uk",
            "time": "20 mins"
        },
        {
            "name": "15-minute vegan chili garlic noodles",
            "image": "/images/15-minute-vegan-chili-garlic-noodles.webp",
            "html": "/recipes/15-minute-vegan-chili-garlic-noodles",
            "category": "Veggie",
            "yield": "2-3",
            "source": "rainbowplantlife.com",
            "time": "15 mins",
            "diet": [
                "Vegan"
            ],
            "cuisine": "Asian"
        },
        {
            "name": "Creamy butter chicken",
            "image": "/images/creamy-butter-chicken.webp",
            "html": "/recipes/creamy-butter-chicken",
            "category": "Poultry",
            "yield": "6",
            "source": "pinchofnom.com",
            "time": "35 mins",
            "positive": true
        },
        {
            "name": "Doner kebab with chicken",
            "image": "/images/doner-kebab-with-chicken.webp",
            "html": "/recipes/doner-kebab-with-chicken",
            "category": "Poultry",
            "yield": "8",
            "source": "www.themediterraneandish.com",
            "time": "40 mins",
            "cuisine": "Turkish"
        },
        {
            "name": "Potato salad recipe with tuna",
            "image": "/images/potato-salad-recipe-with-tuna.webp",
            "html": "/recipes/potato-salad-recipe-with-tuna",
            "category": "Seafood",
            "yield": "8",
            "source": "www.themediterraneandish.com",
            "time": "24 mins",
            "cuisine": "Spanish"
        },
        {
            "name": "Vegetarian turkish pide",
            "image": "/images/vegetarian-turkish-pide.webp",
            "html": "/recipes/vegetarian-turkish-pide",
            "category": "Veggie",
            "yield": "4",
            "source": "www.themediterraneandish.com",
            "time": "50 mins",
            "cuisine": "Turkish"
        },
        {
            "name": "Sweet potato spiced chili",
            "image": "/images/sweet-potato-spiced-chili.webp",
            "html": "/recipes/sweet-potato-spiced-chili",
            "category": "Veggie",
            "yield": "4",
            "source": "www.pickuplimes.com",
            "time": "50 mins",
            "diet": [
                "Vegan",
                "GlutenFree",
                "NutFree"
            ]
        },
        {
            "name": "Chicken kiev",
            "image": "/images/chicken-kiev.webp",
            "html": "/recipes/chicken-kiev",
            "category": "Poultry",
            "yield": "4",
            "source": "Who Put the Beef in Wellington",
            "time": "1 hr 5 mins"
        },
        {
            "name": "Smoked salmon spaghetti with crispy capers",
            "image": "/images/smoked-salmon-spaghetti-with-crispy-capers.webp",
            "html": "/recipes/smoked-salmon-spaghetti-with-crispy-capers",
            "category": "Seafood",
            "yield": "2",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "20 mins"
        },
        {
            "name": "Chicken musakhan",
            "image": "/images/chicken-musakhan.webp",
            "html": "/recipes/chicken-musakhan",
            "category": "Poultry",
            "yield": "4",
            "source": "Falastin",
            "time": "1 hr 5 mins",
            "cuisine": "Palestinian",
            "positive": true
        },
        {
            "name": "Saffron tagliatelle with ricotta and crispy chipotle shallots",
            "image": "/images/saffron-tagliatelle-with-ricotta-and-crispy-chipotle-shallots.webp",
            "html": "/recipes/saffron-tagliatelle-with-ricotta-and-crispy-chipotle-shallots",
            "category": "Veggie",
            "yield": "4",
            "source": "FLAVOUR by Ottolenghi",
            "time": "45 mins"
        },
        {
            "name": "Egyptian hawawshi",
            "image": "/images/egyptian-hawawshi.webp",
            "html": "/recipes/egyptian-hawawshi",
            "category": "RedMeat",
            "yield": "12",
            "source": "www.themediterraneandish.com",
            "time": "30 mins",
            "cuisine": "Egyptian",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Tacos al pastor",
            "image": "/images/tacos-al-pastor.webp",
            "html": "/recipes/tacos-al-pastor",
            "category": "Pork",
            "yield": "3-4",
            "source": "cooking.nytimes.com",
            "time": "30 mins",
            "cuisine": "Mexican"
        },
        {
            "name": "Somerset chicken one-pot",
            "image": "/images/somerset-chicken-one-pot.webp",
            "html": "/recipes/somerset-chicken-one-pot",
            "category": "Poultry",
            "yield": "3",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "35 mins",
            "cuisine": "British"
        },
        {
            "name": "Sweet potato, orange and ginger soup",
            "image": "/images/sweet-potato-orange-and-ginger-soup.webp",
            "html": "/recipes/sweet-potato-orange-and-ginger-soup",
            "category": "Veggie",
            "yield": "4",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "50 mins",
            "positive": true
        },
        {
            "name": "Ciabatta pizza with tahini balsamic drizzle",
            "image": "/images/ciabatta-pizza-with-tahini-balsamic-drizzle.webp",
            "html": "/recipes/ciabatta-pizza-with-tahini-balsamic-drizzle",
            "category": "Veggie",
            "yield": "2",
            "source": "www.pickuplimes.com",
            "time": "30 mins",
            "diet": [
                "Vegan"
            ]
        },
        {
            "name": "Harira",
            "image": "/images/harira.webp",
            "html": "/recipes/harira",
            "category": "RedMeat",
            "yield": "5",
            "source": "www.youtube.com",
            "time": "1 hr 15 mins",
            "cuisine": "Maghrebi",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Roasted potatoes with a\u00efoli and buttered pine nuts",
            "image": "/images/roasted-potatoes-with-aioli-and-buttered-pine-nuts.webp",
            "html": "/recipes/roasted-potatoes-with-aioli-and-buttered-pine-nuts",
            "category": "Veggie",
            "yield": "4",
            "source": "Ottolenghi Test Kitchen: Shelf Love",
            "time": "45 mins"
        },
        {
            "name": "Campfire chilli",
            "image": "/images/campfire-chilli.webp",
            "html": "/recipes/campfire-chilli",
            "category": "RedMeat",
            "yield": "10",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "3 hr 50 mins",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Lahmacun",
            "image": "/images/lahmacun.webp",
            "html": "/recipes/lahmacun",
            "category": "RedMeat",
            "yield": "8",
            "source": "Persiana",
            "time": "1 hr 15 mins",
            "cuisine": "Turkish",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "General tso\u2019s chicken",
            "image": "/images/general-tsos-chicken.webp",
            "html": "/recipes/general-tsos-chicken",
            "category": "Poultry",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "40 mins",
            "cuisine": "Chinese",
            "tags": [
                "deep-fryer"
            ],
            "positive": true
        },
        {
            "name": "Dorset apple cake",
            "image": "/images/dorset-apple-cake.webp",
            "html": "/recipes/dorset-apple-cake",
            "category": "Dessert",
            "yield": "8",
            "source": "www.bbcgoodfood.com",
            "time": "1 hr",
            "cuisine": "British",
            "tags": [
                "cake"
            ],
            "positive": true
        },
        {
            "name": "Lamb mini roast with warm spices",
            "image": "/images/lamb-mini-roast-with-warm-spices.webp",
            "html": "/recipes/lamb-mini-roast-with-warm-spices",
            "category": "RedMeat",
            "yield": "3",
            "source": "www.simplybeefandlamb.co.uk",
            "time": "55 mins",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Sweet mustard salmon with garlicky veg",
            "image": "/images/sweet-mustard-salmon-with-garlicky-veg.webp",
            "html": "/recipes/sweet-mustard-salmon-with-garlicky-veg",
            "category": "Seafood",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "45 mins"
        },
        {
            "name": "Creamy tuscan sausage gnocchi",
            "image": "/images/creamy-tuscan-sausage-gnocchi.webp",
            "html": "/recipes/creamy-tuscan-sausage-gnocchi",
            "category": "Pork",
            "yield": "4",
            "source": "www.thecomfortofcooking.com",
            "time": "20 mins"
        },
        {
            "name": "Curry-crusted sweet potatoes",
            "image": "/images/curry-crusted-sweet-potatoes.webp",
            "html": "/recipes/curry-crusted-sweet-potatoes",
            "category": "Veggie",
            "yield": "2",
            "source": "Bosh!",
            "time": "1 hr 5 mins"
        },
        {
            "name": "Black lime beef skewers with sumac onions",
            "image": "/images/black-lime-beef-skewers-with-sumac-onions.webp",
            "html": "/recipes/black-lime-beef-skewers-with-sumac-onions",
            "category": "RedMeat",
            "yield": "4",
            "source": "Ottolenghi Test Kitchen: Shelf Love",
            "time": "40 mins",
            "cuisine": "Bahamian",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Effortless doughnuts",
            "image": "/images/effortless-doughnuts.webp",
            "html": "/recipes/effortless-doughnuts",
            "category": "Dessert",
            "yield": "12",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "25 mins",
            "tags": [
                "deep-fryer"
            ],
            "positive": true
        },
        {
            "name": "Satay chicken salad wraps",
            "image": "/images/satay-chicken-salad-wraps.webp",
            "html": "/recipes/satay-chicken-salad-wraps",
            "category": "Poultry",
            "yield": "2",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "20 mins"
        },
        {
            "name": "Belly bonnet biryani",
            "image": "/images/belly-bonnet-biryani.webp",
            "html": "/recipes/belly-bonnet-biryani",
            "category": "Pork",
            "yield": "4",
            "source": "Pimp My Rice",
            "time": "1 hr 50 mins"
        },
        {
            "name": "Gigli with chickpeas and za\u2019atar",
            "image": "/images/gigli-with-chickpeas-and-zaatar.webp",
            "html": "/recipes/gigli-with-chickpeas-and-zaatar",
            "category": "Veggie",
            "yield": "4",
            "source": "SIMPLE by Ottolenghi",
            "time": "40 mins"
        },
        {
            "name": "Caramelized zucchini pasta",
            "image": "/images/caramelized-zucchini-pasta.webp",
            "html": "/recipes/caramelized-zucchini-pasta",
            "category": "Veggie",
            "yield": "4",
            "source": "cooking.nytimes.com",
            "time": "50 mins"
        },
        {
            "name": "Spinach and toasted orzo, with dill and green chilli salsa",
            "image": "/images/spinach-and-toasted-orzo-with-dill-and-green-chilli-salsa.webp",
            "html": "/recipes/spinach-and-toasted-orzo-with-dill-and-green-chilli-salsa",
            "category": "Veggie",
            "yield": "4",
            "source": "Falastin",
            "time": "40 mins"
        },
        {
            "name": "Open meat pies",
            "image": "/images/open-meat-pies.webp",
            "html": "/recipes/open-meat-pies",
            "category": "RedMeat",
            "yield": "12",
            "source": "Falastin",
            "time": "2 hr 20 mins"
        },
        {
            "name": "Saffron salmon kababs",
            "image": "/images/saffron-salmon-kababs.webp",
            "html": "/recipes/saffron-salmon-kababs",
            "category": "Seafood",
            "yield": "4",
            "source": "cooking.nytimes.com",
            "time": "20 mins"
        },
        {
            "name": "Bandari monkfish tails",
            "image": "/images/bandari-monkfish-tails.webp",
            "html": "/recipes/bandari-monkfish-tails",
            "category": "Seafood",
            "yield": "4",
            "source": "Persiana",
            "time": "30 mins",
            "cuisine": "Iranian"
        },
        {
            "name": "Chorizo and black bean loaded sweet potato boats",
            "image": "/images/chorizo-and-black-bean-loaded-sweet-potato-boats.webp",
            "html": "/recipes/chorizo-and-black-bean-loaded-sweet-potato-boats",
            "category": "Veggie",
            "yield": "2",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "2 hr",
            "diet": [
                "GlutenFree",
                "DairyFree"
            ]
        },
        {
            "name": "Blackberry and apple hedgerow crumble bars",
            "image": "/images/blackberry-and-apple-hedgerow-crumble-bars.webp",
            "html": "/recipes/blackberry-and-apple-hedgerow-crumble-bars",
            "category": "Dessert",
            "yield": "10",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "1 hr 10 mins"
        },
        {
            "name": "Lemon pollock with sweet potato chips & broccoli mash",
            "image": "/images/lemon-pollock-with-sweet-potato-chips-broccoli-mash.webp",
            "html": "/recipes/lemon-pollock-with-sweet-potato-chips-broccoli-mash",
            "category": "Seafood",
            "yield": "2",
            "source": "www.bbcgoodfood.com",
            "time": "50 mins"
        },
        {
            "name": "Peruvian chicken bowl with potatoes",
            "image": "/images/peruvian-chicken-bowl-with-potatoes.webp",
            "html": "/recipes/peruvian-chicken-bowl-with-potatoes",
            "category": "Poultry",
            "yield": "4",
            "source": "www.triedandtruerecipe.com",
            "time": "1 hr 30 mins",
            "cuisine": "Peruvian"
        },
        {
            "name": "Menemen (turkish scrambled eggs with tomato)",
            "image": "/images/menemen-turkish-scrambled-eggs-with-tomato.webp",
            "html": "/recipes/menemen-turkish-scrambled-eggs-with-tomato",
            "category": "Veggie",
            "yield": "4",
            "source": "cooking.nytimes.com",
            "time": "25 mins",
            "cuisine": "Turkish"
        },
        {
            "name": "Chorizo patatas bravas",
            "image": "/images/chorizo-patatas-bravas.webp",
            "html": "/recipes/chorizo-patatas-bravas",
            "category": "Pork",
            "yield": "6",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "40 mins",
            "cuisine": "Spanish"
        },
        {
            "name": "Pasta with sausage and white beans",
            "image": "/images/pasta-with-sausage-and-white-beans.webp",
            "html": "/recipes/pasta-with-sausage-and-white-beans",
            "category": "Pork",
            "yield": "5",
            "source": "www.triedandtruerecipe.com",
            "time": "1 hr"
        },
        {
            "name": "Lamb rosemary and apple burgers with apple and cider relish",
            "image": "/images/lamb-rosemary-and-apple-burgers-with-apple-and-cider-relish.webp",
            "html": "/recipes/lamb-rosemary-and-apple-burgers-with-apple-and-cider-relish",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.simplybeefandlamb.co.uk",
            "time": "40 mins"
        },
        {
            "name": "Salmon and couscous salad with cucumber-feta dressing",
            "image": "/images/salmon-and-couscous-salad-with-cucumber-feta-dressing.webp",
            "html": "/recipes/salmon-and-couscous-salad-with-cucumber-feta-dressing",
            "category": "Seafood",
            "yield": "4",
            "source": "cooking.nytimes.com",
            "time": "30 mins"
        },
        {
            "name": "German potato salad",
            "image": "/images/german-potato-salad.webp",
            "html": "/recipes/german-potato-salad",
            "category": "Pork",
            "yield": "6",
            "source": "cooking.nytimes.com",
            "time": "40 mins"
        },
        {
            "name": "Rigatoni caponata gratin",
            "image": "/images/rigatoni-caponata-gratin.webp",
            "html": "/recipes/rigatoni-caponata-gratin",
            "category": "Veggie",
            "yield": "4",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "30 mins",
            "diet": [
                "Vegan",
                "DairyFree"
            ]
        },
        {
            "name": "Sheet-pan cookies",
            "image": "/images/sheet-pan-cookies.webp",
            "html": "/recipes/sheet-pan-cookies",
            "category": "Dessert",
            "yield": "24",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "45 mins"
        },
        {
            "name": "Moroccan chicken and lentil pies",
            "image": "/images/moroccan-chicken-and-lentil-pies.webp",
            "html": "/recipes/moroccan-chicken-and-lentil-pies",
            "category": "Poultry",
            "yield": "2",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "1 hr 20 mins",
            "cuisine": "Moroccan",
            "tags": [
                "pie"
            ]
        },
        {
            "name": "BBQ beef and beans",
            "image": "/images/bbq-beef-and-beans.webp",
            "html": "/recipes/bbq-beef-and-beans",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.budgetbytes.com",
            "time": "30 mins"
        },
        {
            "name": "Easy red lentil meatballs",
            "image": "/images/easy-red-lentil-meatballs.webp",
            "html": "/recipes/easy-red-lentil-meatballs",
            "category": "Veggie",
            "yield": "6",
            "source": "www.triedandtruerecipe.com",
            "time": "1 hr",
            "diet": [
                "Vegan"
            ]
        },
        {
            "name": "Pesto mozzarella roll ups",
            "image": "/images/pesto-mozzarella-roll-ups.webp",
            "html": "/recipes/pesto-mozzarella-roll-ups",
            "category": "Veggie",
            "yield": "8",
            "source": "www.budgetbytes.com",
            "time": "30 mins"
        },
        {
            "name": "Sun-dried tomato meatball sub",
            "image": "/images/sun-dried-tomato-meatball-sub.webp",
            "html": "/recipes/sun-dried-tomato-meatball-sub",
            "category": "RedMeat",
            "yield": "6",
            "source": "www.triedandtruerecipe.com",
            "time": "3 hr 20 mins"
        },
        {
            "name": "The best ever ground beef tacos",
            "image": "/images/the-best-ever-ground-beef-tacos.webp",
            "html": "/recipes/the-best-ever-ground-beef-tacos",
            "category": "RedMeat",
            "yield": "10",
            "source": "www.joyfulhealthyeats.com",
            "time": "30 mins",
            "diet": [
                "GlutenFree"
            ],
            "cuisine": "Mexican",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Pineapple-marinated chicken breasts",
            "image": "/images/pineapple-marinated-chicken-breasts.webp",
            "html": "/recipes/pineapple-marinated-chicken-breasts",
            "category": "Poultry",
            "yield": "4",
            "source": "cooking.nytimes.com",
            "time": "30 mins"
        },
        {
            "name": "Penne all'arrabbiata con funghi champignon",
            "image": "/images/penne-all-arrabbiata-con-funghi-champignon.webp",
            "html": "/recipes/penne-all-arrabbiata-con-funghi-champignon",
            "category": "Veggie",
            "yield": "4",
            "source": "ricette.giallozafferano.it",
            "time": "40 mins",
            "diet": [
                "DairyFree"
            ],
            "cuisine": "Italian"
        },
        {
            "name": "Moroccan-spiced chicken meatballs",
            "image": "/images/moroccan-spiced-chicken-meatballs.webp",
            "html": "/recipes/moroccan-spiced-chicken-meatballs",
            "category": "Poultry",
            "yield": "4",
            "source": "cooking.nytimes.com",
            "time": "30 mins",
            "cuisine": "Moroccan"
        },
        {
            "name": "Tomato and rosemary pearl barley risotto",
            "image": "/images/tomato-and-rosemary-pearl-barley-risotto.webp",
            "html": "/recipes/tomato-and-rosemary-pearl-barley-risotto",
            "category": "Veggie",
            "yield": "4",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "1 hr",
            "cuisine": "Italian"
        },
        {
            "name": "Prawn pasta salad",
            "image": "/images/prawn-pasta-salad.webp",
            "html": "/recipes/prawn-pasta-salad",
            "category": "Seafood",
            "yield": "4",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "15 mins",
            "cuisine": "Mediterranean"
        },
        {
            "name": "Sticky sweet and sour plums and sausages",
            "image": "/images/sticky-sweet-and-sour-plums-and-sausages.webp",
            "html": "/recipes/sticky-sweet-and-sour-plums-and-sausages",
            "category": "Pork",
            "yield": "4",
            "source": "Ottolenghi Test Kitchen: Shelf Love",
            "time": "1 hr 40 mins"
        },
        {
            "name": "Arancini di riso al forno",
            "image": "/images/arancini-di-riso-al-forno.webp",
            "html": "/recipes/arancini-di-riso-al-forno",
            "category": "Pork",
            "yield": "10",
            "source": "ricette.giallozafferano.it",
            "time": "1 hr 45 mins",
            "cuisine": "Italian"
        },
        {
            "name": "Veneto-Style Rice and Peas",
            "image": "/images/veneto-style-rice-and-peas.webp",
            "html": "/recipes/veneto-style-rice-and-peas",
            "category": "Pork",
            "yield": "4",
            "source": "The Complete Italian Cookbook",
            "time": "40 mins",
            "cuisine": "Italian"
        },
        {
            "name": "Skillet chicken with cumin, paprika and mint",
            "image": "/images/skillet-chicken-with-cumin-paprika-and-mint.webp",
            "html": "/recipes/skillet-chicken-with-cumin-paprika-and-mint",
            "category": "Poultry",
            "yield": "4",
            "source": "cooking.nytimes.com",
            "time": "1 hr 20 mins",
            "cuisine": "Middle Eastern"
        },
        {
            "name": "Tuna burgers with cucumber salsa",
            "image": "/images/tuna-burgers-with-cucumber-salsa.webp",
            "html": "/recipes/tuna-burgers-with-cucumber-salsa",
            "category": "Seafood",
            "yield": "2",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "30 mins"
        },
        {
            "name": "Make-ahead freezer burritos",
            "image": "/images/make-ahead-freezer-burritos.webp",
            "html": "/recipes/make-ahead-freezer-burritos",
            "category": "Veggie",
            "yield": "6",
            "source": "www.pickuplimes.com",
            "time": "30 mins",
            "diet": [
                "Vegan",
                "NutFree"
            ],
            "cuisine": "Mexican"
        },
        {
            "name": "Asparagus and ham tart",
            "image": "/images/asparagus-and-ham-tart.webp",
            "html": "/recipes/asparagus-and-ham-tart",
            "category": "Pork",
            "yield": "4",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "30 mins"
        },
        {
            "name": "Japanese-style potato croquette",
            "image": "/images/japanese-style-potato-croquette.webp",
            "html": "/recipes/japanese-style-potato-croquette",
            "category": "Veggie",
            "yield": "4",
            "source": "The Vegan Japanese Cookbook",
            "time": "1 hr 45 mins",
            "diet": [
                "Vegan"
            ],
            "cuisine": "Japanese",
            "tags": [
                "deep-fryer"
            ]
        },
        {
            "name": "Oven fried fish fillets",
            "image": "/images/oven-fried-fish-fillets.webp",
            "html": "/recipes/oven-fried-fish-fillets",
            "category": "Seafood",
            "yield": "4",
            "source": "My Lebanese Cookbook",
            "time": "25 mins",
            "cuisine": "Lebanese"
        },
        {
            "name": "Apple & blackberry crumble",
            "image": "/images/apple-blackberry-crumble.webp",
            "html": "/recipes/apple-blackberry-crumble",
            "category": "Dessert",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "35 mins",
            "cuisine": "British"
        },
        {
            "name": "Meat stuffed potato pancake",
            "image": "/images/meat-stuffed-potato-pancake.webp",
            "html": "/recipes/meat-stuffed-potato-pancake",
            "category": "Poultry",
            "yield": "6",
            "source": "www.cookhomey.com",
            "time": "50 mins"
        },
        {
            "name": "Alcaparrado",
            "image": "/images/alcaparrado.webp",
            "html": "/recipes/alcaparrado",
            "category": "Veggie",
            "yield": "10",
            "source": "The Easy Puerto Rican Cookbook",
            "time": "15 mins",
            "diet": [
                "Vegan",
                "GlutenFree",
                "DairyFree",
                "NutFree"
            ],
            "cuisine": "Puerto Rican"
        },
        {
            "name": "Codfish omelet sandwich",
            "image": "/images/codfish-omelet-sandwich.webp",
            "html": "/recipes/codfish-omelet-sandwich",
            "category": "Seafood",
            "yield": "4",
            "source": "Easy Portuguese Cookbook",
            "time": "20 mins",
            "cuisine": "Portuguese"
        },
        {
            "name": "Pastelillos de carne",
            "image": "/images/pastelillos-de-carne.webp",
            "html": "/recipes/pastelillos-de-carne",
            "category": "RedMeat",
            "yield": "10",
            "source": "The Easy Puerto Rican Cookbook",
            "time": "45 mins",
            "cuisine": "Puerto Rican"
        },
        {
            "name": "Chicken stroganoff",
            "image": "/images/chicken-stroganoff.webp",
            "html": "/recipes/chicken-stroganoff",
            "category": "Poultry",
            "yield": "2",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "55 mins",
            "cuisine": "Russian"
        },
        {
            "name": "Harissa prawns with warm chickpea salad",
            "image": "/images/harissa-prawns-with-warm-chickpea-salad.webp",
            "html": "/recipes/harissa-prawns-with-warm-chickpea-salad",
            "category": "Seafood",
            "yield": "2",
            "source": "BBC Good Food: Cheap & Healthy",
            "time": "12 mins"
        },
        {
            "name": "Mushroom & kidney bean coconut curry",
            "image": "/images/mushroom-kidney-bean-coconut-curry.webp",
            "html": "/recipes/mushroom-kidney-bean-coconut-curry",
            "category": "Veggie",
            "yield": "4",
            "source": "www.pickuplimes.com",
            "time": "20 mins",
            "diet": [
                "Vegan",
                "GlutenFree",
                "NutFree"
            ]
        },
        {
            "name": "Lebanese pizza",
            "image": "/images/lebanese-pizza.webp",
            "html": "/recipes/lebanese-pizza",
            "category": "RedMeat",
            "yield": "2-4",
            "source": "www.itv.com",
            "time": "15 mins",
            "cuisine": "Lebanese",
            "tags": [
                "beef",
                "lamb"
            ]
        },
        {
            "name": "Cranberry, oat and white chocolate biscuits",
            "image": "/images/cranberry-oat-and-white-chocolate-biscuits.webp",
            "html": "/recipes/cranberry-oat-and-white-chocolate-biscuits",
            "category": "Dessert",
            "yield": "30",
            "source": "SWEET by Ottolenghi",
            "time": "45 mins",
            "tags": [
                "cookies"
            ]
        },
        {
            "name": "Pasta and lentils (pasta e lenticchie)",
            "image": "/images/pasta-and-lentils.webp",
            "html": "/recipes/pasta-and-lentils",
            "category": "Veggie",
            "yield": "6",
            "source": "cooking.nytimes.com",
            "time": "1 hr"
        },
        {
            "name": "Coconut-braised spring greens",
            "image": "/images/coconut-braised-spring-greens.webp",
            "html": "/recipes/coconut-braised-spring-greens",
            "category": "Veggie",
            "yield": "2",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "30 mins",
            "diet": [
                "GlutenFree",
                "DairyFree"
            ],
            "cuisine": "Caribbean"
        },
        {
            "name": "Lemon pepper cajun chicken fettuccine alfredo",
            "image": "/images/lemon-pepper-cajun-chicken-fettuccine-alfredo.webp",
            "html": "/recipes/lemon-pepper-cajun-chicken-fettuccine-alfredo",
            "category": "Poultry",
            "yield": "6",
            "source": "www.halfbakedharvest.com",
            "time": "30 mins"
        },
        {
            "name": "Chicken kabsa",
            "image": "/images/chicken-kabsa.webp",
            "html": "/recipes/chicken-kabsa",
            "category": "Poultry",
            "yield": "6",
            "source": "nishkitchen.com",
            "time": "2 hr 15 mins",
            "cuisine": "Saudi Arabian"
        },
        {
            "name": "One pot veggie pasta",
            "image": "/images/one-pot-veggie-pasta.webp",
            "html": "/recipes/one-pot-veggie-pasta",
            "category": "Veggie",
            "yield": "4",
            "source": "www.budgetbytes.com",
            "time": "30 mins"
        },
        {
            "name": "Prawn fried rice",
            "image": "/images/prawn-fried-rice.webp",
            "html": "/recipes/prawn-fried-rice",
            "category": "Seafood",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "30 mins",
            "cuisine": "Asian"
        },
        {
            "name": "Coconut pork stew with garam masala",
            "image": "/images/coconut-pork-stew-with-garam-masala.webp",
            "html": "/recipes/coconut-pork-stew-with-garam-masala",
            "category": "Pork",
            "yield": "8",
            "source": "cooking.nytimes.com",
            "time": "3 hr"
        },
        {
            "name": "Pork katsu stir-fry",
            "image": "/images/pork-katsu-stir-fry.webp",
            "html": "/recipes/pork-katsu-stir-fry",
            "category": "Pork",
            "yield": "2",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "20 mins",
            "cuisine": "Japanese"
        },
        {
            "name": "Ultimate traybake ragu",
            "image": "/images/ultimate-traybake-ragu.webp",
            "html": "/recipes/ultimate-traybake-ragu",
            "category": "Veggie",
            "yield": "6-8",
            "source": "thehappyfoodie.co.uk",
            "time": "1 hr 35 mins",
            "diet": [
                "Vegan"
            ]
        },
        {
            "name": "Kimchi-brined fried chicken sandwich",
            "image": "/images/kimchi-brined-fried-chicken-sandwich.webp",
            "html": "/recipes/kimchi-brined-fried-chicken-sandwich",
            "category": "Poultry",
            "yield": "6",
            "source": "www.seriouseats.com",
            "time": "1 hr",
            "cuisine": "South Korean",
            "tags": [
                "deep-fryer"
            ]
        },
        {
            "name": "Curried shepherd's pie",
            "image": "/images/curried-shepherds-pie.webp",
            "html": "/recipes/curried-shepherds-pie",
            "category": "RedMeat",
            "yield": "6-8",
            "source": "cooking.nytimes.com",
            "time": "1 hr 15 mins",
            "cuisine": "British",
            "tags": [
                "beef",
                "pie"
            ]
        },
        {
            "name": "Fish tagine with preserved lemon and mint",
            "image": "/images/fish-tagine-with-preserved-lemon-and-mint.webp",
            "html": "/recipes/fish-tagine-with-preserved-lemon-and-mint",
            "category": "Seafood",
            "yield": "4",
            "source": "The Modern Tagine Cookbook",
            "time": "1 hr 5 mins",
            "cuisine": "Moroccan"
        },
        {
            "name": "Orange cookies",
            "image": "/images/orange-cookies.webp",
            "html": "/recipes/orange-cookies",
            "category": "Dessert",
            "yield": "24",
            "source": "preppykitchen.com",
            "time": "45 mins",
            "tags": [
                "cookies"
            ]
        },
        {
            "name": "Chicken shawarma pie",
            "image": "/images/chicken-shawarma-pie.webp",
            "html": "/recipes/chicken-shawarma-pie",
            "category": "Poultry",
            "yield": "6",
            "source": "Falastin",
            "time": "2 hr 30 mins",
            "cuisine": "Middle Eastern",
            "tags": [
                "pie"
            ]
        },
        {
            "name": "Tessa\u2019s spice cake",
            "image": "/images/tessas-spice-cake.webp",
            "html": "/recipes/tessas-spice-cake",
            "category": "Dessert",
            "yield": "12",
            "source": "SWEET by Ottolenghi",
            "time": "1 hr 15 mins",
            "diet": [
                "NutFree"
            ],
            "tags": [
                "cake"
            ]
        },
        {
            "name": "Peruvian-style grilled-chicken sandwiches with spicy green sauce",
            "image": "/images/peruvian-style-grilled-chicken-sandwiches-with-spicy-green-sauce.webp",
            "html": "/recipes/peruvian-style-grilled-chicken-sandwiches-with-spicy-green-sauce",
            "category": "Poultry",
            "yield": "4",
            "source": "www.seriouseats.com",
            "time": "40 mins",
            "cuisine": "Peruvian"
        },
        {
            "name": "Gnocchi with mushroom-lentil ragu",
            "image": "/images/gnocchi-with-mushroom-lentil-ragu.webp",
            "html": "/recipes/gnocchi-with-mushroom-lentil-ragu",
            "category": "Veggie",
            "yield": "2",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "30 mins",
            "diet": [
                "Vegan"
            ],
            "cuisine": "Italian"
        },
        {
            "name": "Spiced apple cake",
            "image": "/images/spiced-apple-cake.webp",
            "html": "/recipes/spiced-apple-cake",
            "category": "Dessert",
            "yield": "10",
            "source": "SIMPLE by Ottolenghi",
            "time": "1 hr 25 mins",
            "diet": [
                "NutFree"
            ],
            "tags": [
                "cake"
            ]
        },
        {
            "name": "Classic french toast",
            "image": "/images/classic-french-toast.webp",
            "html": "/recipes/classic-french-toast",
            "category": "Dessert",
            "yield": "4",
            "source": "cooking.nytimes.com",
            "time": "20 mins"
        },
        {
            "name": "Lemon and poppy seed cake",
            "image": "/images/lemon-and-poppy-seed-cake.webp",
            "html": "/recipes/lemon-and-poppy-seed-cake",
            "category": "Dessert",
            "yield": "8",
            "source": "SWEET by Ottolenghi",
            "time": "1 hr 10 mins",
            "tags": [
                "cake"
            ]
        },
        {
            "name": "Polpette in bianco",
            "image": "/images/polpette-in-bianco.webp",
            "html": "/recipes/polpette-in-bianco",
            "category": "Pork",
            "yield": "6",
            "source": "ricette.giallozafferano.it",
            "time": "25 mins",
            "cuisine": "Italian"
        },
        {
            "name": "One-pan crispy spaghetti and chicken",
            "image": "/images/one-pan-crispy-spaghetti-and-chicken.webp",
            "html": "/recipes/one-pan-crispy-spaghetti-and-chicken",
            "category": "Poultry",
            "yield": "4",
            "source": "Ottolenghi Test Kitchen: Shelf Love",
            "time": "1 hr 20 mins"
        },
        {
            "name": "Sausage and chorizo ragu",
            "image": "/images/sausage-and-chorizo-ragu.webp",
            "html": "/recipes/sausage-and-chorizo-ragu",
            "category": "Pork",
            "yield": "6",
            "source": "www.mobkitchen.co.uk",
            "time": "1 hr 40 mins",
            "cuisine": "Italian"
        },
        {
            "name": "Sausages with parsnip and apple mash",
            "image": "/images/sausages-with-parsnip-and-apple-mash.webp",
            "html": "/recipes/sausages-with-parsnip-and-apple-mash",
            "category": "Pork",
            "yield": "2",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "30 mins"
        },
        {
            "name": "Mushrooms and dumplings",
            "image": "/images/mushrooms-and-dumplings.webp",
            "html": "/recipes/mushrooms-and-dumplings",
            "category": "Veggie",
            "yield": "4",
            "source": "cooking.nytimes.com",
            "time": "50 mins"
        },
        {
            "name": "Chickpea pancakes with mango pickle yoghurt",
            "image": "/images/chickpea-pancakes-with-mango-pickle-yoghurt.webp",
            "html": "/recipes/chickpea-pancakes-with-mango-pickle-yoghurt",
            "category": "Veggie",
            "yield": "4",
            "source": "FLAVOUR by Ottolenghi",
            "time": "30 mins",
            "diet": [
                "Vegan"
            ]
        },
        {
            "name": "Berbere spiced chicken, carrots and chickpeas",
            "image": "/images/berbere-spiced-chicken-carrots-and-chickpeas.webp",
            "html": "/recipes/berbere-spiced-chicken-carrots-and-chickpeas",
            "category": "Poultry",
            "yield": "4-6",
            "source": "Ottolenghi Test Kitchen: Shelf Love",
            "time": "1 hr 30 mins",
            "cuisine": "Ethiopian"
        },
        {
            "name": "Confit tandoori chickpeas",
            "image": "/images/confit-tandoori-chickpeas.webp",
            "html": "/recipes/confit-tandoori-chickpeas",
            "category": "Veggie",
            "yield": "4",
            "source": "Ottolenghi Test Kitchen: Shelf Love",
            "time": "1 hr 25 mins"
        },
        {
            "name": "Aushak - afghan dumplings",
            "image": "/images/aushak-afghan-dumplings.webp",
            "html": "/recipes/aushak-afghan-dumplings",
            "category": "Veggie",
            "yield": "40",
            "source": "www.pickuplimes.com",
            "time": "1 hr 15 mins",
            "diet": [
                "Vegan",
                "NutFree"
            ],
            "cuisine": "Afghan"
        },
        {
            "name": "Speedy lamb naans",
            "image": "/images/speedy-lamb-naans.webp",
            "html": "/recipes/speedy-lamb-naans",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.simplybeefandlamb.co.uk",
            "time": "25 mins",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "The big bosh! burger",
            "image": "/images/the-big-bosh-burger.webp",
            "html": "/recipes/the-big-bosh-burger",
            "category": "Veggie",
            "yield": "6",
            "source": "Bosh!",
            "time": "1 hr 10 mins",
            "diet": [
                "Vegan",
                "DairyFree"
            ]
        },
        {
            "name": "Mediterranean style fried rice with anchovy dressing",
            "image": "/images/mediterranean-style-fried-rice-with-anchovy-dressing.webp",
            "html": "/recipes/mediterranean-style-fried-rice-with-anchovy-dressing",
            "category": "Seafood",
            "yield": "2",
            "source": "Ottolenghi Test Kitchen: Shelf Love",
            "time": "45 mins",
            "cuisine": "Mediterranean"
        },
        {
            "name": "Spiced butternut mash with pancake omelettes",
            "image": "/images/spiced-butternut-mash-with-pancake-omelettes.webp",
            "html": "/recipes/spiced-butternut-mash-with-pancake-omelettes",
            "category": "Veggie",
            "yield": "4",
            "source": "Ottolenghi Test Kitchen: Shelf Love",
            "time": "1 hr 35 mins"
        },
        {
            "name": "Beef braised in beer",
            "image": "/images/beef-braised-in-beer.webp",
            "html": "/recipes/beef-braised-in-beer",
            "category": "RedMeat",
            "yield": "6",
            "source": "www.simplybeefandlamb.co.uk",
            "time": "2 hr 50 mins",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Frying pan hawaiian pizza",
            "image": "/images/frying-pan-hawaiian-pizza.webp",
            "html": "/recipes/frying-pan-hawaiian-pizza",
            "category": "Pork",
            "yield": "4",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "45 mins",
            "cuisine": "Italian"
        },
        {
            "name": "Homemade hamburger helper",
            "image": "/images/homemade-hamburger-helper.webp",
            "html": "/recipes/homemade-hamburger-helper",
            "category": "RedMeat",
            "yield": "4",
            "source": "cooking.nytimes.com",
            "time": "1 hr 15 mins",
            "cuisine": "American",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Sausage, apple and stilton strudel",
            "image": "/images/sausage-apple-and-stilton-strudel.webp",
            "html": "/recipes/sausage-apple-and-stilton-strudel",
            "category": "Pork",
            "yield": "6",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "1 hr 5 mins"
        },
        {
            "name": "Lamb tagine",
            "image": "/images/lamb-tagine.webp",
            "html": "/recipes/lamb-tagine",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.gordonrhodes.co.uk",
            "time": "45 mins",
            "cuisine": "Moroccan",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Hearty lentil one pot",
            "image": "/images/hearty-lentil-one-pot.webp",
            "html": "/recipes/hearty-lentil-one-pot",
            "category": "Veggie",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "1 hr 10 mins",
            "diet": [
                "Vegan",
                "GlutenFree",
                "DairyFree"
            ]
        },
        {
            "name": "Poached fish with ginger & sesame broth",
            "image": "/images/poached-fish-with-ginger-sesame-broth.webp",
            "html": "/recipes/poached-fish-with-ginger-sesame-broth",
            "category": "Seafood",
            "yield": "2",
            "source": "BBC Good Food: Cheap & Healthy",
            "time": "18 mins"
        },
        {
            "name": "Chicken bastilla",
            "image": "/images/chicken-bastilla.webp",
            "html": "/recipes/chicken-bastilla",
            "category": "Poultry",
            "yield": "6",
            "source": "Persiana",
            "time": "1 hr 10 mins",
            "cuisine": "Moroccan",
            "tags": [
                "pie"
            ]
        },
        {
            "name": "Baked chicken tenders",
            "image": "/images/baked-chicken-tenders.webp",
            "html": "/recipes/baked-chicken-tenders",
            "category": "Poultry",
            "yield": "4",
            "source": "cooking.nytimes.com",
            "time": "45 mins",
            "cuisine": "American"
        },
        {
            "name": "Lentil tagine with ginger and res el hanout",
            "image": "/images/lentil-tagine-with-ginger-and-res-el-hanout.webp",
            "html": "/recipes/lentil-tagine-with-ginger-and-res-el-hanout",
            "category": "Veggie",
            "yield": "4-6",
            "source": "The Modern Tagine Cookbook",
            "time": "50 mins",
            "cuisine": "Moroccan"
        },
        {
            "name": "Pasta del maresciallo",
            "image": "/images/pasta-del-maresciallo.webp",
            "html": "/recipes/pasta-del-maresciallo",
            "category": "Pork",
            "yield": "4",
            "source": "ricette.giallozafferano.it",
            "time": "28 mins",
            "cuisine": "Italian"
        },
        {
            "name": "All-in one sausage and crispy potato bake",
            "image": "/images/all-in-one-sausage-and-crispy-potato-bake.webp",
            "html": "/recipes/all-in-one-sausage-and-crispy-potato-bake",
            "category": "Pork",
            "yield": "4",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "1 hr"
        },
        {
            "name": "Melting meatball macaroni",
            "image": "/images/melting-meatball-macaroni.webp",
            "html": "/recipes/melting-meatball-macaroni",
            "category": "RedMeat",
            "yield": "6",
            "source": "www.bbcgoodfood.com",
            "time": "50 mins",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Cheat's macaroni cheese with ham, peas and stilton",
            "image": "/images/cheats-macaroni-cheese-with-ham-peas-and-stilton.webp",
            "html": "/recipes/cheats-macaroni-cheese-with-ham-peas-and-stilton",
            "category": "Pork",
            "yield": "4",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "35 mins",
            "cuisine": "Italian"
        },
        {
            "name": "Grilled harissa sardines with fennel & potato salad",
            "image": "/images/grilled-harissa-sardines-with-fennel-potato-salad.webp",
            "html": "/recipes/grilled-harissa-sardines-with-fennel-potato-salad",
            "category": "Seafood",
            "yield": "6",
            "source": "www.bbcgoodfood.com",
            "time": "50 mins"
        },
        {
            "name": "Sweet potato pastilla",
            "image": "/images/sweet-potato-pastilla.webp",
            "html": "/recipes/sweet-potato-pastilla",
            "category": "Veggie",
            "yield": "6",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "1 hr 55 mins",
            "cuisine": "Maghrebi",
            "positive": true
        },
        {
            "name": "Sweet potato cakes with poached eggs",
            "image": "/images/sweet-potato-cakes-with-poached-eggs.webp",
            "html": "/recipes/sweet-potato-cakes-with-poached-eggs",
            "category": "Veggie",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "30 mins"
        },
        {
            "name": "Tuscan chicken pasta",
            "image": "/images/tuscan-chicken-pasta.webp",
            "html": "/recipes/tuscan-chicken-pasta",
            "category": "Poultry",
            "yield": "4",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "25 mins",
            "cuisine": "Italian"
        },
        {
            "name": "Soft lemon cookies",
            "image": "/images/soft-lemon-cookies.webp",
            "html": "/recipes/soft-lemon-cookies",
            "category": "Dessert",
            "yield": "10",
            "source": "www.emmafontanella.com",
            "time": "1 hr 23 mins",
            "tags": [
                "cookies"
            ]
        },
        {
            "name": "Salmon pasta salad with lemon & capers",
            "image": "/images/salmon-pasta-salad-with-lemon-capers.webp",
            "html": "/recipes/salmon-pasta-salad-with-lemon-capers",
            "category": "Seafood",
            "yield": "2",
            "source": "www.bbcgoodfood.com",
            "time": "30 mins"
        },
        {
            "name": "Seafood orzotto",
            "image": "/images/seafood-orzotto.webp",
            "html": "/recipes/seafood-orzotto",
            "category": "Seafood",
            "yield": "2",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "30 mins"
        },
        {
            "name": "BLT breakfast salad",
            "image": "/images/blt-breakfast-salad.webp",
            "html": "/recipes/blt-breakfast-salad",
            "category": "Pork",
            "yield": "4",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "35 mins",
            "cuisine": "British"
        },
        {
            "name": "Tah chin",
            "image": "/images/tah-chin.webp",
            "html": "/recipes/tah-chin",
            "category": "RedMeat",
            "yield": "6",
            "source": "www.jamieoliver.com",
            "time": "4 hr",
            "cuisine": "Iranian",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Chapli burgers",
            "image": "/images/chapli-burgers.webp",
            "html": "/recipes/chapli-burgers",
            "category": "RedMeat",
            "yield": "4-6",
            "source": "cooking.nytimes.com",
            "time": "30 mins",
            "cuisine": "Pakistani",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Coconut and turmeric omelette feast",
            "image": "/images/coconut-and-turmeric-omelette-feast.webp",
            "html": "/recipes/coconut-and-turmeric-omelette-feast",
            "category": "Veggie",
            "yield": "2-4",
            "source": "FLAVOUR by Ottolenghi",
            "time": "45 mins"
        },
        {
            "name": "Spiced lamb & apricot stew",
            "image": "/images/spiced-lamb-apricot-stew.webp",
            "html": "/recipes/spiced-lamb-apricot-stew",
            "category": "RedMeat",
            "yield": "6",
            "source": "Persiana",
            "time": "2 hr 20 mins",
            "cuisine": "Iranian",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Superhealthy pizza",
            "image": "/images/superhealthy-pizza.webp",
            "html": "/recipes/superhealthy-pizza",
            "category": "Veggie",
            "yield": "2",
            "source": "BBC Good Food: Cheap & Healthy",
            "time": "25 mins"
        },
        {
            "name": "Moroccan mushrooms with couscous",
            "image": "/images/moroccan-mushrooms-with-couscous.webp",
            "html": "/recipes/moroccan-mushrooms-with-couscous",
            "category": "Veggie",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "20 mins",
            "cuisine": "Moroccan"
        },
        {
            "name": "Saffron chicken kababs with peaches",
            "image": "/images/saffron-chicken-kababs-with-peaches.webp",
            "html": "/recipes/saffron-chicken-kababs-with-peaches",
            "category": "Poultry",
            "yield": "4",
            "source": "cooking.nytimes.com",
            "time": "40 mins",
            "cuisine": "Iranian"
        },
        {
            "name": "Persian dried lime, lamb & split pea stew",
            "image": "/images/persian-dried-lime-lamb-split-pea-stew.webp",
            "html": "/recipes/persian-dried-lime-lamb-split-pea-stew",
            "category": "RedMeat",
            "yield": "6-8",
            "source": "Persiana",
            "time": "2 hr 50 mins",
            "cuisine": "Iranian",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Sweet and salty lentil spaghetti",
            "image": "/images/sweet-and-salty-lentil-spaghetti.webp",
            "html": "/recipes/sweet-and-salty-lentil-spaghetti",
            "category": "Veggie",
            "yield": "4",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "1 hr 5 mins",
            "diet": [
                "Vegan",
                "DairyFree"
            ]
        },
        {
            "name": "Moroccan lamb tagine",
            "image": "/images/moroccan-lamb-tagine.webp",
            "html": "/recipes/moroccan-lamb-tagine",
            "category": "RedMeat",
            "yield": "4",
            "source": "Slimming World Magazine",
            "time": "50 mins",
            "cuisine": "Moroccan",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Quinoa salad with toasted pistachios, preserved lemons and courgettes",
            "image": "/images/quinoa-salad-with-toasted-pistachios-preserved-lemons-and-courgettes.webp",
            "html": "/recipes/quinoa-salad-with-toasted-pistachios-preserved-lemons-and-courgettes",
            "category": "Veggie",
            "yield": "4-6",
            "source": "Persiana",
            "time": "35 mins",
            "cuisine": "Middle Eastern"
        },
        {
            "name": "Saffron & rosemary chicken fillets",
            "image": "/images/saffron-rosemary-chicken-fillets.webp",
            "html": "/recipes/saffron-rosemary-chicken-fillets",
            "category": "Poultry",
            "yield": "4",
            "source": "Persiana",
            "time": "30 mins",
            "cuisine": "Middle Eastern"
        },
        {
            "name": "Chicken parmigiana",
            "image": "/images/chicken-parmigiana.webp",
            "html": "/recipes/chicken-parmigiana",
            "category": "Poultry",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "30 mins",
            "cuisine": "Italian"
        },
        {
            "name": "Pesto chickpea salad",
            "image": "/images/pesto-chickpea-salad.webp",
            "html": "/recipes/pesto-chickpea-salad",
            "category": "Veggie",
            "yield": "4",
            "source": "www.budgetbytes.com",
            "time": "10 mins"
        },
        {
            "name": "Warm pearl couscous salad",
            "image": "/images/warm-pearl-couscous-salad.webp",
            "html": "/recipes/warm-pearl-couscous-salad",
            "category": "Veggie",
            "yield": "4",
            "source": "www.pickuplimes.com",
            "time": "25 mins",
            "diet": [
                "Vegan"
            ]
        },
        {
            "name": "Seekh kebab with mint chutney",
            "image": "/images/seekh-kebab-with-mint-chutney.webp",
            "html": "/recipes/seekh-kebab-with-mint-chutney",
            "category": "RedMeat",
            "yield": "6",
            "source": "cooking.nytimes.com",
            "time": "2 hr 45 mins",
            "cuisine": "Indian",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Baked sweet potato with easy black bean chilli",
            "image": "/images/baked-sweet-potato-with-easy-black-bean-chilli.webp",
            "html": "/recipes/baked-sweet-potato-with-easy-black-bean-chilli",
            "category": "Veggie",
            "yield": "4",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "1 hr 35 mins",
            "cuisine": "Mexican"
        },
        {
            "name": "Slow-cooked pork and fennel ragu",
            "image": "/images/slow-cooked-pork-and-fennel-ragu.webp",
            "html": "/recipes/slow-cooked-pork-and-fennel-ragu",
            "category": "Pork",
            "yield": "8",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "4 hr 10 mins",
            "diet": [
                "GlutenFree",
                "DairyFree"
            ]
        },
        {
            "name": "Easy pesto chicken and vegetables",
            "image": "/images/easy-pesto-chicken-and-vegetables.webp",
            "html": "/recipes/easy-pesto-chicken-and-vegetables",
            "category": "Poultry",
            "yield": "4",
            "source": "www.budgetbytes.com",
            "time": "30 mins"
        },
        {
            "name": "One-pan orecchiette puttanesca",
            "image": "/images/one-pan-orecchiette-puttanesca.webp",
            "html": "/recipes/one-pan-orecchiette-puttanesca",
            "category": "Veggie",
            "yield": "4",
            "source": "SIMPLE by Ottolenghi",
            "time": "40 mins"
        },
        {
            "name": "Slow-roast pork rolls",
            "image": "/images/slow-roast-pork-rolls.webp",
            "html": "/recipes/slow-roast-pork-rolls",
            "category": "Pork",
            "yield": "4",
            "source": "BBC Good Food: Slow Cooker Favourites",
            "time": "6 hr 10 mins",
            "tags": [
                "slow-cooker"
            ]
        },
        {
            "name": "Spicy shrimp tomato pasta",
            "image": "/images/spicy-shrimp-tomato-pasta.webp",
            "html": "/recipes/spicy-shrimp-tomato-pasta",
            "category": "Seafood",
            "yield": "4",
            "source": "www.budgetbytes.com",
            "time": "25 mins"
        },
        {
            "name": "Summer tagine of lamb, courgettes, peppers and mint",
            "image": "/images/summer-tagine-of-lamb-courgettes-peppers-and-mint.webp",
            "html": "/recipes/summer-tagine-of-lamb-courgettes-peppers-and-mint",
            "category": "RedMeat",
            "yield": "4-6",
            "source": "The Modern Tagine Cookbook",
            "time": "2 hr",
            "cuisine": "Moroccan",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Pearl couscous with sauteed cherry tomatoes",
            "image": "/images/pearl-couscous-with-sauteed-cherry-tomatoes.webp",
            "html": "/recipes/pearl-couscous-with-sauteed-cherry-tomatoes",
            "category": "Veggie",
            "yield": "4",
            "source": "cooking.nytimes.com",
            "time": "25 mins",
            "diet": [
                "DairyFree",
                "NutFree"
            ],
            "cuisine": "Italian"
        },
        {
            "name": "Sardine pasta with crunchy parsley crumbs",
            "image": "/images/sardine-pasta-with-crunchy-parsley-crumbs.webp",
            "html": "/recipes/sardine-pasta-with-crunchy-parsley-crumbs",
            "category": "Seafood",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "35 mins"
        },
        {
            "name": "Jollof rice",
            "image": "/images/jollof-rice.webp",
            "html": "/recipes/jollof-rice",
            "category": "Veggie",
            "yield": "4",
            "source": "Pimp My Rice",
            "time": "1 hr"
        },
        {
            "name": "Spicy bacon broccoli",
            "image": "/images/spicy-bacon-broccoli.webp",
            "html": "/recipes/spicy-bacon-broccoli",
            "category": "Pork",
            "yield": "2",
            "source": "www.chinghehuang.com",
            "time": "10 mins",
            "cuisine": "Asian"
        },
        {
            "name": "Bramley apple meatballs with bramley apple, cranberry sauce",
            "image": "/images/bramley-apple-meatballs-with-creamy-mash-and-bramley-apple-cranberry-sauce.webp",
            "html": "/recipes/bramley-apple-meatballs-with-creamy-mash-and-bramley-apple-cranberry-sauce",
            "category": "Poultry",
            "yield": "4",
            "source": "Preston & Fylde Live Magazine",
            "time": "35 mins"
        },
        {
            "name": "Citrus-glazed pork chops with gingery bok choy",
            "image": "/images/citrus-glazed-pork-chops-with-gingery-bok-choy.webp",
            "html": "/recipes/citrus-glazed-pork-chops-with-gingery-bok-choy",
            "category": "Pork",
            "yield": "4",
            "source": "cooking.nytimes.com",
            "time": "40 mins"
        },
        {
            "name": "Vegetarian minestrone",
            "image": "/images/vegetarian-minestrone.webp",
            "html": "/recipes/vegetarian-minestrone",
            "category": "Veggie",
            "yield": "6",
            "source": "www.budgetbytes.com",
            "time": "45 mins"
        },
        {
            "name": "One-pot pasta with sausage and spinach",
            "image": "/images/one-pot-pasta-with-sausage-and-spinach.webp",
            "html": "/recipes/one-pot-pasta-with-sausage-and-spinach",
            "category": "Pork",
            "yield": "4",
            "source": "cooking.nytimes.com",
            "time": "20 mins",
            "cuisine": "Italian"
        },
        {
            "name": "Teriyaki salmon with asian noodle salad",
            "image": "/images/teriyaki-salmon-with-asian-noodle-salad.webp",
            "html": "/recipes/teriyaki-salmon-with-asian-noodle-salad",
            "category": "Seafood",
            "yield": "4-6",
            "source": "www.joyfulhealthyeats.com",
            "time": "45 mins",
            "diet": [
                "GlutenFree"
            ]
        },
        {
            "name": "Sweet potato in tomato, lime and cardamom sauce",
            "image": "/images/sweet-potato-in-tomato-lime-and-cardamom-sauce.webp",
            "html": "/recipes/sweet-potato-in-tomato-lime-and-cardamom-sauce",
            "category": "Veggie",
            "yield": "4",
            "source": "FLAVOUR by Ottolenghi",
            "time": "55 mins"
        },
        {
            "name": "Za'atar cacio e pepe",
            "image": "/images/za-atar-cacio-e-pepe.webp",
            "html": "/recipes/za-atar-cacio-e-pepe",
            "category": "Veggie",
            "yield": "4",
            "source": "FLAVOUR by Ottolenghi",
            "time": "25 mins",
            "cuisine": "Italian"
        },
        {
            "name": "Andy the gasman's stew",
            "image": "/images/andy-the-gasmans-stew.webp",
            "html": "/recipes/andy-the-gasmans-stew",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.jamieoliver.com",
            "time": "6 hr 30 mins",
            "cuisine": "British"
        },
        {
            "name": "Gnocchi alla sorrentina",
            "image": "/images/gnocchi-alla-sorrentina.webp",
            "html": "/recipes/gnocchi-alla-sorrentina",
            "category": "Veggie",
            "yield": "6-8",
            "source": "www.seriouseats.com",
            "time": "2 hr 30 mins",
            "cuisine": "Italian"
        },
        {
            "name": "Shrimp scampi",
            "image": "/images/shrimp-scampi.webp",
            "html": "/recipes/shrimp-scampi",
            "category": "Seafood",
            "yield": "4",
            "source": "basicswithbabish.co",
            "time": "15 mins"
        },
        {
            "name": "Riojan pork, chorizo and potato pot",
            "image": "/images/riojan-pork-chorizo-and-potato-pot.webp",
            "html": "/recipes/riojan-pork-chorizo-and-potato-pot",
            "category": "Pork",
            "yield": "4-6",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "1 hr 10 mins",
            "diet": [
                "DairyFree"
            ],
            "cuisine": "Spanish"
        },
        {
            "name": "Pasta e fagioli",
            "image": "/images/pasta-e-fagioli.webp",
            "html": "/recipes/pasta-e-fagioli",
            "category": "Pork",
            "yield": "6",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "1 hr 5 mins",
            "diet": [
                "DairyFree"
            ],
            "cuisine": "Italian"
        },
        {
            "name": "Cheesy taco pasta",
            "image": "/images/cheesy-taco-pasta.webp",
            "html": "/recipes/cheesy-taco-pasta",
            "category": "Veggie",
            "yield": "4",
            "source": "www.healthymummy.com",
            "time": "30 mins",
            "cuisine": "Mexican"
        },
        {
            "name": "Beef tagine with sweet potatoes, peas, ginger and ras el hanout",
            "image": "/images/beef-tagine-with-sweet-potatoes-peas-ginger-and-ras-el-hanout.webp",
            "html": "/recipes/beef-tagine-with-sweet-potatoes-peas-ginger-and-ras-el-hanout",
            "category": "RedMeat",
            "yield": "4",
            "source": "The Modern Tagine Cookbook",
            "time": "1 hr 15 mins",
            "cuisine": "Moroccan",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Jerked angry birds",
            "image": "/images/jerked-angry-birds.webp",
            "html": "/recipes/jerked-angry-birds",
            "category": "Poultry",
            "yield": "4",
            "source": "Pimp My Rice",
            "time": "1 hr 30 mins"
        },
        {
            "name": "Lemon garlic chicken orzo soup",
            "image": "/images/lemon-garlic-chicken-orzo-soup.webp",
            "html": "/recipes/lemon-garlic-chicken-orzo-soup",
            "category": "Poultry",
            "yield": "6",
            "source": "hostthetoast.com",
            "time": "1 hr 5 mins",
            "cuisine": "Greek"
        },
        {
            "name": "Slow cooker butter chicken",
            "image": "/images/slow-cooker-butter-chicken.webp",
            "html": "/recipes/slow-cooker-butter-chicken",
            "category": "Poultry",
            "yield": "4-6",
            "source": "cooking.nytimes.com",
            "time": "5 hr 10 mins",
            "cuisine": "Indian",
            "tags": [
                "slow-cooker"
            ]
        },
        {
            "name": "Sweet and sour tofu stir fry",
            "image": "/images/sweet-and-sour-tofu-stir-fry.webp",
            "html": "/recipes/sweet-and-sour-tofu-stir-fry",
            "category": "Veggie",
            "yield": "4",
            "source": "www.pickuplimes.com",
            "time": "30 mins",
            "diet": [
                "Vegan",
                "GlutenFree"
            ],
            "cuisine": "Asian"
        },
        {
            "name": "Sweet potato curry",
            "image": "/images/sweet-potato-curry.webp",
            "html": "/recipes/sweet-potato-curry",
            "category": "Veggie",
            "yield": "4",
            "source": "Slimming World",
            "time": "1 hr",
            "cuisine": "Indian"
        },
        {
            "name": "Citrus-spiced salmon",
            "image": "/images/citrus-spiced-salmon.webp",
            "html": "/recipes/citrus-spiced-salmon",
            "category": "Seafood",
            "yield": "6",
            "source": "Persiana",
            "time": "22 mins",
            "cuisine": "Middle Eastern"
        },
        {
            "name": "Spicy chicken tagine with apricots, rosemary and ginger",
            "image": "/images/spicy-chicken-tagine-with-apricots-rosemary-and-ginger.webp",
            "html": "/recipes/spicy-chicken-tagine-with-apricots-rosemary-and-ginger",
            "category": "Poultry",
            "yield": "4",
            "source": "The Modern Tagine Cookbook",
            "time": "1 hr 15 mins",
            "cuisine": "Moroccan"
        },
        {
            "name": "Festive roast leg of lamb",
            "image": "/images/festive-roast-leg-of-lamb.webp",
            "html": "/recipes/festive-roast-leg-of-lamb",
            "category": "RedMeat",
            "yield": "8",
            "source": "charlotteslivelykitchen.com",
            "time": "2 hr 25 mins",
            "cuisine": "British",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Key lime trifle",
            "image": "/images/key-lime-trifle.webp",
            "html": "/recipes/key-lime-trifle",
            "category": "Dessert",
            "yield": "8",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "35 mins"
        },
        {
            "name": "Ham and peas pasta with garlic parmesan cream sauce",
            "image": "/images/ham-and-peas-pasta-with-garlic-parmesan-cream-sauce.webp",
            "html": "/recipes/ham-and-peas-pasta-with-garlic-parmesan-cream-sauce",
            "category": "Pork",
            "yield": "4",
            "source": "damndelicious.net",
            "time": "30 mins"
        },
        {
            "name": "Spicy butternut squash pasta with spinach",
            "image": "/images/spicy-butternut-squash-pasta-with-spinach.webp",
            "html": "/recipes/spicy-butternut-squash-pasta-with-spinach",
            "category": "Veggie",
            "yield": "4",
            "source": "cooking.nytimes.com",
            "time": "40 mins"
        },
        {
            "name": "Beef & vegetable casserole",
            "image": "/images/beef-vegetable-casserole.webp",
            "html": "/recipes/beef-vegetable-casserole",
            "category": "RedMeat",
            "yield": "5",
            "source": "www.bbcgoodfood.com",
            "time": "4 hr 5 mins",
            "cuisine": "British",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Creamy tomato and spinach pasta",
            "image": "/images/creamy-tomato-and-spinach-pasta.webp",
            "html": "/recipes/creamy-tomato-and-spinach-pasta",
            "category": "Veggie",
            "yield": "4",
            "source": "www.budgetbytes.com",
            "time": "25 mins"
        },
        {
            "name": "Sticky maple chicken traybake",
            "image": "/images/sticky-maple-chicken-traybake.webp",
            "html": "/recipes/sticky-maple-chicken-traybake",
            "category": "Poultry",
            "yield": "4",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "55 mins"
        },
        {
            "name": "Spiced lamb meatball stew",
            "image": "/images/spiced-lamb-meatball-stew.webp",
            "html": "/recipes/spiced-lamb-meatball-stew",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "40 mins",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "5-a-day turmeric traybake chicken",
            "image": "/images/5-a-day-turmeric-traybake-chicken.webp",
            "html": "/recipes/5-a-day-turmeric-traybake-chicken",
            "category": "Poultry",
            "yield": "2",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "45 mins"
        },
        {
            "name": "Loaded chilli sweet potato fries",
            "image": "/images/loaded-chili-sweet-potato-fries.webp",
            "html": "/recipes/loaded-chili-sweet-potato-fries",
            "category": "Veggie",
            "yield": "3",
            "source": "www.pickuplimes.com",
            "time": "1 hr",
            "diet": [
                "Vegan",
                "GlutenFree",
                "NutFree"
            ],
            "cuisine": "American"
        },
        {
            "name": "Easy peasy risotto with chilli & mint crumbs",
            "image": "/images/easy-peasy-risotto-with-chilli-mint-crumbs.webp",
            "html": "/recipes/easy-peasy-risotto-with-chilli-mint-crumbs",
            "category": "Veggie",
            "yield": "3",
            "source": "BBC Good Food: Cheap & Healthy",
            "time": "50 mins"
        },
        {
            "name": "Original flava jerk chicken",
            "image": "/images/original-flava-jerk-chicken.webp",
            "html": "/recipes/original-flava-jerk-chicken",
            "category": "Poultry",
            "yield": "4",
            "source": "www.asda.com",
            "time": "1 hr 15 mins",
            "cuisine": "Caribbean"
        },
        {
            "name": "Smoked fish and parsnip cakes",
            "image": "/images/smoked-fish-and-parsnip-cakes.webp",
            "html": "/recipes/smoked-fish-and-parsnip-cakes",
            "category": "Seafood",
            "yield": "12",
            "source": "SIMPLE by Ottolenghi",
            "time": "55 mins"
        },
        {
            "name": "Sloppy joes",
            "image": "/images/sloppy-joes.webp",
            "html": "/recipes/sloppy-joes",
            "category": "RedMeat",
            "yield": "12",
            "source": "cooking.nytimes.com",
            "time": "1 hr 20 mins",
            "cuisine": "American",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Broccoli pesto & pancetta pasta",
            "image": "/images/broccoli-pesto-pancetta-pasta.webp",
            "html": "/recipes/broccoli-pesto-pancetta-pasta",
            "category": "Pork",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "25 mins",
            "cuisine": "Asian"
        },
        {
            "name": "Creamy white bean and spinach quesadillas",
            "image": "/images/creamy-white-bean-and-spinach-quesadillas.webp",
            "html": "/recipes/creamy-white-bean-and-spinach-quesadillas",
            "category": "Veggie",
            "yield": "4",
            "source": "www.budgetbytes.com",
            "time": "20 mins",
            "cuisine": "Mexican"
        },
        {
            "name": "Sausage, kale & gnocchi one-pot",
            "image": "/images/sausage-kale-gnocchi-one-pot.webp",
            "html": "/recipes/sausage-kale-gnocchi-one-pot",
            "category": "Pork",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "20 mins"
        },
        {
            "name": "Double bean & roasted pepper chilli",
            "image": "/images/double-bean-roasted-pepper-chilli.webp",
            "html": "/recipes/double-bean-roasted-pepper-chilli",
            "category": "Veggie",
            "yield": "8",
            "source": "www.bbcgoodfood.com",
            "time": "1 hr 45 mins",
            "diet": [
                "GlutenFree"
            ]
        },
        {
            "name": "Ground turkey stir fry",
            "image": "/images/ground-turkey-stir-fry.webp",
            "html": "/recipes/ground-turkey-stir-fry",
            "category": "Poultry",
            "yield": "5",
            "source": "www.budgetbytes.com",
            "time": "25 mins"
        },
        {
            "name": "Easy bbq chicken sandwiches",
            "image": "/images/easy-bbq-chicken-sandwiches.webp",
            "html": "/recipes/easy-bbq-chicken-sandwiches",
            "category": "Poultry",
            "yield": "4",
            "source": "www.budgetbytes.com",
            "time": "25 mins"
        },
        {
            "name": "Teriyaki meatball bowls",
            "image": "/images/teriyaki-meatball-bowls.webp",
            "html": "/recipes/teriyaki-meatball-bowls",
            "category": "Pork",
            "yield": "4",
            "source": "www.budgetbytes.com",
            "time": "40 mins"
        },
        {
            "name": "Macaroni chili",
            "image": "/images/macaroni-chili.webp",
            "html": "/recipes/macaroni-chili",
            "category": "Veggie",
            "yield": "4",
            "source": "www.pickuplimes.com",
            "time": "25 mins",
            "diet": [
                "Vegan",
                "DairyFree",
                "NutFree"
            ],
            "cuisine": "American"
        },
        {
            "name": "Mango & lime yogurt loaf",
            "image": "/images/mango-lime-yogurt-loaf.webp",
            "html": "/recipes/mango-lime-yogurt-loaf",
            "category": "Dessert",
            "yield": "10",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "1 hr 20 mins",
            "tags": [
                "cake"
            ]
        },
        {
            "name": "Cajun sausage and rice skillet",
            "image": "/images/cajun-sausage-and-rice-skillet.webp",
            "html": "/recipes/cajun-sausage-and-rice-skillet",
            "category": "Pork",
            "yield": "4",
            "source": "www.budgetbytes.com",
            "time": "45 mins",
            "cuisine": "Cajun"
        },
        {
            "name": "Beef curry",
            "image": "/images/beef-curry.webp",
            "html": "/recipes/beef-curry",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "2 hr 50 mins",
            "cuisine": "Indian",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "BBQ cauliflower pizza",
            "image": "/images/bbq-cauliflower-pizza.webp",
            "html": "/recipes/bbq-cauliflower-pizza",
            "category": "Veggie",
            "yield": "4",
            "source": "www.pickuplimes.com",
            "time": "1 hr 10 mins",
            "diet": [
                "Vegan",
                "DairyFree",
                "NutFree"
            ],
            "cuisine": "American"
        },
        {
            "name": "One pot creamy cajun chicken pasta",
            "image": "/images/one-pot-creamy-cajun-chicken-pasta.webp",
            "html": "/recipes/one-pot-creamy-cajun-chicken-pasta",
            "category": "Poultry",
            "yield": "4",
            "source": "www.budgetbytes.com",
            "time": "30 mins",
            "cuisine": "Cajun"
        },
        {
            "name": "Pesto shrimp pasta",
            "image": "/images/pesto-shrimp-pasta.webp",
            "html": "/recipes/pesto-shrimp-pasta",
            "category": "Seafood",
            "yield": "4",
            "source": "www.budgetbytes.com",
            "time": "25 mins"
        },
        {
            "name": "Slow-cooked beef ragout ",
            "image": "/images/slow-cooked-beef-ragout.webp",
            "html": "/recipes/slow-cooked-beef-ragout",
            "category": "RedMeat",
            "yield": "12",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "4 hr",
            "cuisine": "French",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "West indian rice and beans",
            "image": "/images/west-indian-rice-and-beans.webp",
            "html": "/recipes/west-indian-rice-and-beans",
            "category": "Veggie",
            "yield": "4",
            "source": "www.seriouseats.com",
            "time": "35 mins",
            "cuisine": "Caribbean"
        },
        {
            "name": "White beans with mushrooms and marinara",
            "image": "/images/white-beans-with-mushrooms-and-marinara.webp",
            "html": "/recipes/white-beans-with-mushrooms-and-marinara",
            "category": "Veggie",
            "yield": "4",
            "source": "www.budgetbytes.com",
            "time": "30 mins"
        },
        {
            "name": "Fishcake tacos with mango, lime and cumin yoghurt",
            "image": "/images/fishcake-tacos-with-mango-lime-and-cumin-yoghurt.webp",
            "html": "/recipes/fishcake-tacos-with-mango-lime-and-cumin-yoghurt",
            "category": "Seafood",
            "yield": "12",
            "source": "SIMPLE by Ottolenghi",
            "time": "35 mins",
            "cuisine": "Mexican"
        },
        {
            "name": "Harissa & lamb sausage rolls",
            "image": "/images/harissa-lamb-sausage-rolls.webp",
            "html": "/recipes/harissa-lamb-sausage-rolls",
            "category": "RedMeat",
            "yield": "8",
            "source": "www.bbcgoodfood.com",
            "time": "45 mins",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Antipasto tortellini pasta salad",
            "image": "/images/antipasto-tortellini-pasta-salad.webp",
            "html": "/recipes/antipasto-tortellini-pasta-salad",
            "category": "Pork",
            "yield": "8",
            "source": "hostthetoast.com",
            "time": "28 mins",
            "cuisine": "Italian"
        },
        {
            "name": "Bean burger",
            "image": "/images/bean-burger.webp",
            "html": "/recipes/bean-burger",
            "category": "Veggie",
            "yield": "4",
            "source": "sortedfood.com",
            "time": "20 mins"
        },
        {
            "name": "Lemony pasta with zucchini and fresh herbs",
            "image": "/images/lemony-pasta-with-zucchini-and-fresh-herbs.webp",
            "html": "/recipes/lemony-pasta-with-zucchini-and-fresh-herbs",
            "category": "Veggie",
            "yield": "4",
            "source": "cooking.nytimes.com",
            "time": "30 mins",
            "cuisine": "Italian"
        },
        {
            "name": "Turkey chilli",
            "image": "/images/turkey-chilli.webp",
            "html": "/recipes/turkey-chilli",
            "category": "Poultry",
            "yield": "6",
            "source": "cooking.nytimes.com",
            "time": "45 mins"
        },
        {
            "name": "Cheat's sausage cassoulet",
            "image": "/images/cheats-sausage-cassoulet.webp",
            "html": "/recipes/cheats-sausage-cassoulet",
            "category": "Pork",
            "yield": "3",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "35 mins",
            "diet": [
                "DairyFree"
            ],
            "cuisine": "British"
        },
        {
            "name": "Jerk chicken burger",
            "image": "/images/jerk-chicken-burger.webp",
            "html": "/recipes/jerk-chicken-burger",
            "category": "Poultry",
            "yield": "2",
            "source": "www.bbcgoodfood.com",
            "time": "20 mins",
            "cuisine": "Caribbean"
        },
        {
            "name": "Portobello mushrooms with brioche and a poached egg",
            "image": "/images/portobello-mushrooms-with-brioche-and-a-poached-egg.webp",
            "html": "/recipes/portobello-mushrooms-with-brioche-and-a-poached-egg",
            "category": "Veggie",
            "yield": "4",
            "source": "SIMPLE by Ottolenghi",
            "time": "25 mins"
        },
        {
            "name": "Roasted pepper pasta salad",
            "image": "/images/roasted-pepper-pasta-salad.webp",
            "html": "/recipes/roasted-pepper-pasta-salad",
            "category": "Veggie",
            "yield": "4",
            "source": "theskullery.net",
            "time": "40 mins"
        },
        {
            "name": "Southwestern pasta salad",
            "image": "/images/southwestern-pasta-salad.webp",
            "html": "/recipes/southwestern-pasta-salad",
            "category": "Pork",
            "yield": "8",
            "source": "hostthetoast.com",
            "time": "40 mins",
            "cuisine": "American"
        },
        {
            "name": "Spanish fennel, olive and tomato rice",
            "image": "/images/spanish-fennel-olive-and-tomato-rice.webp",
            "html": "/recipes/spanish-fennel-olive-and-tomato-rice",
            "category": "Veggie",
            "yield": "6",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "1 hr 10 mins",
            "diet": [
                "Vegan",
                "GlutenFree",
                "DairyFree"
            ],
            "cuisine": "Spanish"
        },
        {
            "name": "Spicy pies with sweet potato mash",
            "image": "/images/spicy-pies-with-sweet-potato-mash.webp",
            "html": "/recipes/spicy-pies-with-sweet-potato-mash",
            "category": "RedMeat",
            "yield": "6",
            "source": "www.bbcgoodfood.com",
            "time": "1 hr",
            "tags": [
                "beef",
                "pie"
            ]
        },
        {
            "name": "Anchovy and samphire spaghetti",
            "image": "/images/anchovy-and-samphire-spaghetti.webp",
            "html": "/recipes/anchovy-and-samphire-spaghetti",
            "category": "Seafood",
            "yield": "4",
            "source": "SIMPLE by Ottolenghi",
            "time": "25 mins"
        },
        {
            "name": "Fettuccine with spiced cherry tomato sauce",
            "image": "/images/fettuccine-with-spiced-cherry-tomato-sauce.webp",
            "html": "/recipes/fettuccine-with-spiced-cherry-tomato-sauce",
            "category": "Veggie",
            "yield": "4",
            "source": "SIMPLE by Ottolenghi",
            "time": "1 hr 20 mins"
        },
        {
            "name": "Storecupboard pasta salad",
            "image": "/images/storecupboard-pasta-salad.webp",
            "html": "/recipes/storecupboard-pasta-salad",
            "category": "Seafood",
            "yield": "2",
            "source": "www.bbcgoodfood.com",
            "time": "15 mins",
            "cuisine": "Italian"
        },
        {
            "name": "Thai-style fishcakes",
            "image": "/images/thai-style-fishcakes.webp",
            "html": "/recipes/thai-style-fishcakes",
            "category": "Seafood",
            "yield": "2",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "20 mins",
            "diet": [
                "GlutenFree",
                "DairyFree"
            ],
            "cuisine": "Thai"
        },
        {
            "name": "Cinnamon-lemon pork with apricot couscous",
            "image": "/images/cinnamon-lemon-pork-with-apricot-couscous.webp",
            "html": "/recipes/cinnamon-lemon-pork-with-apricot-couscous",
            "category": "Pork",
            "yield": "4",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "40 mins",
            "cuisine": "Moroccan"
        },
        {
            "name": "Omurice (japanese rice omelet)",
            "image": "/images/omurice-japanese-rice-omelet.webp",
            "html": "/recipes/omurice-japanese-rice-omelet",
            "category": "Pork",
            "yield": "2",
            "source": "cooking.nytimes.com",
            "time": "20 mins",
            "cuisine": "Japanese"
        },
        {
            "name": "Tuna-macaroni salad",
            "image": "/images/tuna-macaroni-salad.webp",
            "html": "/recipes/tuna-macaroni-salad",
            "category": "Seafood",
            "yield": "6",
            "source": "cooking.nytimes.com",
            "time": "20 mins"
        },
        {
            "name": "Very easy lamb with red wine gravy",
            "image": "/images/very-easy-lamb-with-red-wine-gravy.webp",
            "html": "/recipes/very-easy-lamb-with-red-wine-gravy",
            "category": "RedMeat",
            "yield": "4",
            "source": "Freeze - Justine Pattison",
            "time": "30 mins",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "10-minute couscous salad",
            "image": "/images/10-minute-couscous-salad.webp",
            "html": "/recipes/10-minute-couscous-salad",
            "category": "Veggie",
            "yield": "2",
            "source": "www.bbcgoodfood.com",
            "time": "10 mins",
            "cuisine": "Mediterranean"
        },
        {
            "name": "Moroccan lamb salad with jewelled couscous",
            "image": "/images/moroccan-lamb-salad-with-jewelled-couscous.webp",
            "html": "/recipes/moroccan-lamb-salad-with-jewelled-couscous",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "40 mins",
            "cuisine": "Moroccan",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Smoked haddock and crayfish lasagne",
            "image": "/images/smoked-haddock-and-crayfish-lasagne.webp",
            "html": "/recipes/smoked-haddock-and-crayfish-lasagne",
            "category": "Seafood",
            "yield": "6",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "1 hr 20 mins"
        },
        {
            "name": "Curried mushroom flatbreads",
            "image": "/images/curried-mushroom-flatbreads.webp",
            "html": "/recipes/curried-mushroom-flatbreads",
            "category": "Veggie",
            "yield": "2",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "30 mins",
            "cuisine": "Indian"
        },
        {
            "name": "Singapore noodles with prawns",
            "image": "/images/singapore-noodles-with-prawns.webp",
            "html": "/recipes/singapore-noodles-with-prawns",
            "category": "Seafood",
            "yield": "2",
            "source": "www.bbcgoodfood.com",
            "time": "20 mins",
            "cuisine": "Asian"
        },
        {
            "name": "Spiced turkey burgers",
            "image": "/images/spiced-turkey-burgers.webp",
            "html": "/recipes/spiced-turkey-burgers",
            "category": "Poultry",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "20 mins",
            "cuisine": "British"
        },
        {
            "name": "Butter curried rice and egg",
            "image": "/images/butter-curried-rice-and-egg.webp",
            "html": "/recipes/butter-curried-rice-and-egg",
            "category": "Veggie",
            "yield": "4",
            "source": "mobkitchen.co.uk",
            "time": "45 mins"
        },
        {
            "name": "Minty salsa verde lamb with lemony potatoes and leeks",
            "image": "/images/minty-salsa-verde-lamb-with-lemony-potatoes-and-leeks.webp",
            "html": "/recipes/minty-salsa-verde-lamb-with-lemony-potatoes-and-leeks",
            "category": "RedMeat",
            "yield": "2",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "25 mins",
            "diet": [
                "GlutenFree"
            ],
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Pappardelle with rosemary and sausage ragu",
            "image": "/images/pappardelle-with-rosemary-and-sausage-ragu.webp",
            "html": "/recipes/pappardelle-with-rosemary-and-sausage-ragu",
            "category": "Pork",
            "yield": "4",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "35 mins",
            "cuisine": "Italian"
        },
        {
            "name": "Roasted tomato and white bean stew",
            "image": "/images/roasted-tomato-and-white-bean-stew.webp",
            "html": "/recipes/roasted-tomato-and-white-bean-stew",
            "category": "Veggie",
            "yield": "4",
            "source": "cooking.nytimes.com",
            "time": "45 mins",
            "diet": [
                "Vegan"
            ],
            "cuisine": "American"
        },
        {
            "name": "Leek, bacon & potato soup",
            "image": "/images/leek-bacon-potato-soup.webp",
            "html": "/recipes/leek-bacon-potato-soup",
            "category": "Pork",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "30 mins",
            "cuisine": "British"
        },
        {
            "name": "Spiced veggie shepherd's pies ",
            "image": "/images/spiced-veggie-shepherds-pies.webp",
            "html": "/recipes/spiced-veggie-shepherds-pies",
            "category": "Veggie",
            "yield": "4",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "1 hr 15 mins",
            "cuisine": "British",
            "tags": [
                "pie"
            ]
        },
        {
            "name": "Broccoli cheese soup",
            "image": "/images/broccoli-cheese-soup.webp",
            "html": "/recipes/broccoli-cheese-soup",
            "category": "Veggie",
            "yield": "6",
            "source": "www.seriouseats.com",
            "time": "1 hr"
        },
        {
            "name": "Butterscotch blondie bars",
            "image": "/images/butterscotch-blondie-bars.webp",
            "html": "/recipes/butterscotch-blondie-bars",
            "category": "Dessert",
            "yield": "15",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "50 mins"
        },
        {
            "name": "Citrus pepper pork with crushed potatoes",
            "image": "/images/citrus-pepper-pork-with-crushed-potatoes.webp",
            "html": "/recipes/citrus-pepper-pork-with-crushed-potatoes",
            "category": "Pork",
            "yield": "2",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "40 mins"
        },
        {
            "name": "Lemony chicken with roast veg and olives",
            "image": "/images/lemony-chicken-with-roast-veg-and-olives.webp",
            "html": "/recipes/lemony-chicken-with-roast-veg-and-olives",
            "category": "Poultry",
            "yield": "2",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "30 mins"
        },
        {
            "name": "Salmon and spring veg orzo",
            "image": "/images/salmon-and-spring-veg-orzo.webp",
            "html": "/recipes/salmon-and-spring-veg-orzo",
            "category": "Seafood",
            "yield": "4",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "30 mins"
        },
        {
            "name": "Salmon and tomato fusilli gratin",
            "image": "/images/salmon-and-tomato-fusilli-gratin.webp",
            "html": "/recipes/salmon-and-tomato-fusilli-gratin",
            "category": "Seafood",
            "yield": "4",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "30 mins",
            "cuisine": "French"
        },
        {
            "name": "Whole eggs in coconut masala",
            "image": "/images/whole-eggs-in-coconut-masala.webp",
            "html": "/recipes/whole-eggs-in-coconut-masala",
            "category": "Veggie",
            "yield": "4",
            "source": "www.bbc.co.uk",
            "time": "1 hr",
            "diet": [
                "GlutenFree",
                "DairyFree"
            ],
            "cuisine": "Indian"
        },
        {
            "name": "One pot taco beef rice skillet",
            "image": "/images/one-pot-taco-beef-rice-skillet.webp",
            "html": "/recipes/one-pot-taco-beef-rice-skillet",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.slimmingeats.com",
            "time": "45 mins",
            "cuisine": "American",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Honey mustard glazed pork with mash",
            "image": "/images/honey-mustard-glazed-pork-with-mash.webp",
            "html": "/recipes/honey-mustard-glazed-pork-with-mash",
            "category": "Pork",
            "yield": "4",
            "source": "www.hellofresh.co.uk",
            "time": "35 mins"
        },
        {
            "name": "Pancetta penne all'arrabbiata",
            "image": "/images/pancetta-penne-allarrabbiata.webp",
            "html": "/recipes/pancetta-penne-allarrabbiata",
            "category": "Pork",
            "yield": "2",
            "source": "www.hellofresh.co.uk",
            "time": "40 mins",
            "cuisine": "Italian"
        },
        {
            "name": "Vegetarian sausage cassoulet",
            "image": "/images/vegetarian-sausage-cassoulet.webp",
            "html": "/recipes/vegetarian-sausage-cassoulet",
            "category": "Veggie",
            "yield": "2",
            "source": "www.hellofresh.co.uk",
            "time": "30 mins",
            "cuisine": "French"
        },
        {
            "name": "Bean enchiladas",
            "image": "/images/bean-enchiladas.webp",
            "html": "/recipes/bean-enchiladas",
            "category": "Veggie",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "40 mins",
            "cuisine": "Mexican"
        },
        {
            "name": "Chicken pasta bake",
            "image": "/images/chicken-pasta-bake.webp",
            "html": "/recipes/chicken-pasta-bake",
            "category": "Poultry",
            "yield": "6",
            "source": "www.bbcgoodfood.com",
            "time": "1 hr 15 mins"
        },
        {
            "name": "Fish pie fillets",
            "image": "/images/fish-pie-fillets.webp",
            "html": "/recipes/fish-pie-fillets",
            "category": "Seafood",
            "yield": "4",
            "source": "BBC Good Food: Cheap & Healthy",
            "time": "20 mins",
            "tags": [
                "pie"
            ]
        },
        {
            "name": "Mushroom and sage pasta",
            "image": "/images/mushroom-and-sage-pasta.webp",
            "html": "/recipes/mushroom-and-sage-pasta",
            "category": "Veggie",
            "yield": "4",
            "source": "www.mobkitchen.com",
            "time": "30 mins"
        },
        {
            "name": "Moroccan chickpea soup",
            "image": "/images/moroccan-chickpea-soup.webp",
            "html": "/recipes/moroccan-chickpea-soup",
            "category": "Veggie",
            "yield": "4",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "35 mins",
            "cuisine": "Moroccan"
        },
        {
            "name": "Beef stroganoff",
            "image": "/images/beef-stroganoff.webp",
            "html": "/recipes/beef-stroganoff",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "45 mins",
            "cuisine": "Russian",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Lentil shepherds pie with celeriac & butter bean mash",
            "image": "/images/lentil-shepherds-pie-with-celeriac-butter-bean-mash.webp",
            "html": "/recipes/lentil-shepherds-pie-with-celeriac-butter-bean-mash",
            "category": "Veggie",
            "yield": "4",
            "source": "BBC Good Food: Cheap & Healthy",
            "time": "1 hr 10 mins",
            "tags": [
                "pie"
            ]
        },
        {
            "name": "Mango chicken meal bowls",
            "image": "/images/mango-chicken-meal-bowls.webp",
            "html": "/recipes/mango-chicken-meal-bowls",
            "category": "Poultry",
            "yield": "4",
            "source": "thegirlonbloor.com",
            "time": "40 mins",
            "cuisine": "American"
        },
        {
            "name": "Spicy chicken pasties",
            "image": "/images/spicy-chicken-pasties.webp",
            "html": "/recipes/spicy-chicken-pasties",
            "category": "Poultry",
            "yield": "6",
            "source": "The Great British Bake Off: How to Bake",
            "time": "1 hr 40 mins"
        },
        {
            "name": "Spicy noodles with spring onions & fried eggs",
            "image": "/images/spicy-noodles-with-spring-onions-fried-eggs.webp",
            "html": "/recipes/spicy-noodles-with-spring-onions-fried-eggs",
            "category": "Veggie",
            "yield": "4",
            "source": "BBC Good Food: Cheap & Healthy",
            "time": "20 mins"
        },
        {
            "name": "Spicy turkey sweet potatoes",
            "image": "/images/spicy-turkey-sweet-potatoes.webp",
            "html": "/recipes/spicy-turkey-sweet-potatoes",
            "category": "Poultry",
            "yield": "4",
            "source": "BBC Good Food: Cheap & Healthy",
            "time": "50 mins"
        },
        {
            "name": "Coconut-lime shrimp",
            "image": "/images/coconut-lime-shrimp.webp",
            "html": "/recipes/coconut-lime-shrimp",
            "category": "Seafood",
            "yield": "4",
            "source": "cooking.nytimes.com",
            "time": "25 mins"
        },
        {
            "name": "Greek lamb with smoked aubergine & minty broad beans",
            "image": "/images/greek-lamb-with-smoked-aubergine-minty-broad-beans.webp",
            "html": "/recipes/greek-lamb-with-smoked-aubergine-minty-broad-beans",
            "category": "RedMeat",
            "yield": "2",
            "source": "BBC Good Food: Cheap & Healthy",
            "time": "35 mins",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Honey-glazed salmon with charred clementine salsa",
            "image": "/images/honey-glazed-salmon-with-charred-clementine-salsa.webp",
            "html": "/recipes/honey-glazed-salmon-with-charred-clementine-salsa",
            "category": "Seafood",
            "yield": "2",
            "source": "Coop magazine",
            "time": "30 mins"
        },
        {
            "name": "Pasta puttanesca",
            "image": "/images/pasta-puttanesca.webp",
            "html": "/recipes/pasta-puttanesca",
            "category": "Veggie",
            "yield": "4",
            "source": "cooking.nytimes.com",
            "time": "30 mins",
            "cuisine": "Italian"
        },
        {
            "name": "Spaghetti with smoky tomato & seafood sauce",
            "image": "/images/spaghetti-with-smoky-tomato-seafood-sauce.webp",
            "html": "/recipes/spaghetti-with-smoky-tomato-seafood-sauce",
            "category": "Seafood",
            "yield": "4",
            "source": "BBC Good Food: Cheap & Healthy",
            "time": "20 mins"
        },
        {
            "name": "Chorizo & chickpea stew",
            "image": "/images/chorizo-chickpea-stew.webp",
            "html": "/recipes/chorizo-chickpea-stew",
            "category": "Pork",
            "yield": "2",
            "source": "www.bbcgoodfood.com",
            "time": "15 mins",
            "cuisine": "Spanish"
        },
        {
            "name": "Creamy chickpea pasta with spinach and rosemary",
            "image": "/images/creamy-chickpea-pasta-with-spinach-and-rosemary.webp",
            "html": "/recipes/creamy-chickpea-pasta-with-spinach-and-rosemary",
            "category": "Veggie",
            "yield": "4",
            "source": "cooking.nytimes.com",
            "time": "30 mins",
            "cuisine": "Italian"
        },
        {
            "name": "Fettuccine with asparagus and smoked salmon",
            "image": "/images/fettuccine-with-asparagus-and-smoked-salmon.webp",
            "html": "/recipes/fettuccine-with-asparagus-and-smoked-salmon",
            "category": "Seafood",
            "yield": "4",
            "source": "cooking.nytimes.com",
            "time": "35 mins",
            "cuisine": "Italian"
        },
        {
            "name": "Satay sweet potato curry",
            "image": "/images/satay-sweet-potato-curry.webp",
            "html": "/recipes/satay-sweet-potato-curry",
            "category": "Veggie",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "1 hr",
            "diet": [
                "Vegan"
            ],
            "cuisine": "Asian"
        },
        {
            "name": "Scouse",
            "image": "/images/scouse.webp",
            "html": "/recipes/scouse",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.theguardian.com",
            "time": "2 hr 25 mins",
            "cuisine": "British",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Sumac-spiced lamb pides",
            "image": "/images/sumac-spiced-lamb-pides.webp",
            "html": "/recipes/sumac-spiced-lamb-pides",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "1 hr 15 mins",
            "cuisine": "Turkish",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Spiced shepherd's pie with butterbean crust",
            "image": "/images/spiced-shepherd-s-pie-with-butterbean-crust.webp",
            "html": "/recipes/spiced-shepherd-s-pie-with-butterbean-crust",
            "category": "RedMeat",
            "yield": "6",
            "source": "SIMPLE by Ottolenghi",
            "time": "1 hr 15 mins",
            "tags": [
                "lamb",
                "pie"
            ]
        },
        {
            "name": "Easy beef burritos",
            "image": "/images/easy-beef-burritos.webp",
            "html": "/recipes/easy-beef-burritos",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "45 mins",
            "cuisine": "Mexican",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Halal cart-style chicken and rice with white sauce",
            "image": "/images/halal-cart-style-chicken-and-rice-with-white-sauce-recipe.webp",
            "html": "/recipes/halal-cart-style-chicken-and-rice-with-white-sauce-recipe",
            "category": "Poultry",
            "yield": "4-6",
            "source": "www.seriouseats.com",
            "time": "1 hr",
            "cuisine": "American"
        },
        {
            "name": "Easy teriyaki chicken",
            "image": "/images/easy-teriyaki-chicken.webp",
            "html": "/recipes/easy-teriyaki-chicken",
            "category": "Poultry",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "20 mins"
        },
        {
            "name": "Sausage, sage and blackberry traybake",
            "image": "/images/sausage-sage-and-blackberry-traybake.webp",
            "html": "/recipes/sausage-sage-and-blackberry-traybake",
            "category": "Pork",
            "yield": "4",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "1 hr 20 mins",
            "diet": [
                "GlutenFree",
                "DairyFree"
            ]
        },
        {
            "name": "Spicy coconut grilled steak",
            "image": "/images/spicy-coconut-grilled-steak.webp",
            "html": "/recipes/spicy-coconut-grilled-steak",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.bonappetit.com",
            "time": "4 hr 15 mins",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Spicy sweet potato, tomato, ricotta and egg bake",
            "image": "/images/spicy-sweet-potato-tomato-ricotta-and-egg-bake.webp",
            "html": "/recipes/spicy-sweet-potato-tomato-ricotta-and-egg-bake",
            "category": "Veggie",
            "yield": "6",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "50 mins",
            "diet": [
                "GlutenFree"
            ]
        },
        {
            "name": "Tomato soup cake",
            "image": "/images/tomato-soup-cake.webp",
            "html": "/recipes/tomato-soup-cake",
            "category": "Dessert",
            "yield": "12",
            "source": "Simon's Wife",
            "time": "45 mins",
            "cuisine": "American",
            "tags": [
                "cake"
            ]
        },
        {
            "name": "Tuna nicoise salad",
            "image": "/images/tuna-nicoise-salad.webp",
            "html": "/recipes/tuna-nicoise-salad",
            "category": "Seafood",
            "yield": "6-8",
            "source": "www.bonappetit.com",
            "time": "20 mins",
            "cuisine": "French"
        },
        {
            "name": "Vegetable and egg noodle ribbons",
            "image": "/images/vegetable-and-egg-noodle-ribbons.webp",
            "html": "/recipes/vegetable-and-egg-noodle-ribbons",
            "category": "Veggie",
            "yield": "4",
            "source": "The Half Hour Cook",
            "time": "10 mins"
        },
        {
            "name": "Lemon, pineapple and herb-roasted chicken",
            "image": "/images/lemon-pineapple-and-herb-roasted-chicken.webp",
            "html": "/recipes/lemon-pineapple-and-herb-roasted-chicken",
            "category": "Poultry",
            "yield": "2",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "1 hr",
            "diet": [
                "GlutenFree"
            ]
        },
        {
            "name": "Sausage ragu",
            "image": "/images/sausage-ragu.webp",
            "html": "/recipes/sausage-ragu",
            "category": "Pork",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "50 mins"
        },
        {
            "name": "Spicy mushroom & broccoli noodles",
            "image": "/images/spicy-mushroom-broccoli-noodles.webp",
            "html": "/recipes/spicy-mushroom-broccoli-noodles",
            "category": "Veggie",
            "yield": "2",
            "source": "www.bbcgoodfood.com",
            "time": "20 mins",
            "cuisine": "Asian"
        },
        {
            "name": "Smoky black bean and sweet potato vegan burgers",
            "image": "/images/smoky-black-bean-and-sweet-potato-vegan-burgers.webp",
            "html": "/recipes/smoky-black-bean-and-sweet-potato-vegan-burgers",
            "category": "Veggie",
            "yield": "4",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "40 mins",
            "diet": [
                "Vegan",
                "GlutenFree",
                "DairyFree"
            ]
        },
        {
            "name": "Spanish fish pie",
            "image": "/images/spanish-fish-pie.webp",
            "html": "/recipes/spanish-fish-pie",
            "category": "Seafood",
            "yield": "6",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "1 hr",
            "cuisine": "Spanish",
            "tags": [
                "pie"
            ]
        },
        {
            "name": "Greek-style pasta",
            "image": "/images/greek-style-pasta.webp",
            "html": "/recipes/greek-style-pasta",
            "category": "Veggie",
            "yield": "4",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "25 mins",
            "cuisine": "Greek"
        },
        {
            "name": "Mexican sweet potato rice bowls",
            "image": "/images/mexican-sweet-potato-rice-bowls.webp",
            "html": "/recipes/mexican-sweet-potato-rice-bowls",
            "category": "Veggie",
            "yield": "2",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "40 mins",
            "diet": [
                "Vegan",
                "GlutenFree",
                "DairyFree"
            ],
            "cuisine": "Mexican"
        },
        {
            "name": "Spiced chickpea and sweet potato flatbreads",
            "image": "/images/spiced-chickpea-and-sweet-potato-flatbreads.webp",
            "html": "/recipes/spiced-chickpea-and-sweet-potato-flatbreads",
            "category": "Veggie",
            "yield": "2",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "25 mins",
            "cuisine": "Middle Eastern"
        },
        {
            "name": "Tuna sweetcorn burgers",
            "image": "/images/tuna-sweetcorn-burgers.webp",
            "html": "/recipes/tuna-sweetcorn-burgers",
            "category": "Seafood",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "15 mins",
            "cuisine": "American"
        },
        {
            "name": "Zingy lemon chicken casserole",
            "image": "/images/zingy-lemon-chicken-casserole.webp",
            "html": "/recipes/zingy-lemon-chicken-casserole",
            "category": "Poultry",
            "yield": "6",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "50 mins"
        },
        {
            "name": "Beef chow mein",
            "image": "/images/beef-chow-mein.webp",
            "html": "/recipes/beef-chow-mein",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "20 mins",
            "diet": [
                "DairyFree"
            ],
            "cuisine": "Chinese",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Broccoli bolognese with orecchiette",
            "image": "/images/broccoli-bolognese-with-orecchiette.webp",
            "html": "/recipes/broccoli-bolognese-with-orecchiette",
            "category": "Pork",
            "yield": "4",
            "source": "www.bonappetit.com",
            "time": "35 mins"
        },
        {
            "name": "Cowboy bean bake",
            "image": "/images/cowboy-bean-bake.webp",
            "html": "/recipes/cowboy-bean-bake",
            "category": "Pork",
            "yield": "4",
            "source": "Kat",
            "time": "55 mins"
        },
        {
            "name": "Crispy lamb noodles",
            "image": "/images/crispy-lamb-noodles.webp",
            "html": "/recipes/crispy-lamb-noodles",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "30 mins",
            "diet": [
                "DairyFree"
            ],
            "cuisine": "Asian",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Mediterranean fish roast",
            "image": "/images/mediterranean-fish-roast.webp",
            "html": "/recipes/mediterranean-fish-roast",
            "category": "Seafood",
            "yield": "4",
            "source": "slimmingworld.co.uk",
            "time": "45 mins",
            "cuisine": "Mediterranean"
        },
        {
            "name": "Peri peri pork with sweet potato wedges and sweetcorn salsa",
            "image": "/images/peri-peri-pork-with-sweet-potato-wedges-and-sweetcorn-salsa.webp",
            "html": "/recipes/peri-peri-pork-with-sweet-potato-wedges-and-sweetcorn-salsa",
            "category": "Pork",
            "yield": "4",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "30 mins",
            "diet": [
                "GlutenFree"
            ]
        },
        {
            "name": "Sausage meatball sandwiches",
            "image": "/images/sausage-meatball-sandwiches.webp",
            "html": "/recipes/sausage-meatball-sandwiches",
            "category": "Pork",
            "yield": "4",
            "source": "www.bonappetit.com",
            "time": "1 hr 15 mins"
        },
        {
            "name": "School dinner sponge cake",
            "image": "/images/school-dinner-sponge-cake.webp",
            "html": "/recipes/school-dinner-sponge-cake",
            "category": "Dessert",
            "yield": "20",
            "source": "glutenfreecuppatea.co.uk",
            "time": "1 hr 15 mins",
            "diet": [
                "GlutenFree",
                "DairyFree"
            ],
            "tags": [
                "cake"
            ]
        },
        {
            "name": "Spicy coconut grilled chicken thighs",
            "image": "/images/spicy-coconut-grilled-chicken-thighs.webp",
            "html": "/recipes/spicy-coconut-grilled-chicken-thighs",
            "category": "Poultry",
            "yield": "4",
            "source": "www.bonappetit.com",
            "time": "30 mins"
        },
        {
            "name": "Spinach and cashew dhal",
            "image": "/images/spinach-and-cashew-dhal.webp",
            "html": "/recipes/spinach-and-cashew-dhal",
            "category": "Veggie",
            "yield": "4",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "30 mins",
            "diet": [
                "GlutenFree",
                "DairyFree"
            ],
            "cuisine": "Indian"
        },
        {
            "name": "Curried haddock kedgeree",
            "image": "/images/curried-haddock-kedgeree.webp",
            "html": "/recipes/curried-haddock-kedgeree",
            "category": "Seafood",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "25 mins",
            "diet": [
                "GlutenFree"
            ]
        },
        {
            "name": "Fajita chicken traybake",
            "image": "/images/fajita-chicken-traybake.webp",
            "html": "/recipes/fajita-chicken-traybake",
            "category": "Poultry",
            "yield": "4",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "30 mins",
            "diet": [
                "GlutenFree"
            ],
            "cuisine": "Mexican"
        },
        {
            "name": "One-pan pancetta and chilli linguini",
            "image": "/images/one-pan-pancetta-and-chilli-linguini.webp",
            "html": "/recipes/one-pan-pancetta-and-chilli-linguini",
            "category": "Pork",
            "yield": "2",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "20 mins"
        },
        {
            "name": "Summer tagliatelle ragu",
            "image": "/images/summer-tagliatelle-ragu.webp",
            "html": "/recipes/summer-tagliatelle-ragu",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "35 mins",
            "cuisine": "Italian",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Spring vegetable and pancetta risotto",
            "image": "/images/spring-vegetable-and-pancetta-risotto.webp",
            "html": "/recipes/spring-vegetable-and-pancetta-risotto",
            "category": "Pork",
            "yield": "8",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "1 hr",
            "cuisine": "Italian"
        },
        {
            "name": "Barbecue pulled pork",
            "image": "/images/barbecue-pulled-pork.webp",
            "html": "/recipes/barbecue-pulled-pork",
            "category": "Pork",
            "yield": "4",
            "source": "www.slimmingworld.co.uk",
            "time": "10 hr 15 mins",
            "tags": [
                "slow-cooker"
            ]
        },
        {
            "name": "One pot pasta with tomato & mascarpone sauce",
            "image": "/images/one-pot-pasta-with-tomato-mascarpone-sauce.webp",
            "html": "/recipes/one-pot-pasta-with-tomato-mascarpone-sauce",
            "category": "Veggie",
            "yield": "5",
            "source": "www.happyveggiekitchen.com",
            "time": "30 mins",
            "cuisine": "Italian"
        },
        {
            "name": "Harissa and manchego omelettes",
            "image": "/images/harissa-and-manchego-omelettes.webp",
            "html": "/recipes/harissa-and-manchego-omelettes",
            "category": "Veggie",
            "yield": "4",
            "source": "SIMPLE by Ottolenghi",
            "time": "25 mins"
        },
        {
            "name": "Vegan chickpea tikka masala",
            "image": "/images/vegan-chickpea-tikka-masala.webp",
            "html": "/recipes/vegan-chickpea-tikka-masala",
            "category": "Veggie",
            "yield": "6",
            "source": "www.thewanderlustkitchen.com",
            "time": "45 mins",
            "diet": [
                "Vegan"
            ],
            "cuisine": "Indian"
        },
        {
            "name": "Vegetable tagine",
            "image": "/images/vegetable-tagine.webp",
            "html": "/recipes/vegetable-tagine",
            "category": "Veggie",
            "yield": "4",
            "source": "www.slimmingworld.co.uk",
            "time": "1 hr 10 mins"
        },
        {
            "name": "Salmon and broccoli pot pies",
            "image": "/images/salmon-and-broccoli-pot-pies.webp",
            "html": "/recipes/salmon-and-broccoli-pot-pies",
            "category": "Seafood",
            "yield": "4",
            "source": "Sainsbury's",
            "time": "1 hr 5 mins",
            "tags": [
                "pie"
            ]
        },
        {
            "name": "Chargrilled peppers with couscous",
            "image": "/images/chargrilled-peppers-with-couscous.webp",
            "html": "/recipes/chargrilled-peppers-with-couscous",
            "category": "Veggie",
            "yield": "1",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "10 mins"
        },
        {
            "name": "Hawaiian shoyu chicken",
            "image": "/images/hawaiian-shoyu-chicken.webp",
            "html": "/recipes/hawaiian-shoyu-chicken",
            "category": "Poultry",
            "yield": "6",
            "source": "www.thewanderlustkitchen.com",
            "time": "1 hr 5 mins",
            "cuisine": "Hawaiian"
        },
        {
            "name": "Spicy-sweet sambal pork noodles",
            "image": "/images/spicy-sweet-sambal-pork-noodles.webp",
            "html": "/recipes/spicy-sweet-sambal-pork-noodles",
            "category": "Pork",
            "yield": "6",
            "source": "www.bonappetit.com",
            "time": "1 hr",
            "cuisine": "Asian"
        },
        {
            "name": "Tikka salmon traybake",
            "image": "/images/tikka-salmon-traybake.webp",
            "html": "/recipes/tikka-salmon-traybake",
            "category": "Seafood",
            "yield": "2",
            "source": "www.sainsburysmagazine.co.uk",
            "time": "30 mins"
        },
        {
            "name": "White beans and pasta with rosemary pesto",
            "image": "/images/white-beans-and-pasta-with-rosemary-pesto.webp",
            "html": "/recipes/white-beans-and-pasta-with-rosemary-pesto",
            "category": "Veggie",
            "yield": "3",
            "source": "www.thewanderlustkitchen.com",
            "time": "30 mins",
            "cuisine": "Italian"
        },
        {
            "name": "Chorizo & chilli rigatoni",
            "image": "/images/chorizo-chilli-rigatoni.webp",
            "html": "/recipes/chorizo-chilli-rigatoni",
            "category": "Pork",
            "yield": "3",
            "source": "Nosh: Quick & Easy",
            "time": "20 mins",
            "diet": [
                "GlutenFree"
            ]
        },
        {
            "name": "Starbucks lemon loaf",
            "image": "/images/starbucks-lemon-loaf.webp",
            "html": "/recipes/starbucks-lemon-loaf",
            "category": "Dessert",
            "yield": "10",
            "source": "12tomatoes.com",
            "time": "1 hr 10 mins",
            "cuisine": "American",
            "tags": [
                "cake"
            ]
        },
        {
            "name": "Indian chicken korma",
            "image": "/images/indian-chicken-korma.webp",
            "html": "/recipes/indian-chicken-korma",
            "category": "Poultry",
            "yield": "6",
            "source": "www.thewanderlustkitchen.com",
            "time": "3 hr 25 mins",
            "cuisine": "Indian"
        },
        {
            "name": "Goan prawn, potato & coconut curry",
            "image": "/images/goan-prawn-potato-coconut-curry.webp",
            "html": "/recipes/goan-prawn-potato-coconut-curry",
            "category": "Seafood",
            "yield": "6",
            "source": "www.bbcgoodfood.com",
            "time": "1 hr 25 mins",
            "diet": [
                "GlutenFree"
            ],
            "cuisine": "Indian"
        },
        {
            "name": "Pasta with broad beans",
            "image": "/images/pasta-with-broad-beans.webp",
            "html": "/recipes/pasta-with-broad-beans",
            "category": "Veggie",
            "yield": "4",
            "source": "BBC Good Food: Pasta & Noodle Dishes",
            "time": "30 mins"
        },
        {
            "name": "Jerk pork with mango and bean rice",
            "image": "/images/jerk-pork-with-mango-and-bean-rice.webp",
            "html": "/recipes/jerk-pork-with-mango-and-bean-rice",
            "category": "Pork",
            "yield": "4",
            "source": "Morrisons",
            "time": "30 mins",
            "cuisine": "Caribbean"
        },
        {
            "name": "Spiced mushroom & lentil hotpot",
            "image": "/images/spiced-mushroom-lentil-hotpot.webp",
            "html": "/recipes/spiced-mushroom-lentil-hotpot",
            "category": "Veggie",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "45 mins",
            "diet": [
                "Vegan"
            ]
        },
        {
            "name": "Ants climbing trees",
            "image": "/images/ants-climbing-trees.webp",
            "html": "/recipes/ants-climbing-trees",
            "category": "Pork",
            "yield": "4",
            "source": "Pimp My Rice",
            "time": "50 mins"
        },
        {
            "name": "Salmon & saffron risotto",
            "image": "/images/salmon-saffron-risotto.webp",
            "html": "/recipes/salmon-saffron-risotto",
            "category": "Seafood",
            "yield": "4",
            "source": "Pimp My Rice",
            "time": "40 mins"
        },
        {
            "name": "Tagliatelle with goat's cheese",
            "image": "/images/tagliatelle-with-goat-s-cheese.webp",
            "html": "/recipes/tagliatelle-with-goat-s-cheese",
            "category": "Veggie",
            "yield": "2",
            "source": "BBC Good Food: Pasta & Noodle Dishes",
            "time": "20 mins"
        },
        {
            "name": "Pimped rice piri piri",
            "image": "/images/pimped-rice-piri-piri.webp",
            "html": "/recipes/pimped-rice-piri-piri",
            "category": "Poultry",
            "yield": "2",
            "source": "Pimp My Rice",
            "time": "1 hr 35 mins"
        },
        {
            "name": "Jacket potatoes with egg and tonnato sauce",
            "image": "/images/jacket-potatoes-with-egg-and-tonnato-sauce.webp",
            "html": "/recipes/jacket-potatoes-with-egg-and-tonnato-sauce",
            "category": "Veggie",
            "yield": "4",
            "source": "SIMPLE by Ottolenghi",
            "time": "1 hr 5 mins"
        },
        {
            "name": "Tomato pork bombs",
            "image": "/images/tomato-pork-bombs.webp",
            "html": "/recipes/tomato-pork-bombs",
            "category": "Pork",
            "yield": "4",
            "source": "Pimp My Rice",
            "time": "1 hr 10 mins"
        },
        {
            "name": "Mexican bean soup with guacamole",
            "image": "/images/mexican-bean-soup-with-guacamole.webp",
            "html": "/recipes/mexican-bean-soup-with-guacamole",
            "category": "Veggie",
            "yield": "2",
            "source": "www.bbcgoodfood.com",
            "time": "30 mins",
            "cuisine": "Mexican"
        },
        {
            "name": "Beef & ale slice pie",
            "image": "/images/beef-ale-slice-pie.webp",
            "html": "/recipes/beef-ale-slice-pie",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.mobkitchen.co.uk",
            "time": "4 hr",
            "tags": [
                "beef",
                "pie"
            ]
        },
        {
            "name": "Ginger steamed salmon with wild rice",
            "image": "/images/ginger-steamed-salmon-with-wild-rice.webp",
            "html": "/recipes/ginger-steamed-salmon-with-wild-rice",
            "category": "Seafood",
            "yield": "4",
            "source": "Mob Kitchen",
            "time": "15 mins"
        },
        {
            "name": "Spaghetti with tomato, chilli & tuna salsa",
            "image": "/images/spaghetti-with-tomato-chilli-tuna-salsa.webp",
            "html": "/recipes/spaghetti-with-tomato-chilli-tuna-salsa",
            "category": "Seafood",
            "yield": "4",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "20 mins"
        },
        {
            "name": "Chocolate chilli con carne",
            "image": "/images/chocolate-chilli-con-carne.webp",
            "html": "/recipes/chocolate-chilli-con-carne",
            "category": "RedMeat",
            "yield": "4",
            "source": "Mob Kitchen",
            "time": "50 mins",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Za'atar chicken & new potato bake with tzatziki",
            "image": "/images/za-atar-chicken-new-potato-bake-with-tzatziki.webp",
            "html": "/recipes/za-atar-chicken-new-potato-bake-with-tzatziki",
            "category": "Poultry",
            "yield": "4",
            "source": "Mob Kitchen",
            "time": "1 hr 10 mins"
        },
        {
            "name": "South indian-style coconut fish curry",
            "image": "/images/south-indian-style-coconut-fish-curry.webp",
            "html": "/recipes/south-indian-style-coconut-fish-curry",
            "category": "Seafood",
            "yield": "4",
            "source": "Mob Kitchen",
            "time": "20 mins",
            "cuisine": "Indian"
        },
        {
            "name": "Rich one-pan mushroom orzo risotto",
            "image": "/images/rich-one-pan-mushroom-orzo-risotto.webp",
            "html": "/recipes/rich-one-pan-mushroom-orzo-risotto",
            "category": "Veggie",
            "yield": "4",
            "source": "Mob Kitchen",
            "time": "25 mins"
        },
        {
            "name": "Bean, pepper and chorizo stew",
            "image": "/images/bean-pepper-and-chorizo-stew.webp",
            "html": "/recipes/bean-pepper-and-chorizo-stew",
            "category": "Pork",
            "yield": "4",
            "source": "Mob Kitchen",
            "time": "50 mins"
        },
        {
            "name": "Curried lentil, tomato and coconut soup",
            "image": "/images/curried-lentil-tomato-and-coconut-soup.webp",
            "html": "/recipes/curried-lentil-tomato-and-coconut-soup",
            "category": "Veggie",
            "yield": "4",
            "source": "SIMPLE by Ottolenghi",
            "time": "40 mins"
        },
        {
            "name": "Pan-fried chicken with creamy beans & leeks",
            "image": "/images/pan-fried-chicken-with-creamy-beans-leeks.webp",
            "html": "/recipes/pan-fried-chicken-with-creamy-beans-leeks",
            "category": "Poultry",
            "yield": "4",
            "source": "Easy Comfort Food",
            "time": "55 mins"
        },
        {
            "name": "Florentina pizza",
            "image": "/images/florentina-pizza.webp",
            "html": "/recipes/florentina-pizza",
            "category": "Veggie",
            "yield": "4",
            "source": "Easy Comfort Food",
            "time": "30 mins",
            "cuisine": "Italian"
        },
        {
            "name": "Ricotta and oregano meatballs",
            "image": "/images/ricotta-and-oregano-meatballs.webp",
            "html": "/recipes/ricotta-and-oregano-meatballs",
            "category": "RedMeat",
            "yield": "4",
            "source": "SIMPLE by Ottolenghi",
            "time": "1 hr 20 mins",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Fragrant rice with chilli vegetables",
            "image": "/images/fragrant-rice-with-chilli-vegetables.webp",
            "html": "/recipes/fragrant-rice-with-chilli-vegetables",
            "category": "Veggie",
            "yield": "4",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "45 mins"
        },
        {
            "name": "Prawn, chorizo & basil linguine",
            "image": "/images/prawn-chorizo-basil-linguine.webp",
            "html": "/recipes/prawn-chorizo-basil-linguine",
            "category": "Pork",
            "yield": "4",
            "source": "Mob Kitchen",
            "time": "20 mins"
        },
        {
            "name": "The almighty mob chicken pie",
            "image": "/images/the-almighty-mob-chicken-pie.webp",
            "html": "/recipes/the-almighty-mob-chicken-pie",
            "category": "Poultry",
            "yield": "4",
            "source": "www.mobkitchen.co.uk",
            "time": "1 hr",
            "tags": [
                "pie"
            ]
        },
        {
            "name": "Puy lentil and aubergine stew",
            "image": "/images/puy-lentil-and-aubergine-stew.webp",
            "html": "/recipes/puy-lentil-and-aubergine-stew",
            "category": "Veggie",
            "yield": "2",
            "source": "SIMPLE by Ottolenghi",
            "time": "40 mins"
        },
        {
            "name": "Orzo with prawns, tomato and marinated feta",
            "image": "/images/orzo-with-prawns-tomato-and-marinated-feta.webp",
            "html": "/recipes/orzo-with-prawns-tomato-and-marinated-feta",
            "category": "Seafood",
            "yield": "4",
            "source": "SIMPLE by Ottolenghi",
            "time": "40 mins"
        },
        {
            "name": "Sicilian lamb with noodles",
            "image": "/images/sicilian-lamb-with-noodles.webp",
            "html": "/recipes/sicilian-lamb-with-noodles",
            "category": "RedMeat",
            "yield": "6",
            "source": "BBC Good Food: Pasta & Noodle Dishes",
            "time": "1 hr",
            "cuisine": "Italian",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Nduja pesto pasta",
            "image": "/images/nduja-pesto-pasta.webp",
            "html": "/recipes/nduja-pesto-pasta",
            "category": "Veggie",
            "yield": "4",
            "source": "www.mobkitchen.co.uk",
            "time": "25 mins"
        },
        {
            "name": "Pasta with cherry tomato sauce",
            "image": "/images/pasta-with-cherry-tomato-sauce.webp",
            "html": "/recipes/pasta-with-cherry-tomato-sauce",
            "category": "Veggie",
            "yield": "4",
            "source": "BBC Good Food: Pasta & Noodle Dishes",
            "time": "20 mins"
        },
        {
            "name": "Spicy sausage & bean one-pot",
            "image": "/images/spicy-sausage-bean-one-pot.webp",
            "html": "/recipes/spicy-sausage-bean-one-pot",
            "category": "Pork",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "25 mins"
        },
        {
            "name": "Spaghetti with salmon and prawns",
            "image": "/images/spaghetti-with-salmon-and-prawns.webp",
            "html": "/recipes/spaghetti-with-salmon-and-prawns",
            "category": "Seafood",
            "yield": "4",
            "source": "The Half Hour Cook",
            "time": "30 mins"
        },
        {
            "name": "Spicy veggie enchiladas",
            "image": "/images/spicy-veggie-enchiladas.webp",
            "html": "/recipes/spicy-veggie-enchiladas",
            "category": "Veggie",
            "yield": "4",
            "source": "www.mobkitchen.co.uk",
            "time": "1 hr"
        },
        {
            "name": "Puerto rican chicken and rice",
            "image": "/images/puerto-rican-chicken-and-rice.webp",
            "html": "/recipes/puerto-rican-chicken-and-rice",
            "category": "Poultry",
            "yield": "4",
            "source": "Caribbean Food Made Easy",
            "time": "1 hr",
            "cuisine": "Caribbean"
        },
        {
            "name": "Spiced chicken, spinach & sweet potato stew",
            "image": "/images/spiced-chicken-spinach-sweet-potato-stew.webp",
            "html": "/recipes/spiced-chicken-spinach-sweet-potato-stew",
            "category": "Poultry",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "55 mins"
        },
        {
            "name": "Chicken & ham pie",
            "image": "/images/chicken-ham-pie.webp",
            "html": "/recipes/chicken-ham-pie",
            "category": "Poultry",
            "yield": "6",
            "source": "www.bbcgoodfood.com",
            "time": "1 hr 10 mins",
            "cuisine": "British",
            "tags": [
                "pie"
            ]
        },
        {
            "name": "Veggie rice pot",
            "image": "/images/veggie-rice-pot.webp",
            "html": "/recipes/veggie-rice-pot",
            "category": "Veggie",
            "yield": "4",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "35 mins"
        },
        {
            "name": "Courgette, sausage & rigatoni bake",
            "image": "/images/courgette-sausage-rigatoni-bake.webp",
            "html": "/recipes/courgette-sausage-rigatoni-bake",
            "category": "Pork",
            "yield": "2",
            "source": "BBC Good Food: Slow Cooker Favourites",
            "time": "1 hr",
            "tags": [
                "slow-cooker"
            ]
        },
        {
            "name": "Linguine with tuna sauce",
            "image": "/images/linguine-with-tuna-sauce.webp",
            "html": "/recipes/linguine-with-tuna-sauce",
            "category": "Seafood",
            "yield": "4",
            "source": "BBC Good Food: Slow Cooker Favourites",
            "time": "3 hr 35 mins",
            "tags": [
                "slow-cooker"
            ]
        },
        {
            "name": "Beef & lentil cottage pie with cauliflower & potato topping",
            "image": "/images/beef-lentil-cottage-pie-with-cauliflower-potato-topping.webp",
            "html": "/recipes/beef-lentil-cottage-pie-with-cauliflower-potato-topping",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "1 hr 40 mins",
            "cuisine": "British",
            "tags": [
                "beef",
                "pie"
            ]
        },
        {
            "name": "Spanish lamb with sherry, honey & peppers",
            "image": "/images/spanish-lamb-with-sherry-honey-peppers.webp",
            "html": "/recipes/spanish-lamb-with-sherry-honey-peppers",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "2 hr 45 mins",
            "cuisine": "Spanish",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Green summer orzo",
            "image": "/images/green-summer-orzo.webp",
            "html": "/recipes/green-summer-orzo",
            "category": "Veggie",
            "yield": "4",
            "source": "www.mobkitchen.co.uk",
            "time": "12 mins"
        },
        {
            "name": "Peri peri pasta",
            "image": "/images/peri-peri-pasta.webp",
            "html": "/recipes/peri-peri-pasta",
            "category": "Poultry",
            "yield": "4",
            "source": "www.mobkitchen.co.uk",
            "time": "1 hr"
        },
        {
            "name": "Pork and sweet potato meatballs",
            "image": "/images/pork-and-sweet-potato-meatballs.webp",
            "html": "/recipes/pork-and-sweet-potato-meatballs",
            "category": "Pork",
            "yield": "4",
            "source": "www.littlegrazers.com",
            "time": "50 mins"
        },
        {
            "name": "Fish, chips and mushy peas",
            "image": "/images/fish-chips-and-mushy-peas.webp",
            "html": "/recipes/fish-chips-and-mushy-peas",
            "category": "Seafood",
            "yield": "2",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "50 mins"
        },
        {
            "name": "Red Thai salmon curry",
            "image": "/images/red-thai-salmon-curry.webp",
            "html": "/recipes/red-thai-salmon-curry",
            "category": "Seafood",
            "yield": "4",
            "source": "BBC Good Food: Slow Cooker Favourites",
            "time": "2 hr 35 mins",
            "cuisine": "Thai",
            "tags": [
                "slow-cooker"
            ]
        },
        {
            "name": "Lentil bolognaise with parmesan basil and pine nuts",
            "image": "/images/lentil-bolognaise-with-parmesan-basil-and-pine-nuts.webp",
            "html": "/recipes/lentil-bolognaise-with-parmesan-basil-and-pine-nuts",
            "category": "Veggie",
            "yield": "6",
            "source": "www.nadialim.com",
            "time": "50 mins",
            "diet": [
                "GlutenFree",
                "DairyFree"
            ]
        },
        {
            "name": "Lemon layer cake",
            "image": "/images/lemon-layer-cake.webp",
            "html": "/recipes/lemon-layer-cake",
            "category": "Dessert",
            "yield": "12",
            "source": "Sainsbury's",
            "time": "40 mins",
            "tags": [
                "cake"
            ]
        },
        {
            "name": "Chilli con carne pasties",
            "image": "/images/chilli-con-carne-pasties.webp",
            "html": "/recipes/chilli-con-carne-pasties",
            "category": "RedMeat",
            "yield": "12",
            "source": "Bake It Yourself",
            "time": "1 hr 15 mins",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Sweet potato & spinach bake",
            "image": "/images/sweet-potato-spinach-bake.webp",
            "html": "/recipes/sweet-potato-spinach-bake",
            "category": "Veggie",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "1 hr 15 mins",
            "cuisine": "British"
        },
        {
            "name": "Pot noodle",
            "image": "/images/pot-noodle.webp",
            "html": "/recipes/pot-noodle",
            "category": "Poultry",
            "yield": "1",
            "source": "www.annabelkarmel.com",
            "time": "15 mins",
            "cuisine": "Asian"
        },
        {
            "name": "Roast cod with pancetta",
            "image": "/images/roast-cod-with-pancetta.webp",
            "html": "/recipes/roast-cod-with-pancetta",
            "category": "Seafood",
            "yield": "4",
            "source": "The Half Hour Cook",
            "time": "22 mins"
        },
        {
            "name": "Caribbean beef patties",
            "image": "/images/caribbean-beef-patties.webp",
            "html": "/recipes/caribbean-beef-patties",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "1 hr",
            "cuisine": "Caribbean",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Beef strips with orange and ginger",
            "image": "/images/beef-strips-with-orange-and-ginger.webp",
            "html": "/recipes/beef-strips-with-orange-and-ginger",
            "category": "RedMeat",
            "yield": "4",
            "source": "The Half Hour Cook",
            "time": "20 mins",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Spiced lentils",
            "image": "/images/spiced-lentils.webp",
            "html": "/recipes/spiced-lentils",
            "category": "Veggie",
            "yield": "4",
            "source": "The Half Hour Cook",
            "time": "22 mins"
        },
        {
            "name": "Lentil & sweet potato curry",
            "image": "/images/lentil-sweet-potato-curry.webp",
            "html": "/recipes/lentil-sweet-potato-curry",
            "category": "Veggie",
            "yield": "2",
            "source": "www.bbcgoodfood.com",
            "time": "35 mins",
            "cuisine": "Indian"
        },
        {
            "name": "One-pot chicken & mushroom risotto",
            "image": "/images/one-pot-chicken-mushroom-risotto.webp",
            "html": "/recipes/one-pot-chicken-mushroom-risotto",
            "category": "Poultry",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "50 mins"
        },
        {
            "name": "Mediterranean pork stew",
            "image": "/images/mediterranean-pork-stew.webp",
            "html": "/recipes/mediterranean-pork-stew",
            "category": "Pork",
            "yield": "4",
            "source": "Weight Watchers Magazine",
            "time": "45 mins",
            "cuisine": "Mediterranean"
        },
        {
            "name": "Pad thai omelette",
            "image": "/images/pad-thai-omelette.webp",
            "html": "/recipes/pad-thai-omelette",
            "category": "Seafood",
            "yield": "2",
            "source": "The Hairy Dieters: Good Eating",
            "time": "35 mins",
            "cuisine": "Thai"
        },
        {
            "name": "Linguine with garlic butter prawns",
            "image": "/images/linguine-with-garlic-butter-prawns.webp",
            "html": "/recipes/linguine-with-garlic-butter-prawns",
            "category": "Seafood",
            "yield": "2",
            "source": "www.bbcgoodfood.com",
            "time": "20 mins",
            "cuisine": "Italian"
        },
        {
            "name": "Mushroom stroganoff",
            "image": "/images/mushroom-stroganoff.webp",
            "html": "/recipes/mushroom-stroganoff",
            "category": "Veggie",
            "yield": "4",
            "source": "The Half Hour Cook",
            "time": "20 mins"
        },
        {
            "name": "Chicken with lemon and garlic",
            "image": "/images/chicken-with-lemon-and-garlic.webp",
            "html": "/recipes/chicken-with-lemon-and-garlic",
            "category": "Poultry",
            "yield": "4",
            "source": "The Half Hour Cook",
            "time": "8 mins"
        },
        {
            "name": "Omelette Arnold Bennett",
            "image": "/images/omelette-arnold-bennett.webp",
            "html": "/recipes/omelette-arnold-bennett",
            "category": "Seafood",
            "yield": "2",
            "source": "The Half Hour Cook",
            "time": "14 mins"
        },
        {
            "name": "Lamb and spinach curry",
            "image": "/images/lamb-and-spinach-curry.webp",
            "html": "/recipes/lamb-and-spinach-curry",
            "category": "RedMeat",
            "yield": "4",
            "source": "BBC Good Food: Slow Cooker Favourites",
            "time": "2 hr 30 mins",
            "tags": [
                "lamb",
                "slow-cooker"
            ]
        },
        {
            "name": "Sweet potato & coconut curry",
            "image": "/images/sweet-potato-coconut-curry.webp",
            "html": "/recipes/sweet-potato-coconut-curry",
            "category": "Veggie",
            "yield": "6",
            "source": "www.bbcgoodfood.com",
            "time": "7 hr 50 mins",
            "diet": [
                "Vegan",
                "GlutenFree"
            ],
            "cuisine": "Indian",
            "tags": [
                "slow-cooker"
            ]
        },
        {
            "name": "Garlic butter steak and potatoes skillet",
            "image": "/images/garlic-butter-steak-and-potatoes-skillet.webp",
            "html": "/recipes/garlic-butter-steak-and-potatoes-skillet",
            "category": "RedMeat",
            "yield": "6",
            "source": "tipsforhealthy.life",
            "time": "30 mins",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Linguine with ham and mascarpone",
            "image": "/images/linguine-with-ham-and-mascarpone.webp",
            "html": "/recipes/linguine-with-ham-and-mascarpone",
            "category": "Pork",
            "yield": "6",
            "source": "The Half Hour Cook",
            "time": "10 mins"
        },
        {
            "name": "Bolognese with a healthy twist",
            "image": "/images/bolognese-with-a-healthy-twist.webp",
            "html": "/recipes/bolognese-with-a-healthy-twist",
            "category": "Poultry",
            "yield": "4",
            "source": "Sainsbury's",
            "time": "30 mins"
        },
        {
            "name": "Mexican tomato rice",
            "image": "/images/mexican-tomato-rice.webp",
            "html": "/recipes/mexican-tomato-rice",
            "category": "Veggie",
            "yield": "4",
            "source": "The Half Hour Cook",
            "time": "20 mins",
            "cuisine": "Mexican"
        },
        {
            "name": "Smoked paprika pork stew with peppers and potatoes",
            "image": "/images/smoked-paprika-pork-stew-with-peppers-and-potatoes.webp",
            "html": "/recipes/smoked-paprika-pork-stew-with-peppers-and-potatoes",
            "category": "Pork",
            "yield": "4",
            "source": "Sainsbury's",
            "time": "35 mins"
        },
        {
            "name": "Sea-fisherman's pie",
            "image": "/images/sea-fishermans-pie.webp",
            "html": "/recipes/sea-fishermans-pie",
            "category": "Seafood",
            "yield": "4",
            "source": "Bake It Yourself",
            "time": "55 mins",
            "tags": [
                "pie"
            ]
        },
        {
            "name": "Tex-Mex burrito",
            "image": "/images/tex-mex-burrito.webp",
            "html": "/recipes/tex-mex-burrito",
            "category": "Veggie",
            "yield": "2",
            "source": "www.bbcgoodfood.com",
            "time": "20 mins",
            "cuisine": "Mexican"
        },
        {
            "name": "Spanish meatball & butter bean stew",
            "image": "/images/spanish-meatball-butter-bean-stew.webp",
            "html": "/recipes/spanish-meatball-butter-bean-stew",
            "category": "Pork",
            "yield": "3",
            "source": "www.bbcgoodfood.com",
            "time": "50 mins",
            "diet": [
                "GlutenFree"
            ],
            "cuisine": "Spanish"
        },
        {
            "name": "Sticky sesame popcorn chicken",
            "image": "/images/sticky-sesame-popcorn-chicken.webp",
            "html": "/recipes/sticky-sesame-popcorn-chicken",
            "category": "Poultry",
            "yield": "4",
            "source": "www.mobkitchen.co.uk",
            "time": "40 mins"
        },
        {
            "name": "One-pan chicken couscous",
            "image": "/images/one-pan-chicken-couscous.webp",
            "html": "/recipes/one-pan-chicken-couscous",
            "category": "Poultry",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "30 mins",
            "cuisine": "Moroccan"
        },
        {
            "name": "Fragrant pork & rice one-pot",
            "image": "/images/fragrant-pork-rice-one-pot.webp",
            "html": "/recipes/fragrant-pork-rice-one-pot",
            "category": "Pork",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "45 mins",
            "cuisine": "Indian"
        },
        {
            "name": "Prawn and vegetable balti",
            "image": "/images/prawn-and-vegetable-balti.webp",
            "html": "/recipes/prawn-and-vegetable-balti",
            "category": "Seafood",
            "yield": "4",
            "source": "The Half Hour Cook",
            "time": "10 mins"
        },
        {
            "name": "Pea and mint omelette",
            "image": "/images/pea-and-mint-omelette.webp",
            "html": "/recipes/pea-and-mint-omelette",
            "category": "Veggie",
            "yield": "2",
            "source": "The Half Hour Cook",
            "time": "12 mins"
        },
        {
            "name": "Spaghetti carbonara 2",
            "image": "/images/spaghetti-carbonara-2.webp",
            "html": "/recipes/spaghetti-carbonara-2",
            "category": "Pork",
            "yield": "4",
            "source": "The Half Hour Cook",
            "time": "20 mins"
        },
        {
            "name": "Cauliflower, paneer & pea curry",
            "image": "/images/cauliflower-paneer-pea-curry.webp",
            "html": "/recipes/cauliflower-paneer-pea-curry",
            "category": "Veggie",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "45 mins",
            "cuisine": "Indian"
        },
        {
            "name": "Fusilli with sausage",
            "image": "/images/fusilli-with-sausage.webp",
            "html": "/recipes/fusilli-with-sausage",
            "category": "Pork",
            "yield": "4",
            "source": "The Half Hour Cook",
            "time": "20 mins"
        },
        {
            "name": "Spiced potatoes and tomatoes",
            "image": "/images/spiced-potatoes-and-tomatoes.webp",
            "html": "/recipes/spiced-potatoes-and-tomatoes",
            "category": "Veggie",
            "yield": "4",
            "source": "The Half Hour Cook",
            "time": "22 mins"
        },
        {
            "name": "Lemon and parmesan capellini",
            "image": "/images/lemon-and-parmesan-capellini.webp",
            "html": "/recipes/lemon-and-parmesan-capellini",
            "category": "Veggie",
            "yield": "2",
            "source": "The Half Hour Cook",
            "time": "17 mins"
        },
        {
            "name": "Ricotta pasta pockets",
            "image": "/images/ricotta-pasta-pockets.webp",
            "html": "/recipes/ricotta-pasta-pockets",
            "category": "Veggie",
            "yield": "4",
            "source": "BBC Good Food: Pasta & Noodle Dishes",
            "time": "30 mins"
        },
        {
            "name": "Easy chicken tikka masala with basmati rice",
            "image": "/images/easy-chicken-tikka-masala-with-basmati-rice.webp",
            "html": "/recipes/easy-chicken-tikka-masala-with-basmati-rice",
            "category": "Poultry",
            "yield": "4",
            "source": "Nadiya's British Food Adventure",
            "time": "1 hr 30 mins"
        },
        {
            "name": "Gooey pasta bake",
            "image": "/images/gooey-pasta-bake.webp",
            "html": "/recipes/gooey-pasta-bake",
            "category": "Veggie",
            "yield": "2",
            "source": "BBC Good Food: Pasta & Noodle Dishes",
            "time": "35 mins"
        },
        {
            "name": "Hungarian goulash",
            "image": "/images/hungarian-goulash.webp",
            "html": "/recipes/hungarian-goulash",
            "category": "RedMeat",
            "yield": "4",
            "source": "100 Best Slow Cooker Recipes",
            "time": "9 hr 50 mins",
            "cuisine": "Hungarian",
            "tags": [
                "beef",
                "slow-cooker"
            ]
        },
        {
            "name": "Prawn & orange curry with basmati rice",
            "image": "/images/prawn-orange-curry-with-basmati-rice.webp",
            "html": "/recipes/prawn-orange-curry-with-basmati-rice",
            "category": "Seafood",
            "yield": "3",
            "source": "Nadiya's British Food Adventure",
            "time": "55 mins",
            "cuisine": "Indian"
        },
        {
            "name": "Spiced bean & banger stew",
            "image": "/images/spiced-bean-banger-stew.webp",
            "html": "/recipes/spiced-bean-banger-stew",
            "category": "Pork",
            "yield": "4",
            "source": "Nadiya's British Food Adventure",
            "time": "1 hr 45 mins"
        },
        {
            "name": "Spiced shepherd's pie",
            "image": "/images/spiced-shepherd-s-pie.webp",
            "html": "/recipes/spiced-shepherd-s-pie",
            "category": "RedMeat",
            "yield": "4",
            "source": "BBC Good Food: Slow Cooker Favourites",
            "time": "8 hr 30 mins",
            "tags": [
                "lamb",
                "pie",
                "slow-cooker"
            ]
        },
        {
            "name": "Pearl barley, parsnip & preserved lemon tagine",
            "image": "/images/pearl-barley-parsnip-preserved-lemon-tagine.webp",
            "html": "/recipes/pearl-barley-parsnip-preserved-lemon-tagine",
            "category": "Veggie",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "1 hr 25 mins",
            "cuisine": "Moroccan"
        },
        {
            "name": "Minted lamb & pea stew",
            "image": "/images/minted-lamb-pea-stew.webp",
            "html": "/recipes/minted-lamb-pea-stew",
            "category": "RedMeat",
            "yield": "4",
            "source": "BBC Good Food: Slow Cooker Favourites",
            "time": "6 hr 20 mins",
            "tags": [
                "lamb",
                "slow-cooker"
            ]
        },
        {
            "name": "Lemon grass and ginger pork burgers",
            "image": "/images/lemon-grass-and-ginger-pork-burgers.webp",
            "html": "/recipes/lemon-grass-and-ginger-pork-burgers",
            "category": "Pork",
            "yield": "4",
            "source": "The Half Hour Cook",
            "time": "13 mins"
        },
        {
            "name": "Pepper and egg pizza",
            "image": "/images/pepper-and-egg-pizza.webp",
            "html": "/recipes/pepper-and-egg-pizza",
            "category": "Veggie",
            "yield": "4",
            "source": "The Half Hour Cook",
            "time": "27 mins"
        },
        {
            "name": "Tarte au citron",
            "image": "/images/tarte-au-citron.webp",
            "html": "/recipes/tarte-au-citron",
            "category": "Dessert",
            "yield": "8",
            "source": "www.bbc.co.uk",
            "time": "3 hr"
        },
        {
            "name": "Chicken with apples and cider",
            "image": "/images/chicken-with-apples-and-cider.webp",
            "html": "/recipes/chicken-with-apples-and-cider",
            "category": "Poultry",
            "yield": "4",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "35 mins"
        },
        {
            "name": "Spicy-vegetable chapatti wraps",
            "image": "/images/spicy-vegetable-chapatti-wraps.webp",
            "html": "/recipes/spicy-vegetable-chapatti-wraps",
            "category": "Veggie",
            "yield": "4",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "25 mins"
        },
        {
            "name": "Chicken & mushroom puff pie",
            "image": "/images/chicken-mushroom-puff-pie.webp",
            "html": "/recipes/chicken-mushroom-puff-pie",
            "category": "Poultry",
            "yield": "4-6",
            "source": "www.bbcgoodfood.com",
            "time": "1 hr 15 mins",
            "cuisine": "British",
            "tags": [
                "pie"
            ]
        },
        {
            "name": "Garlicky mushroom penne",
            "image": "/images/garlicky-mushroom-penne.webp",
            "html": "/recipes/garlicky-mushroom-penne",
            "category": "Veggie",
            "yield": "2",
            "source": "www.bbcgoodfood.com",
            "time": "35 mins",
            "diet": [
                "Vegan"
            ]
        },
        {
            "name": "Vegetarian shepherd's pie",
            "image": "/images/vegetarian-shepherds-pie.webp",
            "html": "/recipes/vegetarian-shepherds-pie",
            "category": "Veggie",
            "yield": "4-6",
            "source": "Leon Family & Friends",
            "time": "1 hr 20 mins",
            "tags": [
                "pie"
            ]
        },
        {
            "name": "Moroccan lamb",
            "image": "/images/moroccan-lamb.webp",
            "html": "/recipes/moroccan-lamb",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "45 mins",
            "cuisine": "Moroccan",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Curried cod",
            "image": "/images/curried-cod.webp",
            "html": "/recipes/curried-cod",
            "category": "Seafood",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "35 mins"
        },
        {
            "name": "Summer pasta with melting brie",
            "image": "/images/summer-pasta-with-melting-brie.webp",
            "html": "/recipes/summer-pasta-with-melting-brie",
            "category": "Veggie",
            "yield": "4",
            "source": "BBC Good Food: Pasta & Noodle Dishes",
            "time": "25 mins"
        },
        {
            "name": "Pork and rosemary lasagne",
            "image": "/images/pork-and-rosemary-lasagne.webp",
            "html": "/recipes/pork-and-rosemary-lasagne",
            "category": "Pork",
            "yield": "4",
            "source": "BBC Good Food: Pasta & Noodle Dishes",
            "time": "1 hr 30 mins"
        },
        {
            "name": "Cheesey corn and broccoli pasta",
            "image": "/images/cheesey-corn-and-broccoli-pasta.webp",
            "html": "/recipes/cheesey-corn-and-broccoli-pasta",
            "category": "Veggie",
            "yield": "4",
            "source": "BBC Good Food: Pasta & Noodle Dishes",
            "time": "20 mins"
        },
        {
            "name": "Caramel-lime chicken with caribbean mash",
            "image": "/images/caramel-lime-chicken-with-caribbean-mash.webp",
            "html": "/recipes/caramel-lime-chicken-with-caribbean-mash",
            "category": "Poultry",
            "yield": "4",
            "source": "Caribbean Food Made Easy",
            "time": "40 mins",
            "cuisine": "Caribbean"
        },
        {
            "name": "Sausages with puy lentils",
            "image": "/images/sausages-with-puy-lentils.webp",
            "html": "/recipes/sausages-with-puy-lentils",
            "category": "Pork",
            "yield": "4",
            "source": "www.deliaonline.com",
            "time": "1 hr 30 mins"
        },
        {
            "name": "Pork and noodle stir fry",
            "image": "/images/pork-and-noodle-stir-fry.webp",
            "html": "/recipes/pork-and-noodle-stir-fry",
            "category": "Pork",
            "yield": "2",
            "source": "BBC Good Food: Pasta & Noodle Dishes",
            "time": "30 mins"
        },
        {
            "name": "Chicken noodle soup",
            "image": "/images/chicken-noodle-soup.webp",
            "html": "/recipes/chicken-noodle-soup",
            "category": "Poultry",
            "yield": "2",
            "source": "www.bbcgoodfood.com",
            "time": "40 mins"
        },
        {
            "name": "Spaghetti with pea and mint pesto",
            "image": "/images/spaghetti-with-pea-and-mint-pesto.webp",
            "html": "/recipes/spaghetti-with-pea-and-mint-pesto",
            "category": "Veggie",
            "yield": "4",
            "source": "BBC Good Food: Pasta & Noodle Dishes",
            "time": "40 mins"
        },
        {
            "name": "Salmon and broccoli pasta bake",
            "image": "/images/salmon-and-brocolli-pasta-bake.webp",
            "html": "/recipes/salmon-and-brocolli-pasta-bake",
            "category": "Seafood",
            "yield": "4",
            "source": "BBC Good Food: Pasta & Noodle Dishes",
            "time": "1 hr"
        },
        {
            "name": "Fusilli with roasted vegetables",
            "image": "/images/fusilli-with-roasted-vegetables.webp",
            "html": "/recipes/fusilli-with-roasted-vegetables",
            "category": "Veggie",
            "yield": "2",
            "source": "BBC Good Food: Pasta & Noodle Dishes",
            "time": "50 mins"
        },
        {
            "name": "Rigatoni sausage bake",
            "image": "/images/rigatoni-sausage-bake.webp",
            "html": "/recipes/rigatoni-sausage-bake",
            "category": "Pork",
            "yield": "6",
            "source": "BBC Good Food: Pasta & Noodle Dishes",
            "time": "1 hr 15 mins"
        },
        {
            "name": "Spring vegetable tagliatelle with lemon sauce",
            "image": "/images/spring-vegetable-tagliatelle-with-lemon-sauce.webp",
            "html": "/recipes/spring-vegetable-tagliatelle-with-lemon-sauce",
            "category": "Veggie",
            "yield": "4",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "30 mins"
        },
        {
            "name": "Chilli con carne",
            "image": "/images/chilli-con-carne.webp",
            "html": "/recipes/chilli-con-carne",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "1 hr",
            "cuisine": "Mexican",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Chinese chicken with pineapple",
            "image": "/images/chinese-chicken-with-pineapple.webp",
            "html": "/recipes/chinese-chicken-with-pineapple",
            "category": "Poultry",
            "yield": "4",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "25 mins",
            "cuisine": "Chinese"
        },
        {
            "name": "Honey-mustard sausage, bacon and sweet potato bake",
            "image": "/images/honey-mustard-sausage-bacon-and-sweet-potato-bake.webp",
            "html": "/recipes/honey-mustard-sausage-bacon-and-sweet-potato-bake",
            "category": "Pork",
            "yield": "4",
            "source": "Tesco Magazine",
            "time": "45 mins"
        },
        {
            "name": "Prawn & salmon burgers with spicy mayo",
            "image": "/images/prawn-salmon-burgers-with-spicy-mayo.webp",
            "html": "/recipes/prawn-salmon-burgers-with-spicy-mayo",
            "category": "Seafood",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "25 mins"
        },
        {
            "name": "Root vegetable Thai curry",
            "image": "/images/root-vegetable-thai-curry.webp",
            "html": "/recipes/root-vegetable-thai-curry",
            "category": "Veggie",
            "yield": "4",
            "source": "www.bbc.co.uk",
            "time": "1 hr 30 mins",
            "cuisine": "Thai"
        },
        {
            "name": "Baked lamb, vegetables and rice",
            "image": "/images/baked-lamb-vegetables-and-rice.webp",
            "html": "/recipes/baked-lamb-vegetables-and-rice",
            "category": "RedMeat",
            "yield": "4",
            "source": "European Peasant Cookery",
            "time": "1 hr 55 mins",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Kidney bean curry",
            "image": "/images/kidney-bean-curry.webp",
            "html": "/recipes/kidney-bean-curry",
            "category": "Veggie",
            "yield": "2",
            "source": "www.bbcgoodfood.com",
            "time": "35 mins",
            "diet": [
                "Vegan",
                "GlutenFree"
            ],
            "cuisine": "British"
        },
        {
            "name": "Jerk pork & pineapple skewers with black beans & rice",
            "image": "/images/jerk-pork-pineapple-skewers-with-black-beans-rice.webp",
            "html": "/recipes/jerk-pork-pineapple-skewers-with-black-beans-rice",
            "category": "Pork",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "20 mins",
            "cuisine": "Creole"
        },
        {
            "name": "Curried chicken & new potato traybake",
            "image": "/images/curried-chicken-new-potato-traybake.webp",
            "html": "/recipes/curried-chicken-new-potato-traybake",
            "category": "Poultry",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "1 hr",
            "diet": [
                "GlutenFree"
            ]
        },
        {
            "name": "Apple & mustard pork burgers",
            "image": "/images/apple-mustard-pork-burgers.webp",
            "html": "/recipes/apple-mustard-pork-burgers",
            "category": "Pork",
            "yield": "2",
            "source": "www.bbcgoodfood.com",
            "time": "25 mins"
        },
        {
            "name": "Lentil ragu",
            "image": "/images/lentil-ragu.webp",
            "html": "/recipes/lentil-ragu",
            "category": "Veggie",
            "yield": "6",
            "source": "www.bbcgoodfood.com",
            "time": "1 hr 30 mins",
            "cuisine": "Italian"
        },
        {
            "name": "Rosemary chicken with tomato sauce",
            "image": "/images/rosemary-chicken-with-tomato-sauce.webp",
            "html": "/recipes/rosemary-chicken-with-tomato-sauce",
            "category": "Poultry",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "35 mins",
            "cuisine": "Italian"
        },
        {
            "name": "New potato & mince curry",
            "image": "/images/new-potato-mince-curry.webp",
            "html": "/recipes/new-potato-mince-curry",
            "category": "Poultry",
            "yield": "4",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "1 hr"
        },
        {
            "name": "Caribbean fish pie",
            "image": "/images/caribbean-fish-pie.webp",
            "html": "/recipes/caribbean-fish-pie",
            "category": "Seafood",
            "yield": "4-5",
            "source": "Caribbean Food Made Easy",
            "time": "55 mins",
            "cuisine": "Caribbean",
            "tags": [
                "pie"
            ]
        },
        {
            "name": "Butternut & bacon fusilli",
            "image": "/images/butternut-bacon-fusilli.webp",
            "html": "/recipes/butternut-bacon-fusilli",
            "category": "Pork",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "1 hr",
            "cuisine": "Italian"
        },
        {
            "name": "Chicken & broccoli noodles",
            "image": "/images/chicken-broccoli-noodles.webp",
            "html": "/recipes/chicken-broccoli-noodles",
            "category": "Pork",
            "yield": "4",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "20 mins"
        },
        {
            "name": "Piri-piri chicken with spicy rice",
            "image": "/images/piri-piri-chicken-with-spicy-rice.webp",
            "html": "/recipes/piri-piri-chicken-with-spicy-rice",
            "category": "Poultry",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "1 hr 5 mins",
            "cuisine": "Portuguese"
        },
        {
            "name": "Chestnut mushroom, fennel & bacon fusilli",
            "image": "/images/chestnut-mushroom-fennel-bacon-fusilli.webp",
            "html": "/recipes/chestnut-mushroom-fennel-bacon-fusilli",
            "category": "Pork",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "45 mins"
        },
        {
            "name": "Cajun-spiced chicken",
            "image": "/images/cajun-spiced-chicken.webp",
            "html": "/recipes/cajun-spiced-chicken",
            "category": "Poultry",
            "yield": "4",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "20 mins",
            "cuisine": "Cajun"
        },
        {
            "name": "Pasta with chilli tomatoes & spinach",
            "image": "/images/pasta-with-chilli-tomatoes-spinach.webp",
            "html": "/recipes/pasta-with-chilli-tomatoes-spinach",
            "category": "Veggie",
            "yield": "2",
            "source": "www.bbcgoodfood.com",
            "time": "30 mins",
            "cuisine": "Italian"
        },
        {
            "name": "Baked vegetable biryani",
            "image": "/images/baked-vegetable-biryani.webp",
            "html": "/recipes/baked-vegetable-biryani",
            "category": "Veggie",
            "yield": "4",
            "source": "Slimming World magazine",
            "time": "1 hr 10 mins"
        },
        {
            "name": "Becker BBQ shrimp",
            "image": "/images/becker-bbq-shrimp.webp",
            "html": "/recipes/becker-bbq-shrimp",
            "category": "Seafood",
            "yield": "3",
            "source": "www.budgetbytes.com",
            "time": "50 mins"
        },
        {
            "name": "Lamb carry",
            "image": "/images/lamb-carry.webp",
            "html": "/recipes/lamb-carry",
            "category": "RedMeat",
            "yield": "4",
            "source": "Caribbean Food Made Easy",
            "time": "3 hr",
            "cuisine": "Caribbean",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Summer vegetable paella",
            "image": "/images/summer-vegetable-paella.webp",
            "html": "/recipes/summer-vegetable-paella",
            "category": "Veggie",
            "yield": "4",
            "source": "joanne-eatswellwithothers.com",
            "time": "45 mins",
            "cuisine": "Spanish"
        },
        {
            "name": "Very veggie lentil bake",
            "image": "/images/very-veggie-lentil-bake.webp",
            "html": "/recipes/very-veggie-lentil-bake",
            "category": "Veggie",
            "yield": "4",
            "source": "www.amuse-your-bouche.com",
            "time": "50 mins",
            "cuisine": "British"
        },
        {
            "name": "Teriyaki & ginger beef burgers",
            "image": "/images/teriyaki-ginger-beef-burgers.webp",
            "html": "/recipes/teriyaki-ginger-beef-burgers",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.mobkitchen.co.uk",
            "time": "30 mins",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Aubergine goulash",
            "image": "/images/aubergine-goulash.webp",
            "html": "/recipes/aubergine-goulash",
            "category": "Veggie",
            "yield": "4",
            "source": "www.amuse-your-bouche.com",
            "time": "1 hr 5 mins",
            "cuisine": "Hungarian"
        },
        {
            "name": "Pork & chorizo enchiladas",
            "image": "/images/pork-chorizo-enchiladas.webp",
            "html": "/recipes/pork-chorizo-enchiladas",
            "category": "Pork",
            "yield": "8",
            "source": "www.bbcgoodfood.com",
            "time": "1 hr 40 mins",
            "cuisine": "Mexican"
        },
        {
            "name": "Vegetarian casserole",
            "image": "/images/vegetarian-casserole.webp",
            "html": "/recipes/vegetarian-casserole",
            "category": "Veggie",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "50 mins",
            "diet": [
                "Vegan"
            ],
            "cuisine": "Mediterranean"
        },
        {
            "name": "Speedy Moroccan meatballs",
            "image": "/images/speedy-moroccan-meatballs.webp",
            "html": "/recipes/speedy-moroccan-meatballs",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "20 mins",
            "cuisine": "Moroccan"
        },
        {
            "name": "Amatriciana chicken traybake",
            "image": "/images/amatriciana-chicken-traybake.webp",
            "html": "/recipes/amatriciana-chicken-traybake",
            "category": "Poultry",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "1 hr 15 mins",
            "diet": [
                "GlutenFree"
            ],
            "cuisine": "Italian"
        },
        {
            "name": "Veggie shepherd's pie with sweet potato mash",
            "image": "/images/veggie-shepherds-pie-with-sweet-potato-mash.webp",
            "html": "/recipes/veggie-shepherds-pie-with-sweet-potato-mash",
            "category": "Veggie",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "1 hr 10 mins",
            "cuisine": "British",
            "tags": [
                "pie"
            ]
        },
        {
            "name": "Butternut squash & sage risotto",
            "image": "/images/butternut-squash-sage-risotto.webp",
            "html": "/recipes/butternut-squash-sage-risotto",
            "category": "Veggie",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "50 mins",
            "cuisine": "Italian"
        },
        {
            "name": "Baked sweet potatoes with steak fajita filling",
            "image": "/images/baked-sweet-potatoes-with-steak-fajita-filling.webp",
            "html": "/recipes/baked-sweet-potatoes-with-steak-fajita-filling",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "1 hr 10 mins",
            "diet": [
                "GlutenFree"
            ],
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Creamy egg curry",
            "image": "/images/creamy-egg-curry.webp",
            "html": "/recipes/creamy-egg-curry",
            "category": "Veggie",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "30 mins",
            "cuisine": "Indian"
        },
        {
            "name": "Coconut & squash dhansak",
            "image": "/images/coconut-squash-dhansak.webp",
            "html": "/recipes/coconut-squash-dhansak",
            "category": "Veggie",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "20 mins",
            "cuisine": "Indian"
        },
        {
            "name": "Chicken & chorizo rice pot",
            "image": "/images/chicken-chorizo-rice-pot.webp",
            "html": "/recipes/chicken-chorizo-rice-pot",
            "category": "Poultry",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "1 hr 40 mins",
            "cuisine": "Spanish"
        },
        {
            "name": "Herbed lamb cutlets with roasted vegetables",
            "image": "/images/herbed-lamb-cutlets-with-roasted-vegetables.webp",
            "html": "/recipes/herbed-lamb-cutlets-with-roasted-vegetables",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "1 hr",
            "cuisine": "Mediterranean",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Five-veg lasagne",
            "image": "/images/five-veg-lasagne.webp",
            "html": "/recipes/five-veg-lasagne",
            "category": "Veggie",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "1 hr 5 mins",
            "cuisine": "Italian"
        },
        {
            "name": "Spring vegetable soup with basil pesto",
            "image": "/images/spring-vegetable-soup-with-basil-pesto.webp",
            "html": "/recipes/spring-vegetable-soup-with-basil-pesto",
            "category": "Veggie",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "25 mins",
            "cuisine": "Italian"
        },
        {
            "name": "Samosa pie",
            "image": "/images/samosa-pie.webp",
            "html": "/recipes/samosa-pie",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "35 mins",
            "cuisine": "Indian",
            "tags": [
                "lamb",
                "pie"
            ]
        },
        {
            "name": "Quick seafood paella",
            "image": "/images/quick-seafood-paella.webp",
            "html": "/recipes/quick-seafood-paella",
            "category": "Seafood",
            "yield": "4",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "30 mins",
            "cuisine": "Spanish"
        },
        {
            "name": "Springtime lamb stew",
            "image": "/images/springtime-lamb-stew.webp",
            "html": "/recipes/springtime-lamb-stew",
            "category": "RedMeat",
            "yield": "4",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "1 hr 5 mins",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Fluffy American pancakes",
            "image": "/images/fluffy-american-pancakes.webp",
            "html": "/recipes/fluffy-american-pancakes",
            "category": "Dessert",
            "yield": "4-6",
            "source": "www.bbc.co.uk",
            "time": "40 mins",
            "cuisine": "American"
        },
        {
            "name": "Sweet potato & lentil soup",
            "image": "/images/sweet-potato-lentil-soup.webp",
            "html": "/recipes/sweet-potato-lentil-soup",
            "category": "Veggie",
            "yield": "5",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "40 mins"
        },
        {
            "name": "Spiced lamb pie",
            "image": "/images/spiced-lamb-pie.webp",
            "html": "/recipes/spiced-lamb-pie",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "4 hr 50 mins",
            "tags": [
                "lamb",
                "pie"
            ]
        },
        {
            "name": "Cod with lemon and parsley",
            "image": "/images/cod-with-lemon-and-parsley.webp",
            "html": "/recipes/cod-with-lemon-and-parsley",
            "category": "Seafood",
            "yield": "2",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "20 mins"
        },
        {
            "name": "Superhealthy chicken pie",
            "image": "/images/superhealthy-chicken-pie.webp",
            "html": "/recipes/superhealthy-chicken-pie",
            "category": "Poultry",
            "yield": "4",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "55 mins",
            "tags": [
                "pie"
            ]
        },
        {
            "name": "Creamy tomato & pepper pasta",
            "image": "/images/creamy-tomato-pepper-pasta.webp",
            "html": "/recipes/creamy-tomato-pepper-pasta",
            "category": "Veggie",
            "yield": "4",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "1 hr"
        },
        {
            "name": "Deb Henley's chicken enchiladas verdes",
            "image": "/images/deb-henley-s-chicken-enchiladas-verdes.webp",
            "html": "/recipes/deb-henley-s-chicken-enchiladas-verdes",
            "category": "Poultry",
            "yield": "4-6",
            "source": "Leon Family & Friends",
            "time": "1 hr"
        },
        {
            "name": "Soy salmon with sesame stir fry",
            "image": "/images/soy-salmon-with-sesame-stir-fry.webp",
            "html": "/recipes/soy-salmon-with-sesame-stir-fry",
            "category": "Seafood",
            "yield": "4",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "30 mins"
        },
        {
            "name": "Turkey bolognese",
            "image": "/images/turkey-bolognese.webp",
            "html": "/recipes/turkey-bolognese",
            "category": "Poultry",
            "yield": "4",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "40 mins"
        },
        {
            "name": "Black bean chilli",
            "image": "/images/black-bean-chilli.webp",
            "html": "/recipes/black-bean-chilli",
            "category": "Veggie",
            "yield": "4",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "40 mins"
        },
        {
            "name": "Zesty haddock with crushed potatoes & peas",
            "image": "/images/zesty-haddock-with-crushed-potatoes-peas.webp",
            "html": "/recipes/zesty-haddock-with-crushed-potatoes-peas",
            "category": "Seafood",
            "yield": "4",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "35 mins"
        },
        {
            "name": "Harissa chicken with chickpea salad",
            "image": "/images/harissa-chicken-with-chickpea-salad.webp",
            "html": "/recipes/harissa-chicken-with-chickpea-salad",
            "category": "Poultry",
            "yield": "2",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "30 mins"
        },
        {
            "name": "Roast tomato & pepper gnocchi",
            "image": "/images/roast-tomato-pepper-gnocchi.webp",
            "html": "/recipes/roast-tomato-pepper-gnocchi",
            "category": "Veggie",
            "yield": "4",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "35 mins"
        },
        {
            "name": "Italian vegetable soup",
            "image": "/images/italian-vegetable-soup.webp",
            "html": "/recipes/italian-vegetable-soup",
            "category": "Veggie",
            "yield": "8",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "1 hr",
            "cuisine": "Italian"
        },
        {
            "name": "Beef & salsa burgers",
            "image": "/images/beef-salsa-burgers.webp",
            "html": "/recipes/beef-salsa-burgers",
            "category": "RedMeat",
            "yield": "4",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "20 mins",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Spicy pork & aubergine",
            "image": "/images/spicy-pork-aubergine.webp",
            "html": "/recipes/spicy-pork-aubergine",
            "category": "Pork",
            "yield": "4",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "35 mins"
        },
        {
            "name": "Prawns with tomato & feta",
            "image": "/images/prawns-with-tomato-feta.webp",
            "html": "/recipes/prawns-with-tomato-feta",
            "category": "Seafood",
            "yield": "4",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "30 mins"
        },
        {
            "name": "Smoked haddock fishcakes",
            "image": "/images/smoked-haddock-fishcakes.webp",
            "html": "/recipes/smoked-haddock-fishcakes",
            "category": "Seafood",
            "yield": "3",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "1 hr 10 mins"
        },
        {
            "name": "Sweet & sour turkey",
            "image": "/images/sweet-sour-turkey.webp",
            "html": "/recipes/sweet-sour-turkey",
            "category": "Poultry",
            "yield": "4",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "25 mins"
        },
        {
            "name": "Glazed lemon-pepper chicken",
            "image": "/images/glazed-lemon-pepper-chicken.webp",
            "html": "/recipes/glazed-lemon-pepper-chicken",
            "category": "Poultry",
            "yield": "4",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "45 mins"
        },
        {
            "name": "Quick fish risotto",
            "image": "/images/quick-fish-risotto.webp",
            "html": "/recipes/quick-fish-risotto",
            "category": "Seafood",
            "yield": "4",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "30 mins"
        },
        {
            "name": "Pea & pesto soup with fish finger croutons",
            "image": "/images/pea-pesto-soup-with-fish-finger-croutons.webp",
            "html": "/recipes/pea-pesto-soup-with-fish-finger-croutons",
            "category": "Seafood",
            "yield": "4",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "20 mins"
        },
        {
            "name": "Tuna & tomato rice",
            "image": "/images/tuna-tomato-rice.webp",
            "html": "/recipes/tuna-tomato-rice",
            "category": "Seafood",
            "yield": "4",
            "source": "BBC Good Food: Low-fat Feasts",
            "time": "25 mins"
        },
        {
            "name": "Hearty lamb & barley soup",
            "image": "/images/hearty-lamb-barley-soup.webp",
            "html": "/recipes/hearty-lamb-barley-soup",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "35 mins",
            "diet": [
                "DairyFree"
            ],
            "cuisine": "British",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Spiced bulghar pilaf with fish",
            "image": "/images/spiced-bulghar-pilaf-with-fish.webp",
            "html": "/recipes/spiced-bulghar-pilaf-with-fish",
            "category": "Seafood",
            "yield": "4",
            "source": "BBC Good Food: One-pot Dishes",
            "time": "45 mins"
        },
        {
            "name": "Pearl barley, bacon & leek casserole",
            "image": "/images/pearl-barley-bacon-leek-casserole.webp",
            "html": "/recipes/pearl-barley-bacon-leek-casserole",
            "category": "Pork",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "55 mins"
        },
        {
            "name": "Asian salmon & sweet potato tray bake",
            "image": "/images/asian-salmon-sweet-potato-tray-bake.webp",
            "html": "/recipes/asian-salmon-sweet-potato-tray-bake",
            "category": "Seafood",
            "yield": "3",
            "source": "Sainsbury's",
            "time": "40 mins"
        },
        {
            "name": "Bean & sausage hotpot",
            "image": "/images/bean-sausage-hotpot.webp",
            "html": "/recipes/bean-sausage-hotpot",
            "category": "Pork",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "45 mins",
            "cuisine": "British"
        },
        {
            "name": "Fish pie with saffron mash",
            "image": "/images/fish-pie-with-saffron-mash.webp",
            "html": "/recipes/fish-pie-with-saffron-mash",
            "category": "Seafood",
            "yield": "6",
            "source": "www.bbcgoodfood.com",
            "time": "1 hr 45 mins",
            "cuisine": "British",
            "tags": [
                "pie"
            ]
        },
        {
            "name": "Florentine chicken bake",
            "image": "/images/florentine-chicken-bake.webp",
            "html": "/recipes/florentine-chicken-bake",
            "category": "Poultry",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "25 mins",
            "diet": [
                "GlutenFree"
            ],
            "cuisine": "Italian"
        },
        {
            "name": "Chicken, chickpea & lemon casserole",
            "image": "/images/chicken-chickpea-lemon-casserole.webp",
            "html": "/recipes/chicken-chickpea-lemon-casserole",
            "category": "Poultry",
            "yield": "2",
            "source": "www.bbcgoodfood.com",
            "time": "35 mins",
            "cuisine": "British"
        },
        {
            "name": "Super smoky bacon & tomato spaghetti",
            "image": "/images/super-smoky-bacon-tomato-spaghetti.webp",
            "html": "/recipes/super-smoky-bacon-tomato-spaghetti",
            "category": "Pork",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "25 mins",
            "cuisine": "Italian"
        },
        {
            "name": "Cider pork & bacon pie with cauliflower and potato mash",
            "image": "/images/cider-pork-bacon-pie-with-cauliflower-and-potato-mash.webp",
            "html": "/recipes/cider-pork-bacon-pie-with-cauliflower-and-potato-mash",
            "category": "Pork",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "55 mins",
            "diet": [
                "GlutenFree"
            ],
            "tags": [
                "pie"
            ]
        },
        {
            "name": "Salmon & ginger fish cakes",
            "image": "/images/salmon-ginger-fish-cakes.webp",
            "html": "/recipes/salmon-ginger-fish-cakes",
            "category": "Seafood",
            "yield": "2",
            "source": "www.bbcgoodfood.com",
            "time": "45 mins",
            "cuisine": "Asian"
        },
        {
            "name": "Garlic ginger glazed salmon",
            "image": "/images/garlic-ginger-glazed-salmon.webp",
            "html": "/recipes/garlic-ginger-glazed-salmon",
            "category": "Seafood",
            "yield": "4",
            "source": "letthebakingbeginblog.com",
            "time": "45 mins",
            "cuisine": "American"
        },
        {
            "name": "Chipotle-bean chilli with baked eggs",
            "image": "/images/chipotle-bean-chilli-with-baked-eggs.webp",
            "html": "/recipes/chipotle-bean-chilli-with-baked-eggs",
            "category": "Veggie",
            "yield": "4",
            "source": "BBC Good Food: One-pot Dishes",
            "time": "50 mins"
        },
        {
            "name": "Fragrant courgette & prawn curry",
            "image": "/images/fragrant-courgette-prawn-curry.webp",
            "html": "/recipes/fragrant-courgette-prawn-curry",
            "category": "Seafood",
            "yield": "4",
            "source": "BBC Good Food: One-pot Dishes",
            "time": "35 mins",
            "cuisine": "Indian"
        },
        {
            "name": "Smoky sweet potato & bean cakes with citrus salad",
            "image": "/images/smoky-sweet-potato-bean-cakes-with-citrus-salad.webp",
            "html": "/recipes/smoky-sweet-potato-bean-cakes-with-citrus-salad",
            "category": "Veggie",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "30 mins"
        },
        {
            "name": "Herby lamb burgers with sweet potato fries",
            "image": "/images/herby-lamb-burgers-with-sweet-potato-fries.webp",
            "html": "/recipes/herby-lamb-burgers-with-sweet-potato-fries",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "25 mins",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Chorizo, new potatoes and haddock",
            "image": "/images/chorizo-new-potatoes-and-haddock.webp",
            "html": "/recipes/chorizo-new-potatoes-and-haddock",
            "category": "Seafood",
            "yield": "2",
            "source": "BBC Good Food: One-pot Dishes",
            "time": "30 mins"
        },
        {
            "name": "Beef enchilada with sweetcorn and avacado salsa",
            "image": "/images/beef-enchilada-with-sweetcorn-and-avacado-salsa.webp",
            "html": "/recipes/beef-enchilada-with-sweetcorn-and-avacado-salsa",
            "category": "RedMeat",
            "yield": "4",
            "source": "Nosh for graduates",
            "time": "50 mins",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Spiced cod",
            "image": "/images/spiced-cod.webp",
            "html": "/recipes/spiced-cod",
            "category": "Seafood",
            "yield": "4",
            "source": "Nosh for graduates",
            "time": "25 mins"
        },
        {
            "name": "Thai massaman curry",
            "image": "/images/thai-massaman-curry.webp",
            "html": "/recipes/thai-massaman-curry",
            "category": "RedMeat",
            "yield": "2",
            "source": "Blue Dragon Thai Massaman Curry",
            "time": "35 mins",
            "cuisine": "Thai",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Thai green curry",
            "image": "/images/thai-green-curry.webp",
            "html": "/recipes/thai-green-curry",
            "category": "Poultry",
            "yield": "2",
            "source": "Blue Dragon Thai Green Curry",
            "time": "35 mins",
            "cuisine": "Thai"
        },
        {
            "name": "Bean & pasta meatball stew",
            "image": "/images/bean-pasta-meatball-stew.webp",
            "html": "/recipes/bean-pasta-meatball-stew",
            "category": "Pork",
            "yield": "4",
            "source": "BBC Good Food: One-pot Dishes",
            "time": "55 mins"
        },
        {
            "name": "Jerk sweet potato & black bean curry",
            "image": "/images/jerk-sweet-potato-black-bean-curry.webp",
            "html": "/recipes/jerk-sweet-potato-black-bean-curry",
            "category": "Veggie",
            "yield": "10",
            "source": "www.bbcgoodfood.com",
            "time": "1 hr 35 mins",
            "diet": [
                "Vegan"
            ],
            "cuisine": "Caribbean"
        },
        {
            "name": "Moroccan meatball tagine with lemon & olives",
            "image": "/images/moroccan-meatball-tagine-with-lemon-olives.webp",
            "html": "/recipes/moroccan-meatball-tagine-with-lemon-olives",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "1 hr 5 mins",
            "cuisine": "Moroccan",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Oven-baked risotto",
            "image": "/images/oven-baked-risotto.webp",
            "html": "/recipes/oven-baked-risotto",
            "category": "Pork",
            "yield": "4",
            "source": "BBC Good Food: One-pot Dishes",
            "time": "35 mins"
        },
        {
            "name": "Broccoli lemon chicken",
            "image": "/images/broccoli-lemon-chicken.webp",
            "html": "/recipes/broccoli-lemon-chicken",
            "category": "Poultry",
            "yield": "2",
            "source": "BBC Good Food: One-pot Dishes",
            "time": "25 mins"
        },
        {
            "name": "Prawn pasta",
            "image": "/images/prawn-pasta.webp",
            "html": "/recipes/prawn-pasta",
            "category": "Seafood",
            "yield": "4",
            "source": "Caribbean Food Made Easy",
            "time": "30 mins",
            "cuisine": "Caribbean"
        },
        {
            "name": "One-pan lamb & couscous",
            "image": "/images/one-pan-lamb-couscous.webp",
            "html": "/recipes/one-pan-lamb-couscous",
            "category": "RedMeat",
            "yield": "2",
            "source": "www.bbcgoodfood.com",
            "time": "25 mins",
            "cuisine": "British",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Ratatouille with poached eggs",
            "image": "/images/ratatouille-with-poached-eggs.webp",
            "html": "/recipes/ratatouille-with-poached-eggs",
            "category": "Veggie",
            "yield": "4",
            "source": "BBC Good Food: One-pot Dishes",
            "time": "1 hr 10 mins"
        },
        {
            "name": "5-a-day couscous",
            "image": "/images/5-a-day-couscous.webp",
            "html": "/recipes/5-a-day-couscous",
            "category": "Poultry",
            "yield": "2",
            "source": "www.bbcgoodfood.com",
            "time": "40 mins"
        },
        {
            "name": "Sticky jerk salmon with mango slaw",
            "image": "/images/sticky-jerk-salmon-with-mango-slaw.webp",
            "html": "/recipes/sticky-jerk-salmon-with-mango-slaw",
            "category": "Seafood",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "25 mins",
            "cuisine": "Caribbean"
        },
        {
            "name": "Chicken & spring vegetable stew",
            "image": "/images/chicken-spring-vegetable-stew.webp",
            "html": "/recipes/chicken-spring-vegetable-stew",
            "category": "Poultry",
            "yield": "2",
            "source": "BBC Good Food: One-pot Dishes",
            "time": "30 mins"
        },
        {
            "name": "Oven-baked leek & bacon risotto",
            "image": "/images/oven-baked-leek-bacon-risotto.webp",
            "html": "/recipes/oven-baked-leek-bacon-risotto",
            "category": "Pork",
            "yield": "4",
            "source": "BBC Good Food: One-pot Dishes",
            "time": "40 mins"
        },
        {
            "name": "Moroccan chicken with sweet potato mash",
            "image": "/images/moroccan-chicken-with-sweet-potato-mash.webp",
            "html": "/recipes/moroccan-chicken-with-sweet-potato-mash",
            "category": "Poultry",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "35 mins",
            "cuisine": "Moroccan"
        },
        {
            "name": "Chicken, sweet potato & coconut curry",
            "image": "/images/chicken-sweet-potato-coconut-curry.webp",
            "html": "/recipes/chicken-sweet-potato-coconut-curry",
            "category": "Poultry",
            "yield": "3",
            "source": "www.bbcgoodfood.com",
            "time": "45 mins",
            "cuisine": "Indian"
        },
        {
            "name": "Salted caramel brownies",
            "image": "/images/salted-caramel-brownies.webp",
            "html": "/recipes/salted-caramel-brownies",
            "category": "Dessert",
            "yield": "16",
            "source": "www.bbcgoodfood.com",
            "time": "50 mins",
            "cuisine": "British"
        },
        {
            "name": "Thai green chicken curry",
            "image": "/images/thai-green-chicken-curry.webp",
            "html": "/recipes/thai-green-chicken-curry",
            "category": "Poultry",
            "yield": "8",
            "source": "BBC Good Food: One-pot Dishes",
            "time": "40 mins",
            "cuisine": "Thai"
        },
        {
            "name": "Poached chicken with chilli sauce",
            "image": "/images/poached-chicken-with-chilli-sauce.webp",
            "html": "/recipes/poached-chicken-with-chilli-sauce",
            "category": "Poultry",
            "yield": "2",
            "source": "Nosh for graduates",
            "time": "30 mins"
        },
        {
            "name": "Sausage & prawn jambalaya",
            "image": "/images/sausage-prawn-jambalaya.webp",
            "html": "/recipes/sausage-prawn-jambalaya",
            "category": "Pork",
            "yield": "4",
            "source": "BBC Good Food: One-pot Dishes",
            "time": "50 mins",
            "cuisine": "Creole"
        },
        {
            "name": "Turkish lamb pilau",
            "image": "/images/turkish-lamb-pilau.webp",
            "html": "/recipes/turkish-lamb-pilau",
            "category": "RedMeat",
            "yield": "4",
            "source": "BBC Good Food: One-pot Dishes",
            "time": "40 mins",
            "cuisine": "Turkish",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Chicken, ginger & green bean hotpot",
            "image": "/images/chicken-ginger-green-bean-hotpot.webp",
            "html": "/recipes/chicken-ginger-green-bean-hotpot",
            "category": "Poultry",
            "yield": "2",
            "source": "BBC Good Food: One-pot Dishes",
            "time": "33 mins"
        },
        {
            "name": "Moroccan spiced fish with ginger mash",
            "image": "/images/moroccan-spiced-fish-with-ginger-mash.webp",
            "html": "/recipes/moroccan-spiced-fish-with-ginger-mash",
            "category": "Seafood",
            "yield": "2",
            "source": "www.bbcgoodfood.com",
            "time": "30 mins",
            "diet": [
                "GlutenFree"
            ],
            "cuisine": "Moroccan"
        },
        {
            "name": "Sticky jerk & brown sugar ribs with pineapple rice",
            "image": "/images/sticky-jerk-brown-sugar-ribs-with-pineapple-rice.webp",
            "html": "/recipes/sticky-jerk-brown-sugar-ribs-with-pineapple-rice",
            "category": "Pork",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "2 hr 40 mins",
            "cuisine": "Caribbean"
        },
        {
            "name": "Smoked fish and lime patties",
            "image": "/images/smoked-fish-and-lime-patties.webp",
            "html": "/recipes/smoked-fish-and-lime-patties",
            "category": "Seafood",
            "yield": "2",
            "source": "Caribbean Food Made Easy",
            "time": "1 hr 15 mins",
            "cuisine": "Caribbean"
        },
        {
            "name": "Mango chicken, bean & rice bake",
            "image": "/images/mango-chicken-bean-rice-bake.webp",
            "html": "/recipes/mango-chicken-bean-rice-bake",
            "category": "Poultry",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "1 hr 30 mins",
            "cuisine": "Caribbean"
        },
        {
            "name": "Spicy jerk prawn & mango tacos with coconut dressing",
            "image": "/images/spicy-jerk-prawn-mango-tacos-with-coconut-dressing.webp",
            "html": "/recipes/spicy-jerk-prawn-mango-tacos-with-coconut-dressing",
            "category": "Seafood",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "15 mins",
            "cuisine": "Caribbean"
        },
        {
            "name": "Jerk beefburger with pineapple relish & chips",
            "image": "/images/jerk-beefburger-with-pineapple-relish-chips.webp",
            "html": "/recipes/jerk-beefburger-with-pineapple-relish-chips",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "55 mins",
            "cuisine": "Caribbean",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Seafood pasta",
            "image": "/images/seafood-pasta.webp",
            "html": "/recipes/seafood-pasta",
            "category": "Seafood",
            "yield": "4",
            "source": "BBC Good Food: One-pot Dishes",
            "time": "30 mins"
        },
        {
            "name": "Mediterranean vegetables with lamb",
            "image": "/images/mediterranean-vegetables-with-lamb.webp",
            "html": "/recipes/mediterranean-vegetables-with-lamb",
            "category": "RedMeat",
            "yield": "4",
            "source": "BBC Good Food: One-pot Dishes",
            "time": "45 mins",
            "cuisine": "Mediterranean",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Sizzling summer cod",
            "image": "/images/sizzling-summer-cod.webp",
            "html": "/recipes/sizzling-summer-cod",
            "category": "Seafood",
            "yield": "2",
            "source": "BBC Good Food: One-pot Dishes",
            "time": "30 mins"
        },
        {
            "name": "Chicken with creamy bacon penne",
            "image": "/images/chicken-with-creamy-bacon-penne.webp",
            "html": "/recipes/chicken-with-creamy-bacon-penne",
            "category": "Poultry",
            "yield": "2",
            "source": "BBC Good Food: One-pot Dishes",
            "time": "35 mins"
        },
        {
            "name": "Pork patties with chilli dipping sauce",
            "image": "/images/pork-patties-with-chilli-dipping-sauce.webp",
            "html": "/recipes/pork-patties-with-chilli-dipping-sauce",
            "category": "Pork",
            "yield": "3-4",
            "source": "Nosh for graduates",
            "time": "40 mins"
        },
        {
            "name": "Cajun chicken salad with bacon and potatoes",
            "image": "/images/cajun-chicken-salad-with-bacon-and-potatoes.webp",
            "html": "/recipes/cajun-chicken-salad-with-bacon-and-potatoes",
            "category": "Poultry",
            "yield": "4",
            "source": "Nosh for graduates",
            "time": "55 mins",
            "cuisine": "Cajun"
        },
        {
            "name": "Leek and salmon pie",
            "image": "/images/leek-and-salmon-pie.webp",
            "html": "/recipes/leek-and-salmon-pie",
            "category": "Seafood",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "40 mins",
            "diet": [
                "GlutenFree"
            ],
            "tags": [
                "pie"
            ]
        },
        {
            "name": "Cider sausages with leek and potato mash",
            "image": "/images/cider-sausages-with-leek-and-potato-mash.webp",
            "html": "/recipes/cider-sausages-with-leek-and-potato-mash",
            "category": "Pork",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "35 mins",
            "diet": [
                "GlutenFree"
            ]
        },
        {
            "name": "Cashew nut chicken",
            "image": "/images/cashew-nut-chicken.webp",
            "html": "/recipes/cashew-nut-chicken",
            "category": "Poultry",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "25 mins",
            "diet": [
                "GlutenFree"
            ]
        },
        {
            "name": "Mustard chicken & pancetta gratin",
            "image": "/images/mustard-chicken-pancetta-gratin.webp",
            "html": "/recipes/mustard-chicken-pancetta-gratin",
            "category": "Poultry",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "40 mins",
            "diet": [
                "GlutenFree"
            ],
            "cuisine": "French"
        },
        {
            "name": "Meatball pasta bake",
            "image": "/images/meatball-pasta-bake.webp",
            "html": "/recipes/meatball-pasta-bake",
            "category": "RedMeat",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "1 hr",
            "diet": [
                "GlutenFree"
            ],
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Paprika sausage salad",
            "image": "/images/paprika-sausage-salad.webp",
            "html": "/recipes/paprika-sausage-salad",
            "category": "Pork",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "20 mins",
            "diet": [
                "GlutenFree"
            ]
        },
        {
            "name": "Sausages & mash with cumberland sauce",
            "image": "/images/sausages-mash-with-cumberland-sauce.webp",
            "html": "/recipes/sausages-mash-with-cumberland-sauce",
            "category": "Pork",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "1 hr 5 mins",
            "diet": [
                "GlutenFree"
            ]
        },
        {
            "name": "Quick chicken & red lentil curry",
            "image": "/images/quick-chicken-red-lentil-curry.webp",
            "html": "/recipes/quick-chicken-red-lentil-curry",
            "category": "Poultry",
            "yield": "1",
            "source": "Nosh: Quick & Easy",
            "time": "40 mins",
            "cuisine": "Indian"
        },
        {
            "name": "One pot pancetta risotto with mushrooms and tomatoes",
            "image": "/images/one-pot-pancetta-risotto-with-mushrooms-and-tomatoes.webp",
            "html": "/recipes/one-pot-pancetta-risotto-with-mushrooms-and-tomatoes",
            "category": "Pork",
            "yield": "3",
            "source": "Nosh for graduates",
            "time": "40 mins"
        },
        {
            "name": "Lamb & chilli gratinata",
            "image": "/images/lamb-chilli-gratinata.webp",
            "html": "/recipes/lamb-chilli-gratinata",
            "category": "RedMeat",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "35 mins",
            "diet": [
                "GlutenFree"
            ],
            "cuisine": "Italian",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Smartie oat cookies",
            "image": "/images/smartie-oat-cookies.webp",
            "html": "/recipes/smartie-oat-cookies",
            "category": "Dessert",
            "yield": "24",
            "source": "Nosh: Quick & Easy",
            "time": "30 mins",
            "tags": [
                "cookies"
            ]
        },
        {
            "name": "Minted lamb pie",
            "image": "/images/minted-lamb-pie.webp",
            "html": "/recipes/minted-lamb-pie",
            "category": "RedMeat",
            "yield": "4",
            "source": "www.noshbooks.com",
            "time": "1 hr 5 mins",
            "diet": [
                "GlutenFree"
            ],
            "tags": [
                "lamb",
                "pie"
            ]
        },
        {
            "name": "Spaghetti carbonara",
            "image": "/images/spaghetti-carbonara.webp",
            "html": "/recipes/spaghetti-carbonara",
            "category": "Pork",
            "yield": "3",
            "source": "Nosh for Students Vol 2",
            "time": "20 mins"
        },
        {
            "name": "Traditional pork steaks with honey and mustard sauce",
            "image": "/images/traditional-pork-steaks-with-honey-and-mustard-sauce.webp",
            "html": "/recipes/traditional-pork-steaks-with-honey-and-mustard-sauce",
            "category": "Pork",
            "yield": "2",
            "source": "Nosh for graduates",
            "time": "30 mins"
        },
        {
            "name": "Hoisin chicken noodles with ginger and garlic",
            "image": "/images/hoisin-chicken-noodles-with-ginger-and-garlic.webp",
            "html": "/recipes/hoisin-chicken-noodles-with-ginger-and-garlic",
            "category": "Poultry",
            "yield": "2",
            "source": "Nosh for graduates",
            "time": "15 mins"
        },
        {
            "name": "Seafood and spinach pasta",
            "image": "/images/seafood-and-spinach-pasta.webp",
            "html": "/recipes/seafood-and-spinach-pasta",
            "category": "Seafood",
            "yield": "3",
            "source": "Nosh for graduates",
            "time": "45 mins"
        },
        {
            "name": "Moroccan chicken with green olives and lemon",
            "image": "/images/moroccan-chicken-with-green-olives-and-lemon.webp",
            "html": "/recipes/moroccan-chicken-with-green-olives-and-lemon",
            "category": "Poultry",
            "yield": "2",
            "source": "Nosh for graduates",
            "time": "55 mins",
            "cuisine": "Moroccan"
        },
        {
            "name": "Sweet and sour fish with coconut rice and mangetout",
            "image": "/images/sweet-and-sour-fish-with-coconut-rice-and-mangetout.webp",
            "html": "/recipes/sweet-and-sour-fish-with-coconut-rice-and-mangetout",
            "category": "Seafood",
            "yield": "2",
            "source": "Nosh for graduates",
            "time": "30 mins"
        },
        {
            "name": "Chicken biryani with naans",
            "image": "/images/chicken-biryani-with-naans.webp",
            "html": "/recipes/chicken-biryani-with-naans",
            "category": "Poultry",
            "yield": "2",
            "source": "Nosh for graduates",
            "time": "45 mins"
        },
        {
            "name": "Sausage and trottole pasta",
            "image": "/images/sausage-and-trottole-pasta.webp",
            "html": "/recipes/sausage-and-trottole-pasta",
            "category": "Pork",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "25 mins",
            "diet": [
                "GlutenFree"
            ]
        },
        {
            "name": "Coconut cod with lemon sauce and broccoli mash",
            "image": "/images/coconut-cod-with-lemon-sauce-and-broccoli-mash.webp",
            "html": "/recipes/coconut-cod-with-lemon-sauce-and-broccoli-mash",
            "category": "Seafood",
            "yield": "4",
            "source": "www.noshbooks.com",
            "time": "30 mins",
            "diet": [
                "GlutenFree"
            ]
        },
        {
            "name": "Saffron ginger chicken with rice and naan bread",
            "image": "/images/saffron-ginger-chicken-with-rice-and-naan-bread.webp",
            "html": "/recipes/saffron-ginger-chicken-with-rice-and-naan-bread",
            "category": "Poultry",
            "yield": "4",
            "source": "Nosh for graduates",
            "time": "1 hr 15 mins",
            "positive": true
        },
        {
            "name": "Chorizo and butter bean stew",
            "image": "/images/chorizo-and-butter-bean-stew.webp",
            "html": "/recipes/chorizo-and-butter-bean-stew",
            "category": "Pork",
            "yield": "2",
            "source": "Nosh for graduates",
            "time": "25 mins"
        },
        {
            "name": "Sweet honey chicken with risotto rice",
            "image": "/images/sweet-honey-chicken-with-risotto-rice.webp",
            "html": "/recipes/sweet-honey-chicken-with-risotto-rice",
            "category": "Poultry",
            "yield": "4",
            "source": "Nosh for graduates",
            "time": "50 mins"
        },
        {
            "name": "Five spice pork with ginger and honey",
            "image": "/images/five-spice-pork-with-ginger-and-honey.webp",
            "html": "/recipes/five-spice-pork-with-ginger-and-honey",
            "category": "Pork",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "20 mins",
            "diet": [
                "GlutenFree"
            ]
        },
        {
            "name": "Italian meatballs",
            "image": "/images/italian-meatballs.webp",
            "html": "/recipes/italian-meatballs",
            "category": "RedMeat",
            "yield": "3",
            "source": "Nosh for graduates",
            "time": "40 mins",
            "cuisine": "Italian",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Light and lemony prawn risotto",
            "image": "/images/light-and-lemony-prawn-risotto.webp",
            "html": "/recipes/light-and-lemony-prawn-risotto",
            "category": "Seafood",
            "yield": "3",
            "source": "Nosh for graduates",
            "time": "35 mins"
        },
        {
            "name": "Thai salmon with coconut rice and green chilli dressing",
            "image": "/images/thai-salmon-with-coconut-rice-and-green-chilli-dressing.webp",
            "html": "/recipes/thai-salmon-with-coconut-rice-and-green-chilli-dressing",
            "category": "Seafood",
            "yield": "2",
            "source": "Nosh for graduates",
            "time": "40 mins",
            "cuisine": "Thai"
        },
        {
            "name": "Five spice beef noodles",
            "image": "/images/five-spice-beef-noodles.webp",
            "html": "/recipes/five-spice-beef-noodles",
            "category": "RedMeat",
            "yield": "2",
            "source": "Nosh for graduates",
            "time": "25 mins",
            "tags": [
                "beef"
            ]
        },
        {
            "name": "Sweet and sour chicken noodles",
            "image": "/images/sweet-and-sour-chicken-noodles.webp",
            "html": "/recipes/sweet-and-sour-chicken-noodles",
            "category": "Poultry",
            "yield": "2",
            "source": "Nosh for graduates",
            "time": "25 mins"
        },
        {
            "name": "Mango chicken salad",
            "image": "/images/mango-chicken-salad.webp",
            "html": "/recipes/mango-chicken-salad",
            "category": "Poultry",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "20 mins",
            "diet": [
                "GlutenFree"
            ]
        },
        {
            "name": "Pan roasted chicken with spicy fried rice",
            "image": "/images/pan-roasted-chicken-with-spicy-fried-rice.webp",
            "html": "/recipes/pan-roasted-chicken-with-spicy-fried-rice",
            "category": "Poultry",
            "yield": "3",
            "source": "Nosh for graduates",
            "time": "40 mins"
        },
        {
            "name": "Tuna noodles with honey and ginger dressing",
            "image": "/images/tuna-noodles-with-honey-and-ginger-dressing.webp",
            "html": "/recipes/tuna-noodles-with-honey-and-ginger-dressing",
            "category": "Seafood",
            "yield": "3",
            "source": "Nosh for graduates",
            "time": "21 mins"
        },
        {
            "name": "Shiitake mushroom risotto",
            "image": "/images/shiitake-mushroom-risotto.webp",
            "html": "/recipes/shiitake-mushroom-risotto",
            "category": "Veggie",
            "yield": "4",
            "source": "Nosh for graduates",
            "time": "35 mins"
        },
        {
            "name": "Mushroom and bacon tagliatelle",
            "image": "/images/mushroom-and-bacon-tagliatelle.webp",
            "html": "/recipes/mushroom-and-bacon-tagliatelle",
            "category": "Pork",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "25 mins",
            "diet": [
                "GlutenFree"
            ]
        },
        {
            "name": "Prawn jambalaya",
            "image": "/images/prawn-jambalaya.webp",
            "html": "/recipes/prawn-jambalaya",
            "category": "Seafood",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "25 mins",
            "diet": [
                "GlutenFree"
            ],
            "cuisine": "Creole"
        },
        {
            "name": "Oriental marinated chicken strips with bistro salad",
            "image": "/images/oriental-marinated-chicken-strips-with-bistro-salad.webp",
            "html": "/recipes/oriental-marinated-chicken-strips-with-bistro-salad",
            "category": "Poultry",
            "yield": "3",
            "source": "Nosh for graduates",
            "time": "20 mins",
            "cuisine": "Asian"
        },
        {
            "name": "Thai prawn curry",
            "image": "/images/thai-prawn-curry.webp",
            "html": "/recipes/thai-prawn-curry",
            "category": "Seafood",
            "yield": "4",
            "source": "Nosh for graduates",
            "time": "20 mins",
            "cuisine": "Thai"
        },
        {
            "name": "Pan fried chicken with lime & coconut sauce",
            "image": "/images/pan-fried-chicken-with-lime-coconut-sauce.webp",
            "html": "/recipes/pan-fried-chicken-with-lime-coconut-sauce",
            "category": "Poultry",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "30 mins"
        },
        {
            "name": "Lamb chops with pancetta salad & yoghurt dressing",
            "image": "/images/lamb-chops-with-pancetta-salad-yoghurt-dressing.webp",
            "html": "/recipes/lamb-chops-with-pancetta-salad-yoghurt-dressing",
            "category": "RedMeat",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "30 mins",
            "diet": [
                "GlutenFree"
            ],
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Chicken in black bean sauce",
            "image": "/images/chicken-in-black-bean-sauce.webp",
            "html": "/recipes/chicken-in-black-bean-sauce",
            "category": "Poultry",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "30 mins",
            "diet": [
                "GlutenFree"
            ]
        },
        {
            "name": "Thai pork curry",
            "image": "/images/thai-pork-curry.webp",
            "html": "/recipes/thai-pork-curry",
            "category": "Pork",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "25 mins",
            "diet": [
                "GlutenFree"
            ],
            "cuisine": "Thai"
        },
        {
            "name": "Paprika salmon with soy &  honey",
            "image": "/images/paprika-salmon-with-soy-honey.webp",
            "html": "/recipes/paprika-salmon-with-soy-honey",
            "category": "Seafood",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "30 mins",
            "diet": [
                "GlutenFree"
            ]
        },
        {
            "name": "Sausage with apple and mustard",
            "image": "/images/sausage-with-apple-and-mustard.webp",
            "html": "/recipes/sausage-with-apple-and-mustard",
            "category": "Pork",
            "yield": "4",
            "source": "Nosh for Students",
            "time": "45 mins"
        },
        {
            "name": "Almonds and apricot chicken tagine",
            "image": "/images/almonds-and-apricot-chicken-tagine.webp",
            "html": "/recipes/almonds-and-apricot-chicken-tagine",
            "category": "Poultry",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "25 mins"
        },
        {
            "name": "Speedy pork bolognese",
            "image": "/images/speedy-pork-bolognese.webp",
            "html": "/recipes/speedy-pork-bolognese",
            "category": "Pork",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "30 mins",
            "diet": [
                "GlutenFree"
            ]
        },
        {
            "name": "Tarragon & chicken rigatoni",
            "image": "/images/tarragon-chicken-rigatoni.webp",
            "html": "/recipes/tarragon-chicken-rigatoni",
            "category": "Poultry",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "30 mins",
            "diet": [
                "GlutenFree"
            ]
        },
        {
            "name": "Lamb kheema with naan bread",
            "image": "/images/lamb-kheema-with-naan-bread.webp",
            "html": "/recipes/lamb-kheema-with-naan-bread",
            "category": "RedMeat",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "25 mins",
            "diet": [
                "GlutenFree"
            ],
            "cuisine": "Indian",
            "tags": [
                "lamb"
            ]
        },
        {
            "name": "Balsamic chicken with creamy mash",
            "image": "/images/balsamic-chicken-with-creamy-mash.webp",
            "html": "/recipes/balsamic-chicken-with-creamy-mash",
            "category": "Poultry",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "40 mins",
            "diet": [
                "GlutenFree"
            ]
        },
        {
            "name": "Olive & tuna pasta",
            "image": "/images/olive-tuna-pasta.webp",
            "html": "/recipes/olive-tuna-pasta",
            "category": "Seafood",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "20 mins",
            "diet": [
                "GlutenFree"
            ]
        },
        {
            "name": "Warm asian salmon salad",
            "image": "/images/warm-asian-salmon-salad.webp",
            "html": "/recipes/warm-asian-salmon-salad",
            "category": "Seafood",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "25 mins",
            "diet": [
                "GlutenFree"
            ]
        },
        {
            "name": "Pasta amatriciana",
            "image": "/images/pasta-amatriciana.webp",
            "html": "/recipes/pasta-amatriciana",
            "category": "Pork",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "20 mins",
            "diet": [
                "GlutenFree"
            ]
        },
        {
            "name": "Spanish prawn risotto",
            "image": "/images/spanish-prawn-risotto.webp",
            "html": "/recipes/spanish-prawn-risotto",
            "category": "Seafood",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "30 mins",
            "diet": [
                "GlutenFree"
            ],
            "cuisine": "Spanish"
        },
        {
            "name": "Chicken and mushrooms curry with coconut milk",
            "image": "/images/chicken-and-mushrooms-curry-with-coconut-milk.webp",
            "html": "/recipes/chicken-and-mushrooms-curry-with-coconut-milk",
            "category": "Poultry",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "35 mins",
            "diet": [
                "GlutenFree"
            ],
            "cuisine": "Indian"
        },
        {
            "name": "Mackerel in curried couscous salad",
            "image": "/images/mackerel-in-curried-couscous-salad.webp",
            "html": "/recipes/mackerel-in-curried-couscous-salad",
            "category": "Seafood",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "20 mins"
        },
        {
            "name": "Penne boscaiola",
            "image": "/images/penne-boscaiola.webp",
            "html": "/recipes/penne-boscaiola",
            "category": "Pork",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "25 mins",
            "diet": [
                "GlutenFree"
            ]
        },
        {
            "name": "Sweet chilli chicken",
            "image": "/images/sweet-chilli-chicken.webp",
            "html": "/recipes/sweet-chilli-chicken",
            "category": "Poultry",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "30 mins",
            "diet": [
                "GlutenFree"
            ]
        },
        {
            "name": "Leek and tarragon rigatoni",
            "image": "/images/leek-and-tarragon-rigatoni.webp",
            "html": "/recipes/leek-and-tarragon-rigatoni",
            "category": "Veggie",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "20 mins",
            "diet": [
                "GlutenFree"
            ]
        },
        {
            "name": "Prawn &  spinach risotto",
            "image": "/images/prawn-spinach-risotto.webp",
            "html": "/recipes/prawn-spinach-risotto",
            "category": "Seafood",
            "yield": "3",
            "source": "Nosh: Quick & Easy",
            "time": "30 mins",
            "diet": [
                "GlutenFree"
            ]
        },
        {
            "name": "Prawn & chorizo pasta",
            "image": "/images/prawn-chorizo-pasta.webp",
            "html": "/recipes/prawn-chorizo-pasta",
            "category": "Seafood",
            "yield": "3",
            "source": "Nosh: Quick & Easy",
            "time": "20 mins",
            "diet": [
                "GlutenFree"
            ]
        },
        {
            "name": "Paella",
            "image": "/images/paella.webp",
            "html": "/recipes/paella",
            "category": "Seafood",
            "yield": "4",
            "source": "www.bbcgoodfood.com",
            "time": "30 mins",
            "cuisine": "Spanish"
        },
        {
            "name": "Italian cod and bacon stew",
            "image": "/images/italian-cod-and-bacon-stew.webp",
            "html": "/recipes/italian-cod-and-bacon-stew",
            "category": "Seafood",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "20 mins",
            "diet": [
                "GlutenFree"
            ],
            "cuisine": "Italian"
        },
        {
            "name": "Sweet potato, bacon & orzo pasta salad",
            "image": "/images/sweet-potato-bacon-orzo-pasta-salad.webp",
            "html": "/recipes/sweet-potato-bacon-orzo-pasta-salad",
            "category": "Pork",
            "yield": "4",
            "source": "Nosh: Quick & Easy",
            "time": "40 mins",
            "positive": true
        },
        {
            "name": "Cinnamon pull-apart bread",
            "image": "/images/cinnamon-pull-apart-bread.webp",
            "html": "/recipes/cinnamon-pull-apart-bread",
            "category": "Dessert",
            "yield": "12",
            "source": "thepioneerwoman.com",
            "time": "55 mins",
            "tags": [
                "bread"
            ]
        },
        {
            "name": "Millionaire shortbread",
            "image": "/images/millionaire-shortbread.webp",
            "html": "/recipes/millionaire-shortbread",
            "category": "Dessert",
            "yield": "12",
            "source": "James' nan",
            "time": "55 mins"
        }
    ]
}