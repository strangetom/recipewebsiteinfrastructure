#! /usr/bin/env python3

import argparse
import os
import subprocess
import sys

from builder import RecipeWebsite


def minify(filename: str) -> None:
    """Minify files using esbuild
    Minified files have .min inserted before file extension

    Parameters
    ----------
    filename : str
        Path to javascript or css file
    """
    minified_filename = filename.split(".")[0] + ".min." + filename.split(".")[1]
    minified_filename = minified_filename.replace("static", "website").replace(
        ".ts", ".js"
    )
    subprocess.run(
        [
            "/usr/bin/esbuild",
            filename,
            "--minify",
            f"--outfile={minified_filename}",
            "--log-level=silent",
        ],
        stdout=subprocess.DEVNULL,
        stderr=subprocess.STDOUT,
    )


def copy_dir(src: str, dst: str) -> None:
    """Mirror a directory contents from source to destination
    Deletions in the source directory are mirrored in destination

    Parameters
    ----------
    src : str
        Source directory
    dst : str
        Destination directory
    """
    subprocess.Popen(["/usr/bin/rsync", "-r", "--delete", "-c", src, dst])


if __name__ == "__main__":
    argparser = argparse.ArgumentParser(description="Build recipe site")
    argparser.add_argument(
        "--skip-uncooked",
        action="store_true",
        help="Skip recipes without photos when generating website",
    )
    args = argparser.parse_args()

    # If website/ folder and subfolders don't exist, create them
    if not os.path.isdir("website"):
        os.mkdir("website")
    if not os.path.isdir("website/css"):
        os.mkdir("website/css")
    if not os.path.isdir("website/js"):
        os.mkdir("website/js")
    if not os.path.isdir("website/feed"):
        os.mkdir("website/feed")
    if not os.path.isdir("website/recipes"):
        os.mkdir("website/recipes")

    # Copying images
    print("Copying images...")
    copy_dir("../Recipes/assets/images/", "./website/images")
    copy_dir("./static/img/", "./website/img")
    copy_dir("./static/icons/", "./website/icons")
    copy_dir("./static/fonts/", "./website/fonts")
    copy_dir("../Recipes/assets/covers/", "./website/covers")

    # Build html, search_data and feeds
    site = RecipeWebsite()
    site.build_site(skip_uncooked=args.skip_uncooked)

    # Minify css using csso
    print("Minifying css...")
    minify("static/css/colours.css")
    minify("static/css/recipe.css")
    minify("static/css/index.css")
    minify("static/css/offline.css")
    minify("static/css/all.css")
    minify("static/css/latest.css")
    copy_dir("./static/css/atom.xsl", "./website/css/")

    # Minify js using uglifyjs
    print("Minifying js...")
    minify("static/js/search.ts")
    minify("static/js/search_data.js")
    minify("static/js/scale_timers.ts")
    minify("static/service-worker.js")

    print("Finished")
    sys.exit(os.EX_OK)
