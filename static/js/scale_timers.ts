"use strict";

// https://qaz.wtf/u/fraction.cgi?text=1%2F14
const fractionMap = {
  "0": "",
  "0.5": "½",
  "0.333": "⅓",
  "0.666": "⅔",
  "0.25": "¼",
  "0.75": "¾",
  "0.2": "⅕",
  "0.4": "⅖",
  "0.6": "⅗",
  "0.8": "⅘",
  "0.1665": "⅙",
  "0.833": "⅚",
  "0.14285": "⅐",
  "0.125": "⅛",
  "0.375": "⅜",
  "0.625": "⅝",
  "0.875": "⅞",
};

const convertableUnits = {
  metric: {
    oz: [28, "g"],
    ounce: [28, "g"],
    ounces: [28, "g"],
    lb: [454, "g"],
    lbs: [454, "g"],
    pound: [454, "g"],
    pounds: [454, "g"],
    cup: [250, "ml"],
    cups: [250, "ml"],
    pint: [568, "ml"],
    pints: [568, "ml"],
    quart: [950, "nl"],
    quarts: [950, "nl"],
  },
  imperial: {
    g: [0.035, "oz"],
    kg: [2.2, "lbs"],
    ml: [0.00423, "cups"],
    l: [4.423, "cups"],
  },
};

// Create a reference for the wake lock so we can release it later
var wakelock = null;

// Create a class for the TimerToast element, based on div element
class TimerToast extends HTMLDivElement {
  // Store current value of timer in seconds
  counter: number;
  // Store intial value for resetting
  initial: number;
  // Span to display time
  time: HTMLSpanElement = document.createElement("span");
  // Div to show progress bar
  progress: HTMLDivElement = document.createElement("div");
  // Interval id for when timer is running
  interval: number;
  // Store notification status and create unique ID
  notificationEnabled: boolean = false;
  notificationId: string = Date.now().toString();
  /**
   * TimerToast custom element
   * @param {number} period time period in seconds
   */
  constructor(period: number, label: string) {
    // Always call super first in constructor
    // Return value from super() is a reference to this element
    super();

    this.counter = period;
    this.initial = period;
    this.displayTime();

    // Add class to span dislaying time
    this.time.classList.add("timer-text");

    // Add class to div showing progress and set width
    this.progress.classList.add("timer-progress");
    this.progress.style.width = "0%";

    // Create Div to hold toast buttons
    const controls = document.createElement("div");
    controls.classList.add("timer-btns");

    // If notifications are supported and not already denied, add notification icon and add event listeners
    if (window.Notification && Notification.permission !== "denied") {
      const notificationIcon = document.createElement("img");
      notificationIcon.src = "../img/notification-disabled.svg";
      notificationIcon.classList.add("timer-icon");
      notificationIcon.alt = "Toggle timer notification";
      const notificationBtn = document.createElement("button");
      notificationBtn.classList.add("timer-btn");
      notificationBtn.addEventListener("click", () => {
        if (this.notificationEnabled) {
          notificationIcon.src = "../img/notification-disabled.svg";
          this.notificationEnabled = false;
        } else {
          // Ask for notification permission if we don't already have it
          if (Notification.permission === "default") {
            Notification.requestPermission().then((permission) => {
              if (permission === "granted") {
                notificationIcon.src = "../img/notification.svg";
                this.notificationEnabled = true;
              }
            });
          } else {
            notificationIcon.src = "../img/notification.svg";
            this.notificationEnabled = true;
          }
        }
      });
      notificationBtn.appendChild(notificationIcon);
      controls.appendChild(notificationBtn);
    }

    // Create increment button and add event listeners
    const incIcon = document.createElement("img");
    incIcon.src = "../img/timer-increment.svg";
    incIcon.classList.add("timer-icon");
    incIcon.alt = "Add 1 minute to timer";
    const incBtn = document.createElement("button");
    incBtn.classList.add("timer-btn");
    incBtn.addEventListener("click", () => {
      this.increment();
    });
    incBtn.appendChild(incIcon);
    controls.appendChild(incBtn);

    // Create start icon and add event liseners
    const startIcon = document.createElement("img");
    startIcon.src = "../img/timer-start.svg";
    startIcon.classList.add("timer-icon");
    startIcon.alt = "Start timer";
    const startBtn = document.createElement("button");
    startBtn.classList.add("timer-btn");
    startBtn.addEventListener("click", () => {
      // If no timer (i.e. interval = null), create timer and set icon to reset
      // Else, reset timer and change icon to start
      if (this.interval == null) {
        this.interval = setInterval(this.decrement.bind(this), 1000);
        startIcon.src = "../img/timer-reset.svg";
      } else {
        this.reset();
        startIcon.src = "../img/timer-start.svg";
      }
    });
    startBtn.appendChild(startIcon);
    controls.appendChild(startBtn);

    // Create close button and add event listeners
    const closeIcon = document.createElement("img");
    closeIcon.src = "../img/timer-close.svg";
    closeIcon.classList.add("timer-icon");
    closeIcon.alt = "Delete timer";
    const closeBtn = document.createElement("button");
    closeBtn.classList.add("timer-btn");
    closeBtn.addEventListener("click", () => {
      // Remove notification if exists
      this.closeNotification();
      // Add toast-delete class to animate delete
      this.classList.add("toast-delete");
      // When animation ends, remove element from dom
      this.addEventListener("animationend", () => {
        this.parentNode.removeChild(this);
      });
    });
    closeBtn.appendChild(closeIcon);
    controls.appendChild(closeBtn);

    const timeDiv = document.createElement("div");
    this.appendChild(timeDiv);
    timeDiv.appendChild(this.time);

    // Create label if there is one
    if (label != "") {
      // Span to display label
      let labelSpan: HTMLSpanElement = document.createElement("span");
      // Strip of '@' from beginning
      labelSpan.innerText = label.replace("@", "");
      labelSpan.classList.add("timer-label");
      timeDiv.appendChild(labelSpan);
    }

    this.appendChild(this.progress);
    this.appendChild(controls);
    this.classList.add("timer");
  }

  /**
   * Display time on toast
   */
  displayTime() {
    let count = Math.abs(this.counter);
    let hours = Math.floor(count / 3600);
    let minutes = Math.floor((count % 3600) / 60);
    let seconds = Math.floor(count % 60);
    let displayHours = hours < 10 ? "0" + hours : hours.toString();
    let displayMinutes = minutes < 10 ? "0" + minutes : minutes.toString();
    let displaySeconds = seconds < 10 ? "0" + seconds : seconds.toString();
    this.time.textContent =
      displayHours + ":" + displayMinutes + ":" + displaySeconds;
    if (this.counter <= 0) {
      this.time.textContent = "-" + this.time.textContent;
    }
  }
  /**
   * Increment timer by 1 minute
   */
  increment() {
    this.counter += 60;
    this.initial += 60;
    this.displayTime();
  }
  /**
   * Decrement counter by 1
   */
  decrement() {
    this.counter--;
    this.displayTime();
    if (this.counter > 0) {
      // Calculate progress as a percent, then update progress bar by setting width
      this.progress.style.width =
        ((100 * (this.initial - this.counter)) / this.initial).toFixed(2) + "%";
    } else if (this.counter == 0) {
      // Set progress width to 0 and add timer-expired class
      this.progress.style.width = "100%";
      this.time.classList.add("timer-expired");
      // Create notification if enabled
      if (this.notificationEnabled && Notification.permission === "granted") {
        navigator.serviceWorker.ready.then((sw) => {
          let badge = "/favicon.svg";
          let icon = "/img/logo.svg";
          let text = "Timer expired!";
          let data = {
            url: window.location.href,
          };
          let options = {
            body: text,
            icon: icon,
            badge: badge,
            data: data,
            tag: this.notificationId,
          };
          sw.showNotification("Stranger Foods", options);
        });
      }
    }
  }
  /**
   * Reset timer to initial state
   * Remove notification if there is one
   */
  reset() {
    if (this.interval != null) {
      clearInterval(this.interval);
      this.interval = null;
    }
    this.closeNotification();
    this.time.classList.remove("timer-expired");
    this.counter = this.initial;
    this.progress.style.width = "0%";
    this.displayTime();
  }

  /**
   * Close notification created by this TimerToast
   */
  closeNotification() {
    navigator.serviceWorker.ready.then((sw) => {
      sw.getNotifications({ tag: this.notificationId }).then((notif) => {
        for (const n of notif) {
          n.close();
        }
      });
    });
  }
}

/**
 * On click of one of the scale or units buttons, scale the recipe ingredient quantities and convert units as appropriate
 * @param {Event} event onClick event
 */
function scaleRecipe(event: Event) {
  let el: HTMLButtonElement = <HTMLButtonElement>event.target;
  // Highlight selected button
  highlightButtons(el);

  // Get scale factor and unitsType
  let btn: HTMLInputElement = document.querySelector(
    ".scale-btns input:checked",
  );
  let scaleFactor: number = Number(btn.value.substring(1));
  btn = document.querySelector(".unit-btns input:checked");
  let unitsType: string = btn.value.toLowerCase();

  // Set new servings value
  setServings(scaleFactor);

  // Set new value for all scalable elements
  let SQ: NodeListOf<HTMLSpanElement> = document.querySelectorAll(".scalable"); // scaleable quantities
  for (var i = 0; i < SQ.length; i++) {
    let finalScaling: number = scaleFactor;
    let el: HTMLSpanElement = SQ[i];

    if (el.hasAttribute("data-unit")) {
      // This is a tooltip in the instructions
      // If this item has no quantity, skip it
      if (el.dataset.default == "") {
        continue;
      }

      let unit: string = el.dataset.defaultunit;
      let x: boolean = el.dataset.default.slice(-1) == "x";

      if (Object.keys(convertableUnits[unitsType]).includes(unit)) {
        // We're converting unitsType
        // Update scaleFactor to include units conversion
        let conversionData = convertableUnits[unitsType][unit];
        finalScaling *= conversionData[0];
        // Switch unitsType
        el.dataset.unit = conversionData[1];
      } else if (x) {
        // The unit here may by of the form '400 g tin' or similar. We want to scale the 400 and g parts
        // We only care if we're doing units conversion
        el.dataset.unit = convertMixedUnit(unitsType, unit);
      } else {
        // Reset to default unit
        el.dataset.unit = el.dataset.defaultunit;
      }

      let scaled: string;
      if (el.dataset.default.includes("-")) {
        scaled = scaleRange(el.dataset.default, finalScaling);
      } else if (x) {
        let val: number = Number(el.dataset.default.slice(0, -1));
        scaled = formatQuantity(val * finalScaling);
      } else {
        let val: number = Number(el.dataset.default);
        scaled = formatQuantity(val * finalScaling);
      }

      setScaledValue(el, scaled, x);
    } else {
      //This is an ingredient
      let unitEl: HTMLSpanElement = <HTMLSpanElement>el.nextElementSibling;
      let unit: string = unitEl.dataset.unit;
      if (Object.keys(convertableUnits[unitsType]).includes(unit)) {
        // We're converting unitsType
        // Update scaleFactor to include units conversion
        let conversionData = convertableUnits[unitsType][unit];
        finalScaling *= conversionData[0];
        // Switch unitsType
        unitEl.innerHTML = conversionData[1];
      } else {
        // Attempt to convert mixed unit, otherwise use default unit
        unitEl.innerHTML = convertMixedUnit(unitsType, unit);
      }

      let scaled: string;
      if (el.dataset.default.includes("-")) {
        let defaultVal: string = el.dataset.default;
        scaled = scaleRange(defaultVal, finalScaling);
      } else {
        let defaultVal: number = Number(el.dataset.default);
        scaled = formatQuantity(defaultVal * finalScaling);
      }
      // This will never have an 'x' because it's kept outside the span for the ingredients
      setScaledValue(el, scaled, false);
    }
  }
}

/**
 * Scale recipe servings number
 * @param  {number} SF  Scale factor to scale servings by
 */
function setServings(SF: number) {
  let el: HTMLSpanElement = document.getElementById("servings");
  let initial: string = el.dataset.default;
  if (initial.includes("-")) {
    el.innerHTML = scaleRange(initial, SF);
  } else {
    el.innerHTML = formatQuantity(Number(initial) * SF);
  }
}

/**
 * Highlight clicked scaling and converting button
 * @param  {HTMLButtonElement} el Add 'selected' class to clicked button and remove from other buttons in group
 */
function highlightButtons(el: HTMLButtonElement) {
  let buttonRow: HTMLCollection = el.parentElement.children;
  for (const btn of buttonRow) {
    btn.classList.remove("selected");
  }
  el.classList.add("selected");
}

/**
 * Convert a mixed unit e.g. 400 g can from one unit to another
 * @param  {string} unitsType   metric/imperial
 * @param  {string} unitStr     Mixed unit to be converted
 *
 * @return {string}             unitStr converted to unit set by unitsType
 */
function convertMixedUnit(unitsType: string, unitStr: string): string {
  let possibleUnits: string = Object.keys(convertableUnits[unitsType]).join(
    "|",
  );
  let re = new RegExp(
    "(?<quantity>\\d+)\\s(?<unit>" + possibleUnits + ")(?<extra>.*)",
    "g",
  );
  let out = re.exec(unitStr);
  if (out) {
    // Match, so scale
    let conversionData = convertableUnits[unitsType][out.groups.unit];
    let newUnit =
      formatQuantity(Number(out.groups.quantity) * conversionData[0]) +
      " " +
      conversionData[1] +
      out.groups.extra;
    return newUnit;
  } else {
    return unitStr;
  }
}

/**
 * Set scaled number in dom
 * @param  {HTMLSpanElement} el   Element to set value in
 * @param  {string}  val  Value to set in element
 * @param  {boolean} x    Boolean indicating if 'x' should be appended to value prior to inserting into element
 */
function setScaledValue(el: HTMLSpanElement, val: string, x: boolean) {
  if (x) {
    val += "x";
  }
  if ("quantity" in el.dataset) {
    // Tooltips
    el.dataset.quantity = val;
  } else {
    // Ingredients list
    el.innerHTML = val;
  }
}

/**
 * Scale a range e.g. 1-2 by a scale factor, returning a well formatted range
 * @param  {string} strValue Range to be formatted
 * @param  {number} SF       Scale factor to scale range by
 *
 * @return {string}          Formatted range
 */
function scaleRange(strValue: string, SF: number): string {
  let split = strValue.split("-");
  let lower = Number(split[0]) * SF;
  let upper = Number(split[1]) * SF;
  return formatQuantity(lower) + "-" + formatQuantity(upper);
}

/**
 * Format a quantity, replacing decimals with unicode fractions where suitable
 * @param  {number} value Number to be formatted
 *
 * @return {string}       Formatted number
 */
function formatQuantity(value: number): string {
  // If value is an integer, we don't need to do anything.
  if (Number.isInteger(Number(value))) {
    return value.toString();
  }

  // Split into integer and fractional parts
  let integer: number = Math.floor(value);
  let frac: number = Number(value % 1);

  // Match fractional part to closest unicode fraction
  let fractionValues: number[] = Object.keys(fractionMap).map(Number);
  let fractionMatch: number = fractionValues.reduce(function (prev, curr) {
    return Math.abs(curr - frac) < Math.abs(prev - frac) ? curr : prev;
  });
  let fractionStr: string = fractionMap[fractionMatch];

  if (integer == 0) {
    return fractionStr;
  } else {
    return integer.toString() + fractionStr;
  }
}

/**
 * Add a toast for a timer
 * @param {Event} e Event
 */
function addTimerToast(e: Event) {
  // 'this' is used here to get the element the event listener was original attached to, rather than the element that fired it
  // See https://stackoverflow.com/questions/42644069/addeventlistener-event-on-parent-target-child
  let period: number = Number(this.dataset.time);
  let label: string = this.dataset.label;
  let instruction: HTMLLIElement = (e.target as HTMLButtonElement).closest(
    "li",
  );
  instruction.appendChild(new TimerToast(period, label));
}

/**
 * Toggle wakelock when checkbox state changes
 */
async function toggleWakeLock() {
  if (wakelock == null) {
    try {
      wakelock = await navigator.wakeLock.request("screen");
    } catch (err) {
      console.log(`Wakelock failed: ${err.message}`);
    }
  } else {
    // Release wakelock and set variable back to null
    wakelock.release().then(() => (wakelock = null));
  }
}

/**
 * Enable light theme if set on homepage
 */
function setTheme() {
  let html = document.querySelector("html");
  let lightTheme = localStorage.getItem("light-theme");
  if (
    lightTheme == null &&
    window.matchMedia("(prefers-color-scheme: light)").matches
  ) {
    html.classList.add("light-theme");
  } else if (lightTheme == "true") {
    html.classList.add("light-theme");
  }
}

/**
 * Enable share button on platforms that support it and set share content
 */
function enableShareButton() {
  let title: string = document.querySelector("h1").textContent;
  const shareData = {
    title: title,
    text: `${title} on Stranger Foods`,
    url: location.href,
  };
  if (navigator.share !== undefined) {
    let shareBtn = document.querySelector("#share");
    shareBtn.classList.remove("hidden");
    shareBtn.addEventListener("click", async () => {
      await navigator.share(shareData);
    });
  }
}

/**
 * Change units of temperatures in instructions between Celcius and Farenheit
 * @param {Event} event onClick event
 */
function changeTemperatureUnits(event: Event) {
  let el: HTMLButtonElement = <HTMLButtonElement>event.target;
  // Highlight selected button
  highlightButtons(el);

  // Get scale factor and unitsType
  let btn: HTMLInputElement = document.querySelector(
    ".temp-btns input:checked",
  );
  let unit: string = btn.value;

  let temps: NodeListOf<HTMLSpanElement> =
    document.querySelectorAll(".temperature");
  temps.forEach((el) => {
    let value = Number(el.dataset.value);
    if (unit == "F") {
      // Convert Celcius to Farenheit, rounding to nearest integer
      value = Math.round(value * 1.8 + 32);
    }
    // Set new innerHTML
    el.innerHTML = `<b>${value}</b>&deg;${unit}`;
  });
}

document.addEventListener("DOMContentLoaded", () => {
  // Add event listeners for scaling and units buttons
  var scalebtns: NodeListOf<HTMLButtonElement> =
    document.querySelectorAll(".scale-btns input");
  for (const btn of scalebtns) {
    btn.addEventListener("click", scaleRecipe);
  }
  var unitbtns: NodeListOf<HTMLButtonElement> =
    document.querySelectorAll(".unit-btns input");
  for (const btn of unitbtns) {
    btn.addEventListener("click", scaleRecipe);
  }
  var tempbtns: NodeListOf<HTMLButtonElement> =
    document.querySelectorAll(".temp-btns input");
  for (const btn of tempbtns) {
    btn.addEventListener("click", changeTemperatureUnits);
  }

  // Add event listeners for ingredient subsititions
  var substitions: NodeListOf<HTMLSpanElement> =
    document.querySelectorAll(".substitute-btn");
  for (const el of substitions) {
    el.addEventListener("click", (e) => {
      let subsText = e.target.closest("button").querySelector("span");
      subsText.classList.toggle("hidden");
    });
  }

  // Add event listeners for timers
  customElements.define("timer-toast", TimerToast, { extends: "div" });
  var timers: NodeListOf<HTMLButtonElement> =
    document.querySelectorAll(".toast-btn");
  for (const timer of timers) {
    timer.addEventListener("click", addTimerToast);
  }

  // If Screen Wake Lock API is supported, show checkbox to enable it and add event listener to toggle
  if ("wakeLock" in navigator) {
    let wakelockCheckbox: HTMLInputElement = <HTMLInputElement>(
      document.querySelector("#wakelock")
    );
    // Unhide
    wakelockCheckbox.closest("span").classList.remove("hidden");
    // Add event listener for toggling
    wakelockCheckbox.addEventListener("change", toggleWakeLock);
  }

  // Show button for toggling amounts inline in instructions
  document.querySelector("#show-amounts").classList.remove("hidden");
  let showAmountCheckbox: HTMLInputElement = <HTMLInputElement>(
    document.querySelector("#amounts")
  );
  showAmountCheckbox.addEventListener("change", () => {
    let tooltips: NodeListOf<HTMLSpanElement> =
      document.querySelectorAll(".tooltip");
    tooltips.forEach((el) => {
      el.classList.toggle("visible");
    });
  });

  // Set light theme if selected on homepage
  setTheme();

  // Enable share button on platforms that support it
  enableShareButton();
});
