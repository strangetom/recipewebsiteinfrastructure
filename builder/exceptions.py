class InvalidTimerError(Exception):
    pass


class InvalidTemperatureError(Exception):
    pass


class InvalidBoldNumberError(Exception):
    pass
