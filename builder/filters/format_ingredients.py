#! /usr/bin/env python3

import re
import uuid
from html import unescape
from itertools import groupby
from typing import Any

import lxml.etree as ET


def create_quantity_element(quantity: str, formatted_quantity: str) -> ET.Element:
    """Create element for ingredient quantity

    Parameters
    ----------
    quantity : str
        Ingredient quantity string
    formatted_quantity : str
        Formatted ingredient quantity string

    Returns
    -------
    ET.Element
        Element for ingredient quantity
    """
    # If no quantity, return empty span
    if quantity == "" and formatted_quantity == "":
        return ET.Element("span")

    # Flag to indicate is quantity ends with 'x'
    ends_x = quantity[-1] == "x"

    if ends_x:
        quantity = quantity[:-1]
        formatted_quantity = formatted_quantity[:-1]

    quantity_span = ET.Element(
        "span", attrib={"data-default": quantity, "class": "scalable"}
    )
    quantity_span.text = unescape(formatted_quantity)

    if ends_x:
        quantity_span.tail = "x "

    return quantity_span


def create_unit_element(unit: str) -> ET.Element:
    """Create element for ingredient unit

    Parameters
    ----------
    unit : str
        Ingredient unit

    Returns
    -------
    ET.Element
        Element for ingredient unit
    """
    # If no unit, return empty span
    if unit == "":
        return ET.Element("span", attrib={"data-unit": ""})

    unit_span = ET.Element("span", attrib={"data-unit": unit})
    unit_span.text = unit

    return unit_span


def list_element(ingredient: dict[str, str]) -> ET.Element:
    """Return html for ingredient as a list element

    The list element contains the following children:
    - <span> containing quantity, can be empty
    - <span> containing units, can be emptpy
    - <span> containing name, with comment append to tail
    Optional:
    - <span> containing ingredient subtitutions

    Parameters
    ----------
    ingredient : Dict[str, Any]
        ingredient dictionary containing fields: quantity, formatted_quantity,
        unit, additional, ingredients, synonym
        Optional fields: substitute, link

    Returns
    -------
    ET.Element
        html list element for ingredient

    """
    li = ET.Element("li", attrib={"itemprop": "recipeIngredient"})

    # Create elements for quantity and unit and add to li element
    quantity = create_quantity_element(
        ingredient["quantity"], ingredient["formatted_quantity"]
    )
    unit = create_unit_element(ingredient["unit"])
    li.append(quantity)
    li.append(unit)

    # Create element for name, which may be a link, and add to li element
    if "link" in ingredient.keys():
        name = ET.Element("a")
        name.attrib["href"] = f"/recipes/{ingredient['link']}"
    else:
        name = ET.Element("span")

    name.text = unescape(ingredient["name"])
    li.append(name)

    if ingredient["prep"] != "":
        name.tail = ", "
        prep = ET.Element("span", attrib={"class": "prep"})
        prep.text = unescape(ingredient["prep"])
        li.append(prep)

    if ingredient["comment"] != "":
        last_el = li[-1]
        if ingredient["comment"].startswith("("):
            last_el.tail = unescape(f" {ingredient['comment']}")
        else:
            last_el.tail = unescape(f", {ingredient['comment']}")

    # If ingredient has substitutes, append to li element
    if "substitute" in ingredient.keys():
        """
        Create the following element structure and append it to the list element
        <span class="substitute">
            <img src="../img/cart.svg">
            <small class="substitute-text">
                Substitute with: substitute 1 / substitute 2
            </small>
        </span>

        """
        button = ET.Element("button", attrib={"class": "substitute-btn"})
        div = ET.Element("div", attrib={"class": "substitute-btn-icon"})

        svg = ET.Element(
            "svg",
            attrib={
                "xmlns": "http://www.w3.org/2000/svg",
                "width": "16",
                "height": "16",
                "fill": "none",
                "stroke-width": "1.5",
                "stroke": "currentColor",
                "viewBox": "0 0 24 24",
            },
        )
        path = ET.Element(
            "path",
            attrib={
                "stroke-linecap": "round",
                "stroke-linejoin": "round",
                "d": "M7.5 21 3 16.5m0 0L7.5 12M3 16.5h13.5m0-13.5L21 7.5m0 0L16.5 12M21 7.5H7.5",
            },
        )
        svg.append(path)
        div.append(svg)
        button.append(div)

        span = ET.Element("span", attrib={"class": "substitute-text hidden"})
        span.text = "or " + " / ".join(ingredient["substitute"])
        button.append(span)
        li.append(button)

    return li


def format_ingredients(
    ingredients: list[dict[str, Any]], ingredient_headings: dict[str, str]
) -> str:
    """Jinja template function to html for ingrediensts list.

    Ingredients are stored as a list.
    Each list element is either a dict, which represents a single ingredient, or
    a list of dicts, which represent a group of ingredients with a title.

    Groups of ingredients get wrapped in a <details> tag so they can be collapsed.

    Parameters
    ----------
    ingredients : list[dict[str, Any]]
        List of ingredients dicts
    inigredient_headings: dict[int, str]
        Dict of ingredient headings for group of ingredients.
        The key is the group ID (integer greater than 0).

    Returns
    -------
    str
        html markup for ingredients list

    """
    # Create a div as the root element.
    div = ET.Element("div")

    # Group ingredients into lists of either a single ingredient group or a list of
    # single ingredient items
    # If the element is a group, return uuid (i.e. a unique value) so consecutive
    # groups don't get grouped.
    # Then iterate through groups and create markup.
    for group_id, group in groupby(ingredients, key=lambda x: x.get("group", -1)):
        group = list(group)

        # Not a group
        if group_id == -1:
            ul = ET.Element("ul")
            for item in group:
                ul.append(list_element(item))

            div.append(ul)

        else:
            details = ET.Element("details")
            summary = ET.Element("summary", attrib={"class": "ingredient-subheading"})
            summary.text = ingredient_headings.get(str(group_id))

            # Carat icon to display after summary when details element is not open
            svg = ET.Element(
                "svg",
                attrib={
                    "xmlns": "http://www.w3.org/2000/svg",
                    "width": "16",
                    "height": "16",
                    "fill": "currentColor",
                    "class": "icon",
                    "viewBox": "0 0 16 16",
                },
            )
            path = ET.Element(
                "path",
                attrib={
                    "fill-rule": "evenodd",
                    "d": "M4.646 1.646a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 0 .708l-6 6a.5.5 0 0 1-.708-.708L10.293 8 4.646 2.354a.5.5 0 0 1 0-.708z",
                },
            )
            svg.append(path)
            summary.append(svg)

            details.append(summary)

            ul = ET.Element("ul")
            # Skip first element
            for item in group:
                ul.append(list_element(item))

            details.append(ul)
            div.append(details)

    # Add whitspace to indent - this modifies in place - then convert to string.
    ET.indent(div, space="    ", level=3)
    stringified = ET.tostring(div, encoding="unicode", method="html")
    # In xml boolean attributes don't exist so add 'open' attribute manually.
    stringified = stringified.replace("<details>", "<details open>")
    # Ingredients that use 1x, 2x etc get a extra space when rendered in html caused by
    # the new line added when stringifying.
    # Use a regular expression to remove that newline
    stringified = re.sub(r"\n\s+</b>x", "</b>x", stringified)

    return stringified
