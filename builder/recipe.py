import datetime
import os
from functools import lru_cache
from pathlib import Path

import isodate
from bs4 import BeautifulSoup

from .models import BookDB
from .utils import readable_duration, select_annotatable_ingredients, sha256


def load_recipe_book_info() -> dict[str, dict[str, str | int]]:
    """Load recipe book data from database

    Returns
    -------
    dict[str, dict[str, str | int]]
        Dict of recipe book names and ISBNs
    """
    books = {}
    for book in BookDB.select():
        books[book.title] = {"isbn": book.isbn, "cover": book.cover}

    return books


RECIPE_BOOKS = load_recipe_book_info()


class RecipePage:
    def __init__(self, db_dict, template):
        self.template = template
        self.recipe = self._generate_info(db_dict)
        self.date = datetime.datetime.strptime(self.recipe["dateCooked"], "%Y-%m-%d")

    def _generate_info(self, db_dict):
        recipe = db_dict

        # Fix image path for recipe page
        # If image doesn't exist, replace with svg icon
        if os.path.isfile("website/images/" + recipe["image"]):
            recipe["image"] = f'/images/{recipe["image"]}'
        else:
            recipe["image"] = f'/img/placeholder-{recipe["category"].lower()}.svg'

        # Add nutritionInfo flag if nutrition info exists
        recipe["nutritionInfo"] = recipe["nutrition"] is not None

        # Create diet key
        recipe["diet"] = {
            "Vegan": recipe["vegan"],
            "GlutenFree": recipe["glutenFree"],
            "DairyFree": recipe["dairyFree"],
            "NutFree": recipe["nutFree"],
        }
        del recipe["vegan"]
        del recipe["glutenFree"]
        del recipe["dairyFree"]
        del recipe["nutFree"]

        # Fix name of yield key
        recipe["yield"] = recipe["yield_"]
        del recipe["yield_"]

        # Add flag for description
        recipe["hasDescription"] = recipe["description"] != ""

        # If url is empty, set information for displaying book book
        if recipe["url"] == "" and recipe["source"] in RECIPE_BOOKS.keys():
            recipe["book"] = True
            recipe["book_url"] = _book_url(recipe["source"])
            recipe["book_cover"] = RECIPE_BOOKS[recipe["source"]]["cover"]

        # If original url marked as dead, replace with archive url if there is one
        if recipe["deadUrl"] and recipe["archiveUrl"] != "":
            recipe["url"] = recipe["archiveUrl"]

        # Format durations properly
        total_duration = isodate.parse_duration(recipe["totalTime"]).total_seconds()
        recipe["totalTimeReadable"] = readable_duration(total_duration)

        if recipe["prepTime"] != "":
            prep_duration = isodate.parse_duration(recipe["prepTime"]).total_seconds()
            recipe["prepTimeReadable"] = readable_duration(prep_duration)

        if recipe["cookTime"] != "":
            cook_duration = isodate.parse_duration(recipe["cookTime"]).total_seconds()
            recipe["cookTimeReadable"] = readable_duration(cook_duration)

        if recipe["inactiveTime"] != "":
            inactive_duration = isodate.parse_duration(
                recipe["inactiveTime"]
            ).total_seconds()
            recipe["inactiveTimeReadable"] = readable_duration(inactive_duration)

        # Build absolute urls for meta elements
        recipe["image_absolute"] = f"https://www.strangerfoods.org{recipe['image']}"
        recipe["url_absolute"] = (
            f"https://www.strangerfoods.org/recipes/{recipe['slug']}"
        )

        # Add CSP hashes to dictionary
        recipe["style_hash"] = _recipe_CSP_hashes(self.template)

        # Create a filtered view of the ingredients, flattening the list, stripping out
        # elements that aren't dicts and elements with no quantity and no unit.
        # This is done to speed up annotation of instructions. By doing it here, it's
        # only done once per recipe instead of for each step in the instructions.
        recipe["annotatable_ingredients"] = select_annotatable_ingredients(
            recipe["ingredients"]
        )

        return recipe

    def write_page(self, path: str):
        # Build html from template
        html = self.template.render(self.recipe)

        # Save file
        slug = self.recipe["slug"]
        name = Path(path) / f"{slug}.html"
        with open(name, "w") as f:
            f.write(html)

    def search_data(self) -> dict[str, list[str] | str | bool]:
        """Return dictionary of information needed for the search functionality
        on homepage

        Returns
        -------
        dict[str, list[str] | str | bool]
            Search data
        """
        total_time_seconds = isodate.parse_duration(
            self.recipe["totalTime"]
        ).total_seconds()

        search_dict = {
            "name": self.recipe["name"],
            "slug": self.recipe["slug"],
            "category": self.recipe["category"],
            "yield": self.recipe["yield"],
            "source": self.recipe["source"],
            "time": readable_duration(total_time_seconds),
            "ingredients": self._extract_unique_ingredients(),
        }
        diet = [key for key in self.recipe["diet"].keys() if self.recipe["diet"][key]]
        if diet != []:
            search_dict["diet"] = diet

        if self.recipe["cuisine"] != "":
            search_dict["cuisine"] = self.recipe["cuisine"]

        if self.recipe["tags"] != []:
            search_dict["tags"] = self.recipe["tags"]

        if self.recipe["rating"] == "Positive":
            search_dict["positive"] = True

        return search_dict

    def _extract_unique_ingredients(self) -> list[int]:
        """Return list of unique FDC IDs for recipe

        Returns
        -------
        list[int]
            List of unique ingredient FDC IDs
        """
        unique_ingredients = set()
        for ingredient in self.recipe["ingredients"]:
            fdc_id = ingredient["fdc_id"]
            if fdc_id:
                unique_ingredients.add(fdc_id)
        return list(unique_ingredients)

    def feed_data(self) -> dict[str, str | datetime.datetime] | None:
        """Return dict of information needed for json and atom feeds.

        If the recipe has no image, reutrn None

        Returns
        -------
        dict[str, list[str] | str | bool] | None
            dict of information needed for json and atom feeds if recipe has image,
            else None
        """
        if self.recipe["image"].endswith("svg"):
            return None

        title = self.recipe["name"]
        url = "https://www.strangerfoods.org/recipes/" + self.recipe["slug"]
        image = "https://www.strangerfoods.org" + self.recipe["image"]
        feed_content = f"""
        <h1 style="text-align:center;font-family:sans-serif;">{title}</h1>
        <br>
        <img src="{image}" style="text-align:center;"/>
        <br>
        <p style="font-family:sans-serif;>{self.recipe['description']}</p>
        """.strip()

        return {
            "title": title,
            "url": url,
            "image": image,
            "feed_content": feed_content,
            "date": self.date,
        }


@lru_cache
def _recipe_CSP_hashes(template) -> str:
    """Calculate sha256 hash of style tag required by Content Security Policy

    Parameters
    ----------
    template : TEMPLATE
        Jinja2 TEMPLATE object

    Returns
    -------
    str
        Base64 encoded sha256 hash of (style, script)
    """
    with open(template.filename, "r") as f:
        html = f.read()
    soup = BeautifulSoup(html, "lxml")

    # Find inline style
    style = soup.find("style").contents[0]

    return sha256(style)


@lru_cache
def _book_url(title: str) -> str:
    """Return Goodreads URL for book title based on ISBN, if ISBN is available

    Parameters
    ----------
    title : str
        Book title from recipe source field

    Returns
    -------
    str
        URL for Goodreads search function for ISBN or "#"
    """
    if title in RECIPE_BOOKS.keys():
        isbn = RECIPE_BOOKS[title]["isbn"]
        return f"https://www.goodreads.com/search/search?search_type=books&search[query]={isbn}"
    else:
        return ""
