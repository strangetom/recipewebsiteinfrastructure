#!/usr/bin/env python3


import peewee as pw
from playhouse.sqlite_ext import JSONField

database = pw.SqliteDatabase(
    "../Recipes/recipes.db", pragmas={"foreign_keys": 1, "journal_mode": "wal"}
)


class TextField(pw.TextField):
    """Custom field type for text fields that returns an empty string instead of None"""

    def python_value(self, value):
        if value is None:
            return ""
        return value


class BaseModel(pw.Model):
    class Meta:
        database = database


class BookDB(BaseModel):
    isbn = pw.IntegerField(null=True)
    title = pw.TextField(null=True, primary_key=True)
    cover = pw.TextField(null=True)

    class Meta:
        table_name = "books"


class RecipeDB(BaseModel):
    archiveUrl = TextField(null=True)
    archiveTimestamp = TextField(null=True)
    category = TextField(null=True)
    cookTime = TextField(column_name="cookTime", null=True)
    cuisine = TextField(null=True)
    deadUrl = pw.BooleanField(null=True)
    dairyFree = pw.BooleanField(column_name="dairyFree", null=True)
    dateCooked = TextField(column_name="dateCooked", null=True)
    description = TextField(null=True)
    future = pw.BooleanField(null=True)
    glutenFree = pw.BooleanField(column_name="glutenFree", null=True)
    image = TextField(null=True)
    inactiveTime = TextField(column_name="inactiveTime", null=True)
    ingredients = JSONField(null=True)  # list
    ingredient_headings = JSONField(null=True)  # dict
    instructions = JSONField(null=True)  # list
    instruction_headings = JSONField(null=True)  # dict
    name = TextField(null=True)
    notes = TextField(null=True)
    nutFree = pw.BooleanField(column_name="nutFree", null=True)
    nutrition = JSONField(null=True)  # dict
    prepTime = TextField(column_name="prepTime", null=True)
    rating = TextField(null=True)
    slug = TextField(null=False, primary_key=True)
    source = TextField(null=True)
    tags = JSONField(null=True)  # list
    totalTime = TextField(column_name="totalTime", null=True)
    units = TextField(null=True)
    url = TextField(null=True)
    vegan = pw.BooleanField(null=True)
    yield_ = TextField(column_name="yield", null=True)

    class Meta:
        table_name = "recipes"


class CuisinesDB(BaseModel):
    uid = pw.IntegerField(null=False, primary_key=True)
    cuisine = pw.TextField(null=False)
    region = pw.TextField(null=True)
    continent = pw.TextField(null=True)

    class Meta:
        table_name = "cuisines"


class IngredientsDB(BaseModel):
    uid = pw.IntegerField(null=False, primary_key=True)
    fdc_id = pw.IntegerField(null=False)
    description = pw.TextField(null=False)
    synonyms = JSONField(null=False)
    name = pw.TextField(null=False)

    class Meta:
        table_name = "ingredients"


slug_idx = RecipeDB.index(RecipeDB.slug, unique=True, name="recipe_slugs")
RecipeDB.add_index(slug_idx)
