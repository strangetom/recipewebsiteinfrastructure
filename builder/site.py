import hashlib
import json
import os
import re
from collections import Counter, defaultdict
from email import utils
from pathlib import Path
from typing import Any

from feedgen.feed import FeedGenerator
from jinja2 import Environment, FileSystemLoader
from tqdm import tqdm

from .filters import annotate_instruction, format_ingredients
from .models import IngredientsDB, RecipeDB
from .recipe import RecipePage

# Set up jinja environment
TEMPLATE_ENVIRONMENT = Environment(
    autoescape=False,
    loader=FileSystemLoader(os.path.join(os.getcwd(), "templates")),
    trim_blocks=False,
)

# Add filters and apply in specific parts of template by adding
# e.g. {{<var> | format_ingredients}}
TEMPLATE_ENVIRONMENT.filters["format_ingredients"] = format_ingredients
TEMPLATE_ENVIRONMENT.filters["annotate_instruction"] = annotate_instruction


class RecipeWebsite:
    def __init__(
        self,
        db_path: str = "../Recipes/recipes.db",
        skip_uncooked: bool = False,
        recipe_template: str = "template_recipe.html",
        index_template: str = "template_index.html",
        all_template: str = "template_all.html",
    ):
        self.db = db_path
        self.skip_uncooked = skip_uncooked
        self.recipe_template = TEMPLATE_ENVIRONMENT.get_template(recipe_template)
        self.index_template = TEMPLATE_ENVIRONMENT.get_template(index_template)
        self.all_template = TEMPLATE_ENVIRONMENT.get_template(all_template)

    def _get_search_data_hash(self, file: str) -> str:
        """Calculate the sha256 hash of the minified search_data file.
        This is used to tell when an update has been issued (i.e. the hash has changed)
        and therefore when to prompt the user to update the offline cache.

        Parameters
        ----------
        file : str
            File path to search_data.js

        Returns
        -------
        str
            sha256 hash hexdigest of file
        """
        sha256 = hashlib.sha256()
        with open(file, "rb") as f:
            data = f.read()
            sha256.update(data)

        return sha256.hexdigest()

    def _get_cache_size(self):
        """Calculate size of all website files to indicate size for offline
        caching of site

        Returns
        -------
        str
            Formatted string of size in MB to nearest integer
        """
        site_directory = Path("website")
        # Get size of files in website directory
        size: int = sum(
            f.stat().st_size for f in site_directory.glob("*") if f.is_file()
        )

        # Loop through all non-excluded subdirectories and add size
        excluded_sub_dirs = ["screenshots"]
        sub_dirs = [
            d
            for d in site_directory.glob("*")
            if d.is_dir()
            and not d.name.startswith(".")
            and d.name not in excluded_sub_dirs
        ]
        for d in sub_dirs:
            size += sum(f.stat().st_size for f in d.rglob("*") if f.is_file())

        # Return as formatted string
        return size

    def build_site(self, skip_uncooked=False) -> None:
        """Main class function to build site.
        Iterate through each recipes and does the following:
            1. Build the recipe page for the given recipe
            2. Adds that recipe's search data to list
            3. Adds that recipe's feed data to list
            4. Write the search data file
            5. Write index.html
            6. Write all_recipes.html
            7. Selected the latest recipes with image to build feeds from
        """
        recipe_search_data = []
        recipe_feed_data = []
        print("Building recipe pages")

        recipes = (
            RecipeDB.select()
            .where(RecipeDB.future == False)  # noqa: E712
            .order_by(RecipeDB.dateCooked.desc())
            .dicts()
        )
        for r in tqdm(recipes):
            # Skip uncooked recipe (recipes without images) if flag set
            if skip_uncooked and not os.path.isfile("website/images/" + r["image"]):
                continue

            recipe = RecipePage(r, self.recipe_template)
            recipe.write_page("website/recipes")
            recipe_search_data.append(recipe.search_data())
            recipe_feed_data.append(recipe.feed_data())

        # Write search data
        print("Building search data")
        with open("static/js/search_data.js", "w") as f:
            f.write("var recipe_data = ")
            json.dump({"recipes": recipe_search_data}, f, indent=4)

        # Write index page
        print("Building index page")
        homepage_data = self._gather_homepage_data(recipe_search_data)
        html = self.index_template.render(homepage_data)
        with open("website/index.html", "w") as f:
            f.write(html)

        # Write all-recipes page
        allrecipe_data = self._gather_all_recipe_data(recipe_search_data)
        html = self.all_template.render(allrecipe_data)
        with open("website/all-recipes.html", "w") as f:
            f.write(html)

        # Build RSS feed
        print("Building RSS and JSON feeds")
        self.build_feed(recipe_feed_data)

    def _gather_homepage_data(self, search_data):
        """Gather data needed to populate homepage template

        Parameters
        ----------
        search_data : dict[str, str]
            Dict of search data

        Returns
        -------
        dict[str, str]
            Dict of data for homepage template population
        """
        # If recipe image doesn't exist for latest, add a placeholder
        latest = []
        for recipe in search_data[:10]:
            if not os.path.isfile("website/images/" + recipe["slug"] + ".webp"):
                recipe["placeholder"] = (
                    f'/img/placeholder-{recipe["category"].lower()}.svg'
                )

            latest.append(recipe)

        return {
            "latest": latest,
            "total": len(search_data),
            "categories": self._format_categories(self._get_categories()),
            "cuisines": self._format_categories(self._get_cuisines()),
            "diets": self._format_categories(self._get_diets()),
            "tags": self._format_categories(self._get_tags()),
            "sources": sorted(set(self._get_sources())),
            "updateHash": self._get_search_data_hash("static/js/search_data.js"),
            "cacheSize": self._get_cache_size(),
            "ingredients": self._gather_ingredients_data(),
        }

    def _gather_ingredients_data(self) -> dict[int, str]:
        """Return dict of ingredients, the key is the FDC ID and the value is
        the description.

        Returns
        -------
        dict[int, str]
            Ingredients dict
        """
        query = IngredientsDB.select(IngredientsDB.fdc_id, IngredientsDB.name).tuples()
        ingredients = {name: fdc_id for fdc_id, name in query}
        return ingredients

    def _gather_all_recipe_data(self, search_data):
        """Gather data needed to populate the 'all recipes' template

        Parameters
        ----------
        search_data : dict[str, str]
            Dict of search data

        Returns
        -------
        dict[str, str]
            Dict of data for all recipes template population
        """
        # Group recipes by starting letter
        groups = defaultdict(list)
        # Iterate through recipes in alphabetical order
        for recipe in sorted(search_data, key=lambda x: x["name"]):
            key = recipe["name"][0]
            if key.isnumeric():
                key = "#"

            groups[key].append(recipe)

        return {"recipes": dict(groups)}

    def _get_categories(self) -> list[str]:
        """Return list of each instance of each cuisine

        Returns
        -------
        list[str]
            Lsit of all instances of all cuisines
        """
        return [r.category for r in RecipeDB.select(RecipeDB.category) if r.category]

    def _get_cuisines(self) -> list[str]:
        """Return list of each instance of each cuisine

        Returns
        -------
        list[str]
            List of all instances of all cuisines
        """
        return [r.cuisine for r in RecipeDB.select(RecipeDB.cuisine) if r.cuisine]

    def _get_sources(self) -> list[str]:
        """Return list of each instance of each cuisine

        Returns
        -------
        list[str]
            List of all instances of all cuisines
        """
        return [r.source for r in RecipeDB.select(RecipeDB.source) if r.source]

    def _get_diets(self) -> list[str]:
        """Return list containing each instance a diet appears in a recipe

        Returns
        -------
        list[str]
            List of all instances of all diets
        """
        diets = []
        for r in RecipeDB.select(
            RecipeDB.vegan, RecipeDB.nutFree, RecipeDB.glutenFree, RecipeDB.dairyFree
        ):
            if r.vegan:
                diets.append("Vegan")
            if r.glutenFree:
                diets.append("GlutenFree")
            if r.dairyFree:
                diets.append("DairyFree")
            if r.nutFree:
                diets.append("NutFree")

        return diets

    def _get_tags(self) -> list[str]:
        """Return list of each instance of each tag

        Returns
        -------
        list[str]
            List of all instances of all tags
        """
        tags = []
        for r in RecipeDB.select(RecipeDB.tags):
            if r.tags:
                tags.extend(r.tags)
        return tags

    def _format_categories(self, category_list: list[str]) -> list[tuple[str, str]]:
        """From a list of categories, return a list of unique categories with their
        count as strings. Categories can be category, cuisine, diet etc.

        Parameters
        ----------
        category_list : List[str]
            List of categories from recipes

        Returns
        -------
        List[Tuple[str, str]]
            List of tuples, each containing the unique category and unique category
            with count (category, category (count))
        """
        counts = Counter(category_list)
        unique_categories = sorted(set(category_list))

        output = []
        for c in unique_categories:
            # If a capital letter is inside a word, insert a space
            formatted = re.sub(r"(\w)([A-Z])", r"\1 \2", c)

            # Make formatted title case and append to list
            output.append((c, f"{formatted.title()} ({counts[c]})"))

        return output

    def build_feed(self, sorted_recipes: list[dict[str, Any]], num: int = 6) -> None:
        """Build xml for atom feed and json for json feed

        Parameters
        ----------
        sorted_recipes : list[dict[str, Any]]
            List of tuples (recipe dict, date) sorted by date in descending order
        num : int, optional
            Number of latest recipes to include in feed
        """

        feed = FeedGenerator()
        feed.load_extension("media", atom=True)
        feed.id("https://strangerfoods.org/")
        feed.title("Stranger Foods")
        feed.author({"name": "Tom Strange"})
        feed.link(href="https://strangerfoods.org/", rel="alternate")
        feed.subtitle("A catalogue of recipes Tom has made.")
        feed.language("en")
        feed.icon("https://strangerfoods.org/img/logo.svg")

        json_feed: dict[str, Any] = {
            "version": "https://jsonfeed.org/version/1.1",
            "title": "Stranger Foods",
            "home_page_url": "https://strangerfoods.org/",
            "feed_url": "https://strangerfoods.org/feed/feed.json",
            "description": "A catalogue of recipes Tom has made.",
            "authors": [{"name": "Tom Strange"}],
            "language": "en",
            "items": [],
        }
        # Take latest recipes
        for recipe in sorted_recipes:
            if recipe is None:
                continue

            # Skip if no image
            if recipe["image"].endswith("svg"):
                continue

            # Atom feed item
            feed_entry = feed.add_entry()
            feed_entry.title(recipe["title"])
            feed_entry.link(href=recipe["url"], rel="alternate")
            feed_entry.content(recipe["feed_content"], type="html")
            feed_entry.published(utils.formatdate(recipe["date"].timestamp()))
            feed_entry.guid(recipe["url"], permalink=True)
            feed_entry.media.thumbnail(
                thumbnail={
                    "url": recipe["image"],
                    "height": "288",
                    "width": "512",
                    "time": "0",
                }
            )

            # JSON feed item
            item = {
                "id": recipe["url"],
                "url": recipe["url"],
                "title": recipe["title"],
                "content_html": recipe["feed_content"],
                "image": recipe["image"],
                "date_published": recipe["date"].isoformat() + "Z",
                "language": "en",
            }
            json_feed["items"].append(item)

            if len(feed.entry()) == num:
                break

        # Write atom feed to file
        feed.atom_file("./website/feed/atom.xml", pretty=True)
        # Insert stylesheet
        with open("./website/feed/atom.xml", "r+") as f:
            xml = f.readlines()
            xml.insert(1, '<?xml-stylesheet href="/css/atom.xsl" type="text/xsl"?>\n')
            f.seek(0)
            f.write("".join(xml))
            f.truncate()

        # Write JSON feed to file
        with open("./website/feed/feed.json", "w") as f:
            json.dump(json_feed, f, indent=2)
